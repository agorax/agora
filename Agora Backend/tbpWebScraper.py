
from bs4 import BeautifulSoup
import urllib
import re
import sys
sys.stdout = open('edition24.txt', 'w')
base_url = "http://bookswap.case.edu"
pattern = re.compile(r'ISBN-13')
r = urllib.urlopen('http://bookswap.case.edu/search.php?class=&title=&author=&edition=24&isbn=').read()
soup = BeautifulSoup(r)
array = soup.find_all('tr')
for x in xrange(1,len(array)):
    if x < 7:
        pass
    else:
        subarray = array[x].find_all('td')
        for item in subarray:
            if item.find_all('a', href = True):
                isbnPage = urllib.urlopen(base_url + '/' + item.find_all('a', href = True)[0]['href']).read()
                isbnSoup = BeautifulSoup(isbnPage)
                isbn = isbnSoup.find(text = pattern)
                print(isbn.parent.nextSibling[1:])
            else:
                if item.string:
                    print(item.string.encode('utf-8'))


#print(array[100])
#print(soup.find_all('td'))

