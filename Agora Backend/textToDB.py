# coding: utf-8
import MySQLdb
conn = MySQLdb.connect (host = "agora-db-instance.ci2jjl1w50ap.us-east-2.rds.amazonaws.com",user = "jsw104", passwd = "agorapassword",db = "AgoraDB")
cursor = conn.cursor()

file = open("edition24.txt", "r") 
lines = file.readlines()
index = 0
while(index < len(lines)):
	isbn = lines[index]
	name = lines[index + 1][:40].rstrip()
	edition = 24
	author = lines[index + 3]
	price = lines[index + 4][1:-3]
	print(name)
	cursor.execute("""INSERT INTO item (userId, price, name, isbn, edition, author, numberOfPictures, category,school)
                   VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)""", (275, price, name, isbn, edition, author, 1, "Textbooks", "Case Western"))
	index = index + 5
conn.commit()
conn.close()