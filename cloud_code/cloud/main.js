
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.afterDelete('Item', function(request) {
  var Wishlist =  Parse.Object.extend('Wishlist');
  query = new Parse.Query(Wishlist);
  query.equalTo('itemIdArray',request.object.id);
  query.find({
    success: function(wishlist) {
      for(var i = 0; i < wishlist.length; i++) {
        wishlist[i].remove('itemIdArray',request.object.id);
        wishlist[i].save();
      }

    },
    error: function(error) {
      console.error("Error finding related items " + error.code + ": " + error.message);
    }
  });
});

Parse.Cloud.afterSave('Review', function(request) {
  query = new Parse.Query(Parse.User);
  query.equalTo('objectId',request.object.get('userId'));
  query.find({
    success: function(user) {
      Parse.Cloud.useMasterKey();
      user[0].increment('numRatings');
      var newRat = request.object.get('rating');
      var num = user[0].get('numRatings');
      if (num === 1) {
        user[0].set('rating',newRat);
      }
      else {
        var avg = user[0].get('rating');
        avg -= avg / num;
        avg += newRat / num;
        user[0].set('rating',avg);
      }
      user[0].save();
    }
  });
});

Parse.Cloud.beforeSave('Wishlist', function(request, response) {
  console.log(request.object.isNew());
  if(!request.object.isNew() || request.object.get('itemIdArray') === []) {
    var Item =  Parse.Object.extend('Item');
    query = new Parse.Query(Item);
    query.equalTo('objectId', request.object.get('itemIdArray').slice(-1)[0]);
    query.find({
      success: function(item) {
        if(typeof item[0] === 'undefined') {
          response.error('Item Does not Exist');
        }
        else {
          response.success();
        }
      },
      error: function(error) {
        console.log('here');
        response.error('Item no longer exist');
      }
    });
  } else {
    response.success();
  }
});

Parse.Cloud.job('ItemExpiring', function(request,status) {
  var Item =  Parse.Object.extend('Item');
  query = new Parse.Query(Item);
  var date = new Date();
  var numDays = 28;
  date.setDate(date.getDate() - numDays);
  query.lessThan('createdAt',date);
  query.find({
    success: function(items) {
      for(var i = 0; i < items.length; i++) {
        it = items[i];
        console.log(it.get('name') + '---' + it.get('createdAt'));
        it.destroy({});
      }
      status.success("Delete successfully.");
    },
    error: function(error) {
      status.error('Something went wrong ' + error);
    }
  });
  console.log(date);
});

Parse.Cloud.afterDelete(Parse.User, function(request) {
  Parse.Cloud.useMasterKey();
  var userWList = request.object.get('wishlistPointer').id;
  console.log("user wish list " + userWList);
  var Wishlist =  Parse.Object.extend('Wishlist');
  query = new Parse.Query(Wishlist);
  query.equalTo('objectId', userWList);
  query.first({
    success: function(wishlist) {
      wishlist.destroy({});
    },
    error: function(error) {
      console.log("Error: " + error.message);
    }
  });
});
