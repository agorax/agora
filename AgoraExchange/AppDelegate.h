//
//  AppDelegate.h
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "OBDragDrop.h"
#import <CoreData/CoreData.h>
#import <UserNotifications/UserNotifications.h>
#import "SessionManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSObject, UNUserNotificationCenterDelegate> {

NSManagedObjectModel *managedObjectModel;
NSManagedObjectContext *managedObjectContext;
NSPersistentStoreCoordinator *persistentStoreCoordinator;
    
}

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSString *)applicationDocumentsDirectory;
+ (UIViewController *)topViewController:(UIViewController *)rootViewController;

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong) UINavigationController *navigationController;
@property(nonatomic,strong) LoginViewController* LoginViewController;
@property (strong, nonatomic) UIStoryboard *storyboard;
@property BOOL launched;
@property (strong, nonatomic) NSString *deviceToken;

@end

