//
//  AgoraExchange-Bridging-Header.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/9/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#ifndef AgoraExchange_Bridging_Header_h
#define AgoraExchange_Bridging_Header_h

#import "AppDelegate.h"
#import "MarketplaceViewController.h"

#endif /* AgoraExchange_Bridging_Header_h */
