//
//  EditInfoFabButton.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/2/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "FabButton.h"

@interface EditInfoFabButton : FabButton

@end
