//
//  dismissButton.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/7/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface dismissButton : UIButton

@property (nonatomic) BOOL enteringAgoraX;
@property (strong, nonatomic) UIView *viewToBeDismissed;

@end
