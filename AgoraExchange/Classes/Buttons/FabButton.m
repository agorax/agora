//
//  FabButton.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/2/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "FabButton.h"
/*
 Message,
 EditListing,
 EditProfile,
 EditListings,
 Review*/

@implementation FabButton

- (void)setFabType:(fabType)type {
    self.type = type;
    switch (type) {
        case FabMessage: {
            [self setHidden:NO];
            [UIView transitionWithView:self.imageView
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self setImage:[UIImage imageNamed:@"MessageFabGray"] forState:UIControlStateNormal];
                            } completion:nil];
            break;
        }
        case EditListing: {
            [self setHidden:NO];
            [UIView transitionWithView:self.imageView
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self setImage:[UIImage imageNamed:@"PencilFabRed"] forState:UIControlStateNormal];
                            } completion:nil];
            break;
        }

        case EditProfile: {
            [self setHidden:NO];
            [UIView transitionWithView:self.imageView
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self setImage:[UIImage imageNamed:@"PencilFabRed"] forState:UIControlStateNormal];
                            } completion:nil];
            break;
        }
        case EditListings: {
            [self setHidden:NO];
            [UIView transitionWithView:self.imageView
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self setImage:[UIImage imageNamed:@"PencilFabGreen"] forState:UIControlStateNormal];
                            } completion:nil];
            break;
        }
        case AddReview: {
            [self setHidden:NO];
            [UIView transitionWithView:self.imageView
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self setImage:[UIImage imageNamed:@"ReviewFabGray"] forState:UIControlStateNormal];
                            } completion:nil];
            break;
        }
        case None: {
            [UIView transitionWithView:self.imageView
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self setImage:nil forState:UIControlStateNormal];
                                //[self setHidden:YES];
                            } completion:nil];
        }
        default:
            break;
    }
}

@end
