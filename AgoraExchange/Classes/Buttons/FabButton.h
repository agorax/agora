//
//  FabButton.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/2/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, fabType) {
    FabMessage,
    EditListing,
    EditProfile,
    EditListings,
    AddReview,
    None
};

@interface FabButton : UIButton

@property (nonatomic) fabType type;

- (void)setFabType:(fabType)type;

@end
