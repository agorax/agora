//
//  optionButton.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/11/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface optionButton : UIButton

@property (strong, nonatomic) NSString *optionSelected;
@property (strong, nonatomic) id iconPointer;

@end
