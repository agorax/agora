//
//  submitEmail.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/8/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface submitEmail : UIButton

@property (nonatomic) BOOL resendingConfirmationCode;
@property (strong, nonatomic) UIView *viewToBeDismissed;

@end
