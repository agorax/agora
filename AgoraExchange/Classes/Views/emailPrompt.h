//
//  emailPrompt.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/8/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface emailPrompt : UIView
@property (strong, nonatomic) IBOutlet UITextField *emailField;
- (IBAction)submit:(id)sender;

@end
