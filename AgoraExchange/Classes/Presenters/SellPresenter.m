//
//  SellPresenter.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/25/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "SellPresenter.h"
#import "NetworkManager.h"
#import "SessionManager.h"
#import "AgoraImageManager.h"

@implementation SellPresenter

- (void)sellItem:(Item *)item images:(NSArray<UIImage *> *)imageArray withCompletionBlock:(void(^)(AgoraError *error))completionBlock {
    [NetworkManager sellItem:item withToken:[SessionManager fetchAgoraToken] andCompletionBlock:^(int itemId, AgoraError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error){
                item.itemId = itemId;
                for (int i = 0; i < imageArray.count; i++) {
                    [AgoraImageManager uploadToS3:imageArray[i] onItem:item pictureNumber:i+1];
                }
            }
            completionBlock(error);
        });
    }];
}

@end
