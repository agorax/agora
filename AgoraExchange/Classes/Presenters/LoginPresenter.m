//
//  LoginPresenter.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/21/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "LoginPresenter.h"
#import "NetworkManager.h"
#import "SessionManager.h"
#import "AppDelegate.h"

@implementation LoginPresenter

-(void)loginWithEmail:(NSString *)email
              password:(NSString *)password
             apnsToken: (NSString *)apnsToken
   andCompletionBlock:(void(^)(int userId, AgoraError *error))completionBlock {
    [NetworkManager loginWithEmail:email password:password apnsToken:apnsToken andCompletionBlock:^(int userId, NSString *token, AgoraError *error) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SessionManager storeAgoraTokenInKeychain:token];
                [SessionManager setCurrentUserId:userId];
                [SessionManager configureSyncTimer];
                completionBlock(userId, error);
            });
        } else {
            [SessionManager logout];
            completionBlock(userId, error);
        }
    }];
}



@end
