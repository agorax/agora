//
//  ProfileListingsPresenter.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "ProfileListingsPresenter.h"
#import "NetworkManager.h"
#import "SessionManager.h"

@implementation ProfileListingsPresenter

- (void)deleteItems:(NSArray<NSNumber *> *)itemIds{
    for (NSNumber *itemId in itemIds) {
        [NetworkManager deleteItem:itemId withToken:[SessionManager fetchAgoraToken] andCompletionBlock:^(AgoraError *error) {
            if (!error) {
                [self deleteItemsInCoreData:itemIds];
            }
            //delete associated pictures
        }];
    }
}

- (void)deleteItemsInCoreData:(NSArray <NSNumber *>*)itemIds {
    if (!itemIds || [itemIds count] == 0) {
        return;
    }
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"itemId IN %@", itemIds];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    [request setPredicate:predicate];
    NSArray <Item *>*results = [[ProfileListingsPresenter managedObjectContext] executeFetchRequest:request error:nil];
    for(Item *item in results) {
        [[ProfileListingsPresenter managedObjectContext] deleteObject:item];
    }
    [[ProfileListingsPresenter managedObjectContext] save:nil];
}

+(void)deleteItemsNotInArray:(NSArray *)itemArray intoManagedObjectContext:(NSManagedObjectContext *)context{
    if (!itemArray) {
        return;
    }
    NSMutableSet *itemSet = [[NSMutableSet alloc] init];
    for (NSDictionary *itemDict in itemArray) {
        [itemSet addObject:[itemDict objectForKey:@"itemId"]];
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    NSArray <Item *>*results = [context executeFetchRequest:request error:nil];
    for (Item *item in results) {
        if (![itemSet containsObject:@(item.itemId)]) {
            [context deleteObject:item];
        }
    }
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
