//
//  ReviewPresenter.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/22/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "ReviewPresenter.h"
#import "NetworkManager.h"
#import "SessionManager.h"

@implementation ReviewPresenter

- (void)addReview:(Review *)review completionBlock:(void(^)(AgoraError *error))completionBlock {
    [NetworkManager addReview:review withToken:[SessionManager fetchAgoraToken] andCompletionBlock:^(AgoraError *error) {
        completionBlock(error);
    }];
}

@end
