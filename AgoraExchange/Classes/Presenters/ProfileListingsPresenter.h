//
//  ProfileListingsPresenter.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item+CoreDataClass.h"
#import "AgoraError.h"

@interface ProfileListingsPresenter : NSObject

- (void)deleteItems:(NSArray<NSNumber *> *)itemIds;

@end
