//
//  LoginPresenter.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/21/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User+CoreDataClass.h"
#import "AgoraError.h"

@interface LoginPresenter : NSObject

-(void)loginWithEmail:(NSString *)email
             password:(NSString *)password
            apnsToken: (NSString *)apnsToken
   andCompletionBlock:(void(^)(int userId, AgoraError *error))completionBlock;

@end
