//
//  ReviewPresenter.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/22/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Review+CoreDataClass.h"
#import "AgoraError.h"

@interface ReviewPresenter : NSObject

- (void)addReview:(Review *)review completionBlock:(void(^)(AgoraError *error))completionBlock;

@end
