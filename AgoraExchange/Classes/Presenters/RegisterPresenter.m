//
//  RegisterPresenter.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/24/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "RegisterPresenter.h"
#import "pickerData.h"
#import "NetworkManager.h"

@implementation RegisterPresenter


-(void)registerUserWithEmail:(NSString *)email
                            password:(NSString *)password
                              school:(NSString *)school
                  andCompletionBlock:(void(^)(AgoraError *error))completionBlock
{
    [NetworkManager registerWithEmail:email password:password school:school andCompletionBlock:^(AgoraError *error) {
        completionBlock(error);
    }];
}


-(AgoraError *)validateRegistrationWithEmail:(NSString *)email
                                 password:(NSString *)password
                           retypePassword:(NSString *)retypePassword
                                   school:(NSString *)school {
    AgoraError *error;
    error = [self checkEmailFormatWithEmail:email andSchool:school];
    if(error)
    {
        return error;
    }
    error = [self checkPasswordLength:password];
    if (error) {
        return error;
    }
    error = [self checkPasswordsMatchWithPassword:password andRetypedPassword:retypePassword];
    return error;
}

#pragma mark - verification methods
-(AgoraError *)checkPasswordsMatchWithPassword:(NSString*)password andRetypedPassword:(NSString*)retypePassword
{
    return ([retypePassword isEqualToString: password]) ? NULL : [[AgoraError alloc]initWithStatusCode:600 name:@"Registration Verification Error" message:@"Whoops, looks like your passwords don't match!"];
}

-(AgoraError *)checkPasswordLength: (NSString* ) password
{
    return ([password length] >= 8) ? NULL : [[AgoraError alloc]initWithStatusCode:601 name:@"Registration Verification Error" message:@"Whoops, looks like your password is not long enough!"];
    
}

-(AgoraError *)checkEmailFormatWithEmail:(NSString*)email andSchool:(NSString*)school
{
    
    NSString *correctEndOfEmail = [[pickerData collegeNameToEmail] objectForKey:school];
    if (email.length > correctEndOfEmail.length + 1) {
        NSString *endOfEmail = [email substringFromIndex:email.length - correctEndOfEmail.length];
        if ([endOfEmail isEqualToString:correctEndOfEmail]) {
            return NULL;
        }
    }
    return [[AgoraError alloc]initWithStatusCode:602 name:@"Registration Verification Error" message:[NSString stringWithFormat:@"Whoops, please enter a valid %@ email", school]];
}


@end
