//
//  ProfilePresenter.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/15/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "ProfilePresenter.h"
#import "NetworkManager.h"
#import "SessionManager.h"

@implementation ProfilePresenter

- (void)updateBio:(NSString *)bio completionBlock:(void(^)(AgoraError *error))completionBlock {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:bio, @"bio", nil];
    [NetworkManager updateProfileWithDictionary:dict andCompletionBlock:^(AgoraError *error) {
        completionBlock(error);
    }];
}

- (void)updateLocation:(NSString *)location completionBlock:(void(^)(AgoraError *error))completionBlock {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:location, @"location", nil];
    [NetworkManager updateProfileWithDictionary:dict andCompletionBlock:^(AgoraError *error) {
        completionBlock(error);
    }];
}

- (void)updateName:(NSString *)name completionBlock:(void(^)(AgoraError *error))completionBlock {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:name, @"name", nil];
    [NetworkManager updateProfileWithDictionary:dict andCompletionBlock:^(AgoraError *error) {
        completionBlock(error);
    }];
}

@end
