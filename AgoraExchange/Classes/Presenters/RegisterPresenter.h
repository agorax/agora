//
//  RegisterPresenter.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/24/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AgoraError.h"
#import "User+CoreDataClass.h"

@interface RegisterPresenter : NSObject

-(AgoraError *)validateRegistrationWithEmail:(NSString *)email
                                      password:(NSString *)password
                                retypePassword:(NSString *)retypePassword
                                        school:(NSString *)school;
-(void)registerUserWithEmail:(NSString *)email
                            password:(NSString *)password
                              school:(NSString *)school
                  andCompletionBlock:(void(^)(AgoraError *error))completionBlock;
@end
