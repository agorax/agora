//
//  ItemDetailsPresenter.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/15/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "ItemDetailsPresenter.h"
#import "NetworkManager.h"

@implementation ItemDetailsPresenter

- (void)updatePrice:(int)price onItem:(Item *)item completionBlock:(void(^)(AgoraError *error))completionBlock {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@(price), @"price", @(item.itemId), @"itemId", nil];
    [NetworkManager updateItemWithDictionary:dict andCompletionBlock:^(AgoraError *error) {
        completionBlock(error);
    }];
}

- (void)updateDescription:(NSString *)description onItem:(Item *)item completionBlock:(void(^)(AgoraError *error))completionBlock {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:description, @"itemDescription", @(item.itemId), @"itemId", nil];
    [NetworkManager updateItemWithDictionary:dict andCompletionBlock:^(AgoraError *error) {
        completionBlock(error);
    }];
}

@end
