//
//  ProfilePresenter.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/15/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AgoraError.h"

@interface ProfilePresenter : NSObject

- (void)updateBio:(NSString *)bio completionBlock:(void(^)(AgoraError *error))completionBlock;
- (void)updateLocation:(NSString *)location completionBlock:(void(^)(AgoraError *error))completionBlock;
- (void)updateName:(NSString *)name completionBlock:(void(^)(AgoraError *error))completionBlock;

@end
