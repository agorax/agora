//
//  ItemDetailsPresenter.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/15/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AgoraError.h"
#import "Item+CoreDataClass.h"

@interface ItemDetailsPresenter : NSObject

- (void)updatePrice:(int)price onItem:(Item *)item completionBlock:(void(^)(AgoraError *error))completionBlock;
- (void)updateDescription:(NSString *)description onItem:(Item *)item completionBlock:(void(^)(AgoraError *error))completionBlock;

@end
