//
//  SellPresenter.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/25/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item+CoreDataClass.h"
#import "AgoraError.h"
#import "UIKit/UIKit.h"

@interface SellPresenter : NSObject

- (void)sellItem:(Item *)item images:(NSArray<UIImage *> *)imageArray withCompletionBlock:(void(^)(AgoraError *error))completionBlock;

@end
