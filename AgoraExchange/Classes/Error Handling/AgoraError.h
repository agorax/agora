//
//  AgoraError.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/24/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgoraError : NSError

@property (nonatomic) NSUInteger *statusCode;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *message;

-(AgoraError *)initWithStatusCode:(NSUInteger *)statusCode name:(NSString *)name message:(NSString *)message;

+(AgoraError *)createUnhandledNetworkError;

@end
