//
//  AgoraError.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/24/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "AgoraError.h"

@implementation AgoraError

-(AgoraError *)initWithStatusCode:(NSUInteger *)statusCode name:(NSString *)name message:(NSString *)message
{
    self = [super init];
    if (self) {
        self.statusCode = statusCode;
        self.name = name;
        self.message = message;
    }

    return self;
}

+(AgoraError *)createUnhandledNetworkError {
    AgoraError *error = [[AgoraError alloc] initWithStatusCode:400 name:@"Network Error" message:@"Please check your network settings!"];
    return error;
}

@end
