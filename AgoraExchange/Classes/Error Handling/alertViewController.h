//
//  alertViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/5/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SellStepOneViewController.h"
#import "AgoraError.h"

@interface alertViewController : NSObject

//general alers

+(UIAlertController *)dismissableAlertWithError:(AgoraError *)error;

//login alerts

+ (UIAlertController *)wrongEmailOrPassword;
+ (UIAlertController *)passwordsDontMatch;
+ (UIAlertController *)passwordNotLongEnough;
+ (UIAlertController *)emailNotValid: (NSString *)collegeName;
+ (UIAlertController *)enterValidSchoolEmail: (NSString *)school;
+ (UIAlertController *)userNameAlreadyExists;
+ (UIAlertController *)wrongConfirmationCode;
+ (UIAlertController *)limitExceeded;
+ (UIAlertController *)resetUnsuccesful;
+ (UIAlertController *)successReset;
+ (UIAlertController *)codeSent;
+ (UIAlertController *)emailDoesNotExist;
+ (UIAlertController *)sessionTimeout;
+ (UIAlertController *)tooManyLogins;
+ (UIAlertController *)pleaseVerifyEmail;
+ (UIAlertController *)errorResettingPassword;
+ (UIAlertController *)mustEnterEmail;
+ (UIAlertController *)loginTryAgain;
+ (UIAlertController *)errorLoggingIn;
+ (UIAlertController *)checkNetworkConnection;
+ (UIAlertController *)tryAgainLater;
+ (UIAlertController *)weDoneGoofed;
+ (UIAlertController *)imageError;
+ (UIAlertController *)createAccount: (UIViewController *) lvc;

//Buy Alerts
+ (UIAlertController *)messageSent;
+ (UIAlertController *)sendFailed;
+ (UIAlertController *)existingConversation;
+ (UIAlertController *)searchUnsuccessful;
+ (UIAlertController *)tooManyChats;
+ (UIAlertController *)emptyMessage;
+ (UIAlertController *)messageYourself;
+ (UIAlertController *)likeYourOwnItem;
+ (UIAlertController *)reportYourOwnItem;
+ (UIAlertController *)followYourself;
+ (UIAlertController *)followPeople;
+ (UIAlertController *)errorRetrievingShopListings;
+ (UIAlertController *)errorLoadingTopListings;
+ (UIAlertController *)errorRefreshingFeed;
+ (UIAlertController *)errorFindingUsers;

//sell alerts

+ (UIAlertController *)tooManyPictures;
+ (UIAlertController *)verifyPictureReset: (SellStepOneViewController *) vc;
+ (UIAlertController *)mustAddItemPicture;
+ (UIAlertController *)enterValidPrice;
+ (UIAlertController *)mustEnterName;
+ (UIAlertController *)itemDescriptionTooLong;
+ (UIAlertController *)errorListingItem;
+ (UIAlertController *)freeItem;
+ (UIAlertController *)isbnLength;
+ (UIAlertController *)editionLength;
+ (UIAlertController *)authorLength;

//home alerts

+ (UIAlertController *)bioTooLong;
+ (UIAlertController *)errorUpdatingProfile;
+ (UIAlertController *)errorRetrievingProfile;
+ (UIAlertController *)errorRetrievingListings;
+ (UIAlertController *)wrongPassword;
//+ (UIAlertController *)logoutVerification: (UIViewController *) vc;
+ (UIAlertController *)mustHaveProfilePicture;
+ (UIAlertController *)errorDeletingItem;
+ (UIAlertController *)errorLoadingWishlist;
+ (UIAlertController *)emptyWishlist;
+ (UIAlertController *)noListingsYet;
+ (UIAlertController *)userNoReviews;
+ (UIAlertController *)errorLoadingUsersFollowed;
+ (UIAlertController *)noUsersFollowed;

//review alerts

+ (UIAlertController *)alreadyReviewed;
+ (UIAlertController *)reviewOwnItem;
+ (UIAlertController *)reviewUnsuccessful;
+ (UIAlertController *)reviewTooLong;
+ (UIAlertController *)errorLoadingReviews;
+ (UIAlertController *)noReviews;

//report alers

+ (UIAlertController *)reportVerification: (UIViewController *) vc;
+ (UIAlertController *)reportUnsuccessful;
+ (UIAlertController *)alreadyReported;

//chat alerts
+(UIAlertController *)itemDeleted: (NSMutableArray*)item;
+(UIAlertController *)itemDoesNotExist;
+(UIAlertController *)itemDeletedAndExit: (UINavigationController*)nav;
+(UIAlertController *)connectionError;
+(UIAlertController *)userNotFound;
+(UIAlertController *)conversationDeletedAndExit: (UINavigationController *)nav;
+(UIAlertController *)startWithAMessage;

//update version
+ (UIAlertController *)updateVersion;

@end
