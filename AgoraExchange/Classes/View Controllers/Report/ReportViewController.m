//
//  ReportViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ReportViewController.h"
#import "ReportTableViewCell.h"
#import "pickerData.h"
#import "alertViewController.h"
#import "SVProgressHUD.h"

@interface ReportViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSString *reportReason;

@end

static NSString *cellID = @"cellID";
static NSArray *options = nil;
static int optionHeight = 50;

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    // Do any additional setup after loading the view.
}

- (void) setupTableView
{
    self.tableView.backgroundColor = [UIColor clearColor];
    if (options == nil) {
        options = [pickerData getPickerElementsForScreen:@"Report"];
    }
    [self.tableView registerNib:[UINib nibWithNibName:@"ReportTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
}

#pragma mark - table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [options count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    [cell setupWithOption:[options objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return optionHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.reportReason = [options objectAtIndex:indexPath.row];
    UIAlertController *alert = [alertViewController reportVerification:self];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - make report and segue

- (void)makeReport
{
    [SVProgressHUD show];
    self.navigationItem.hidesBackButton = YES;
    PFQuery *query = [PFQuery queryWithClassName:@"Report"];
    [query whereKey:@"reporterID" equalTo:[PFUser currentUser].objectId];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if (error && error.code != 101)
        {
            self.navigationItem.hidesBackButton = NO;
            [SVProgressHUD dismiss];
            UIAlertController *alert = [alertViewController reportUnsuccessful];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        if (object)
        {
            self.navigationItem.hidesBackButton = NO;
            [SVProgressHUD dismiss];
            UIAlertController *alert = [alertViewController alreadyReported];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        else
        {
            PFObject *report = [PFObject objectWithClassName:@"Report"];
            report[@"reason"] = self.reportReason;
            report[@"itemID"] = self.item.objectId;
            report[@"reporterID"] = [PFUser currentUser].objectId;
            [self.tableView setUserInteractionEnabled:NO];
            [report saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                self.navigationItem.hidesBackButton = NO;
                [SVProgressHUD dismiss];
                if (error)
                {
                    [self.tableView setUserInteractionEnabled:YES];
                    UIAlertController *alert = [alertViewController reportUnsuccessful];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else
                {
                    [self performSegueWithIdentifier:@"Report Completed" sender:self];
                }
            }];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

@end
