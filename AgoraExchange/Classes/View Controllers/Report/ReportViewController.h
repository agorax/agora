//
//  ReportViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ReportViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (void)makeReport;
@property (strong, nonatomic) PFObject *item;

@end
