//
//  ViewProfileViewController.h
//  AgoraX
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "User+CoreDataClass.h"
#import "ProfileViewController.h"

@interface ProfileInfoViewController : UIViewController

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UITextView *bioTextView;
@property (strong, nonatomic) IBOutlet UIButton *reviewButton;
@property (strong, nonatomic) IBOutlet UIButton *shopButton;
@property (strong, nonatomic) IBOutlet UIButton *followButton;
- (IBAction)toggleFollow:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ratingsButton;

@property (strong, nonatomic) IBOutlet UIImageView *firstStarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *secondStarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *thirdStarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *fourthStarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *fifthStarImageView;
@property (strong, nonatomic) IBOutlet UILabel *numberRatings;
@property (strong, nonatomic) IBOutlet UIView *ratingsContainerView;
- (IBAction)viewRatings:(id)sender;

- (void)refreshPage;

@end
