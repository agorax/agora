//
//  InitialMessageViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/10/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <Quickblox/Quickblox.h>
#import "ChatManager.h"
#import "Item+CoreDataClass.h"
#import "User+CoreDataClass.h"
#import "SessionManager.h"

@interface InitialMessageViewController : UIViewController <QBChatDelegate>
@property (strong, nonatomic) IBOutlet UITextView *messageTextView;
@property (strong, nonatomic) IBOutlet UIButton *messageButton;
- (IBAction)send:(id)sender;

@property (strong, nonatomic) User *seller;
@property (strong, nonatomic) Item *item;

@end
