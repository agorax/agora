//
//  ItemDetailsViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/3/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ItemDetailsViewController.h"
#import "dateConverter.h"
#import "ProfileInfoViewController.h"
#import "ReportViewController.h"
#import "InitialMessageViewController.h"
#import "WishlistController.h"
#import "alertViewController.h"
#import "AsyncDisplayKit/ASPINRemoteImageDownloader.h"
#import "PinCache.h"
#import <PINRemoteImage/PINRemoteImageCaching.h>
#import "SessionManager.h"
#import <SCLAlertView_Objective_C/SCLAlertView.h>
#import "FabButton.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "ItemDetailsPresenter.h"
#import "AgoraImageManager.h"

typedef NS_ENUM(NSInteger, optionType) {
    Price,
    Description
};

@interface ItemDetailsViewController () <UIScrollViewDelegate>

@property (strong, nonatomic) User *seller;
@property int numberLikes;
@property (weak, nonatomic) IBOutlet FabButton *fabButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textbookInfoConstraint;
@property (weak, nonatomic) IBOutlet UIView *textbookInfoContainer;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *textbookInfoViews;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *textbookInfoLabels;


@property (weak, nonatomic) IBOutlet UILabel *isbnLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *editionLabel;
@property (strong, nonatomic) SCLAlertView *editFieldSCLAlertView;
@property (strong, nonatomic) SCLAlertView *optionsSCLAlertView;
@property optionType editFieldType;
@property (strong, nonatomic) ItemDetailsPresenter *itemDetailsPresenter;

@end

@implementation ItemDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDataModelChange:)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:nil];
    self.itemDetailsPresenter = [[ItemDetailsPresenter alloc] init];
    [self.viewProfileButton setUserInteractionEnabled:NO];
    [self setupProfileView];
    [self setupButtons];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    self.pageControl.currentPage = 0;
    
    self.pageControl.numberOfPages = self.item.numberOfPictures;
    [self setupLabels];
    [self setupTextbookInfo];
    
    // Do any additional setup after loading the view.
}

#pragma mark - image horizontal paging scroll + page indicator

- (void) setupScrollView
{
    self.scrollView.pagingEnabled = YES;
    self.scrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    self.scrollView.delegate = self;

    for (int i = 0; i < self.item.numberOfPictures; i++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
        NSData *imData = [[[[ASPINRemoteImageDownloader sharedDownloader] sharedPINRemoteImageManager] defaultImageCache] objectFromDiskForKey:[AgoraImageManager urlForItem:self.item atIndex:i + 1]];
        if (imData) {
            imageView.image = [UIImage imageWithData:imData];
        }
        else
        {
            imageView.image = [UIImage imageNamed:@"Image"];
            NSURL *url = [NSURL URLWithString:[AgoraImageManager urlForItem:self.item atIndex:i + 1]];
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            imageView.image = image;
                        });
                    }
                }
            }];
            [task resume];
        }

        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [self.scrollView addSubview:imageView];
        
        imageView.userInteractionEnabled = YES;
        UIButton *expand = [[UIButton alloc] initWithFrame:imageView.bounds];
        [expand addTarget:self
                    action:@selector(expandImage:)
          forControlEvents:UIControlEventTouchUpInside];
        [imageView addSubview:expand];
    }
    self.scrollView.contentSize = CGSizeMake(self.item.numberOfPictures * self.scrollView.frame.size.width, self.scrollView.frame.size.height);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self setupScrollView];
}

- (IBAction)expandImage:(id)sender
{
    CGRect  viewRect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    PFImageView *iv = [[PFImageView alloc] initWithFrame:viewRect];
    iv.backgroundColor = [UIColor colorWithWhite:.5 alpha:.5];
    iv.image = ((UIImageView *)((UIView *)sender).superview).image;
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:iv];
    [self.view bringSubviewToFront:iv];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissFullscreenImage:)];
    [iv addGestureRecognizer:tap];
    iv.userInteractionEnabled = YES;
}

- (void) dismissFullscreenImage: (UITapGestureRecognizer *) sender
{
    [sender.view removeFromSuperview];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = CGRectGetWidth(self.scrollView.frame);
    CGFloat currentPage = floor((self.scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1;
    self.pageControl.currentPage = (int)currentPage;
}

#pragma mark - labels, description, price, and date

- (void)setupLabels
{
    self.navItem.title = self.item.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%d dollars", self.item.price];
    self.dateLabel.text = [dateConverter dateToString:self.item.createdAt];
    self.descriptionTextView.text = self.item.itemDescription;
    if (!self.item.itemDescription || [self.item.itemDescription length] == 0)
    {
        self.descriptionTextView.text = @"No description for this item.";
    }
}

- (void)setupTextbookInfo {
    if ([self.item.category isEqualToString:@"Textbooks"]) {
        [self.textbookInfoConstraint setPriority:999];
        [self.textbookInfoContainer setHidden:NO];
        for (UIView *view in self.textbookInfoViews) {
            [view setHidden:NO];
        }
        for (UILabel *label in self.textbookInfoLabels) {
            [label setHidden:NO];
        }
        self.isbnLabel.text = [NSString stringWithFormat:@"%lld", self.item.isbn];
        self.authorLabel.text = (self.item.author && self.item.author.length != 0) ? self.item.author : @"No author provided.";
        self.editionLabel.text = (self.item.edition) ? [NSString stringWithFormat:@"%hd", self.item.edition] : @"No edition provided.";
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    } else {
        [self.textbookInfoConstraint setPriority:997];
        [self.textbookInfoContainer setHidden:YES];
        for (UIView *view in self.textbookInfoViews) {
            [view setHidden:YES];
        }
        for (UILabel *label in self.textbookInfoLabels) {
            [label setHidden:YES];
        }
    }
}

- (void)setupRating
{
    self.ratingLabel.text = [NSString stringWithFormat:@"(%d)", self.seller.numRatings];
    NSNumber *rating = [NSNumber numberWithInt:self.seller.rating];
    double roundedRating = roundf([rating doubleValue] * 2.0f) / 2.0f;
    if (roundedRating <= .75)
    {
        return;
    }
    if (roundedRating <= 1.25)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 1.75)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    if (roundedRating <= 2.25)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 2.75)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    if (roundedRating <= 3.25)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 3.75)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
        
    }
    if (roundedRating <= 4.25)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 4.75)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
}


- (void)setupButtons
{
    if (self.item.userId == [SessionManager getCurrentUser].userId) {
        [self.fabButton setImage:[UIImage imageNamed:@"PencilFabRed"] forState:UIControlStateNormal];
        self.fabButton.type = EditListing;
    } else {
        self.fabButton.type = FabMessage;
    }
}

#pragma mark - load profile/ setup profile view

- (void)setupProfileView
{
    [self.viewProfileButton addTarget:self
                               action:@selector(unhighlight)
                     forControlEvents:UIControlEventTouchUpInside];
    [self.viewProfileButton addTarget:self
               action:@selector(highlight)
     forControlEvents:UIControlEventTouchDown];
    [self.viewProfileButton addTarget:self
               action:@selector(unhighlight)
     forControlEvents:UIControlEventTouchUpOutside];
    [self.viewProfileButton addTarget:self
               action:@selector(unhighlight)
     forControlEvents:UIControlEventTouchCancel];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"userId == %d", self.item.userId]];
    [request setPredicate:searchPredicate];
    [request setFetchLimit:1];
    ItemDetailsViewController* __weak weakSelf = self;
    NSAsynchronousFetchRequest *asyncRequest = [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request completionBlock:^(NSAsynchronousFetchResult *result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf updateProfileViewWithUser:(User *)[result finalResult].firstObject];
        });
    }];
    [[self privateContext] executeRequest:asyncRequest error:nil];
}

#pragma mark - core data

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[ItemDetailsViewController managedObjectContext]];
    return concurrentContext;
}

- (void)handleDataModelChange:(NSNotification *)note {
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    
    for (NSManagedObject *mob in deletedObjects) {
        if ([mob.entity.name isEqualToString:@"Item"]) {
            if (((Item *)mob).itemId == self.item.itemId) {
                //pop this view controller bruh
                return;
            }
        }
    }
    
    for (NSManagedObject *mob in updatedObjects) {
        if ([mob.entity.name isEqualToString:@"Item"]) {
            if (((Item *)mob).itemId == self.item.itemId) {
                self.item = (Item *)mob;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setupLabels];
                });
                return;
            }
        }
    }
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateProfileViewWithUser:(User *)user {
    if(!user) {
        return;
    }
    
    self.seller = user;
    [self.viewProfileButton setUserInteractionEnabled:YES];
    [self setupRating];
    [self.profilePictureImageView setImage:[UIImage imageNamed:@"Profile Picture Small"]];
    [self loadProfilePicture];
    self.sellerNameLabel.text = user.name;
    if ([self.sellerNameLabel.text length] == 0)
    {
        self.sellerNameLabel.text = user.email;
    }
    self.sellerMemberSinceLabel.text = [dateConverter dateToString:self.seller.createdAt];
    
}

- (void)loadProfilePicture {
    self.profilePictureImageView.layer.cornerRadius = self.profilePictureImageView.bounds.size.height/2.0f;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[AgoraImageManager urlForUser:self.seller]]];
    ItemDetailsViewController* __weak weakSelf = self;
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   if (image) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           weakSelf.profilePictureImageView.image = image;
                                       });
                                   }
                               }
                           }];
}

- (IBAction)viewProfile:(id)sender
{
    ProfileViewController *vc = [[UIStoryboard storyboardWithName:@"ViewProfile" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    vc.user = self.seller;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)highlight
{
    self.profileContainerView.alpha = 0.7f;
}

- (void)unhighlight
{
    self.profileContainerView.alpha = 1.0f;
}

#pragma mark - report methods

- (IBAction)report:(id)sender
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Report Completed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reportCompleted:)
                                                 name:@"Report Completed"
                                               object:nil];
    ReportViewController *vc = [UIStoryboard storyboardWithName:@"Report" bundle:nil].instantiateInitialViewController;
    vc.item = self.item;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) reportCompleted:(NSNotification *) notification
{
    double delayInSeconds = 1.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        [self.navigationController popViewControllerAnimated:YES];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    });
}

#pragma mark - Navigation

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Initial Message"])
    {
        if ([self itemSoldByCurrentUser])
        {
            UIAlertController *alert = [alertViewController messageYourself];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
    }
    return YES;
}

-(BOOL)itemSoldByCurrentUser {
    return self.item.userId == [SessionManager getCurrentUser].userId;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Initial Message"])
    {
        ((InitialMessageViewController *) [segue destinationViewController]).seller = self.seller;
        ((InitialMessageViewController *) [segue destinationViewController]).item = self.item;
    }
}

#pragma mark - fab stuff

- (IBAction)didTapFabButton:(id)sender {
    if (self.fabButton.type == FabMessage) {
        //segue to chat view controller
        ChatViewController *cvc = [[UIStoryboard storyboardWithName:@"Talk" bundle:nil] instantiateViewControllerWithIdentifier:@"ChatViewController"];
        cvc.chatDialog = [self getConversation];
        cvc.didStartNewConversation = (cvc.chatDialog == nil);
        cvc.item = self.item;
        cvc.otherUser = self.seller;
        [self.navigationController pushViewController:cvc animated:YES];
    } else if (self.fabButton.type == EditListing) {
        [self showFabOptionsAlertView];
    }
}

- (Conversation *)getConversation {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
    NSArray *conversations = [Conversation getConversationsWithItemId:self.item.itemId inManagedObjectContext:[ItemDetailsViewController managedObjectContext] andFetchRequest:request];
    return (conversations.count == 0) ? nil : conversations[0];
}

- (void)showFabOptionsAlertView {
    self.optionsSCLAlertView = [[SCLAlertView alloc] init];
    ItemDetailsViewController* __weak weakSelf = self;
    [self.optionsSCLAlertView addButton:@"Edit Price" actionBlock:^(void) {
        [weakSelf showfabEditorAlertView:Price];
    }];
    [self.optionsSCLAlertView addButton:@"Edit Description" actionBlock:^(void) {
        [weakSelf showfabEditorAlertView:Description];
    }];
    [self.optionsSCLAlertView showEdit:self title:@"Edit Listing" subTitle:@"Select a field you would like to edit." closeButtonTitle:@"Cancel" duration:0.0f];

    [self.optionsSCLAlertView showEdit:self title:@"Edit Listing" subTitle:@"Select a field you would like to edit." closeButtonTitle:nil duration:0.0f];
}

- (void)showfabEditorAlertView:(optionType)type {
    NSString *title;
    NSString *subTitle;
    NSString *placeholderText;
    self.editFieldType = type;
    switch (type) {
        case Price:
            title = @"Edit Price";
            subTitle = @"Enter a new price.";
            placeholderText = @"Price in Dollars";
            break;
        case Description:
            title = @"Edit Description";
            subTitle = @"Enter a new description.";
            placeholderText = @"Tell everyone about your item!";
            break;
        default:
            return;
    }
    
    self.editFieldSCLAlertView = [[SCLAlertView alloc] init];
    
    UITextField *textField = [self.editFieldSCLAlertView addTextField:placeholderText];
    
    ItemDetailsViewController* __weak weakSelf = self;
    [self.editFieldSCLAlertView addButton:@"Save" actionBlock:^(void) {
        [weakSelf disableUI];
        switch (weakSelf.editFieldType) {
            case Price: {
                [weakSelf.itemDetailsPresenter updatePrice:[textField.text integerValue] onItem:weakSelf.item completionBlock:^(AgoraError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf enableUI];
                        if (error) {
                            UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                            [weakSelf displayAlert:alert];
                        } else {
                            Item *item = [Item getItemWithId:weakSelf.item.itemId inManagedObjectContext:[ItemDetailsViewController managedObjectContext]];
                            item.price = [textField.text integerValue];
                            [[ItemDetailsViewController managedObjectContext] save:nil];
                        }
                    });
                }];
                break;
            }
            case Description: {
                [weakSelf.itemDetailsPresenter updateDescription:textField.text onItem:weakSelf.item completionBlock:^(AgoraError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf enableUI];
                        if (error) {
                            UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                            [weakSelf displayAlert:alert];
                        } else {
                            Item *item = [Item getItemWithId:weakSelf.item.itemId inManagedObjectContext:[ItemDetailsViewController managedObjectContext]];
                            item.itemDescription = textField.text;
                            [[ItemDetailsViewController managedObjectContext] save:nil];
                        }
                    });
                }];
                break;
            }
            default:
                break;
        }
    }];
    
    [self.editFieldSCLAlertView showEdit:self title:title subTitle:subTitle closeButtonTitle:@"Cancel" duration:0.0f];
}

-(void)enableUI {
    [self.view setUserInteractionEnabled:YES];
    [SVProgressHUD dismiss];
}

-(void)disableUI {
    [self.editFieldSCLAlertView resignFirstResponder];
    [self.view setUserInteractionEnabled:NO];
    [SVProgressHUD show];
}

-(void)displayAlert:(UIAlertController *)alert {
    [self presentViewController:alert animated:YES completion:nil];
}

@end
