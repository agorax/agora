//
//  ViewShopViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/25/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ViewShopViewController.h"
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import <CHTCollectionViewWaterfallLayout/CHTCollectionViewWaterfallLayout.h>
#import "ItemCellNode.h"
#import "SVProgressHUD.h"
#import "AgoraColors.h"
#import "ItemDetailsViewController.h"
#import "alertViewController.h"

@interface ViewShopViewController () <ASCollectionDelegate, ASCollectionDataSource, CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic, readonly) CHTCollectionViewWaterfallLayout *collectionViewLayout;
@property (nonatomic, strong) ASCollectionNode *collectionNode;
@property (nonatomic, strong) NSArray <Item *> *itemsArray;

@end

@implementation ViewShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [AgoraColors agoraGradientColor];
    [self setupCollectionView];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    // Do any additional setup after loading the view.
}

#pragma mark - collection view stuff

- (void)setupCollectionView
{
    self.collectionNode = [[ASCollectionNode alloc] initWithCollectionViewLayout:[CHTCollectionViewWaterfallLayout new]];
    self.collectionNode.dataSource = self;
    self.collectionNode.delegate = self;
    self.collectionNode.backgroundColor = [UIColor clearColor];
    self.collectionNode.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.collectionNode.frame = self.view.bounds;
    [self.view addSubnode:self.collectionNode];
    self.collectionViewLayout.minimumInteritemSpacing = 10;
    self.collectionViewLayout.minimumColumnSpacing = 10;
    self.collectionViewLayout.itemRenderDirection = CHTCollectionViewWaterfallLayoutItemRenderDirectionLeftToRight;
    self.collectionViewLayout.sectionInset = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    [self reloadWishlist];
}

- (CHTCollectionViewWaterfallLayout *)collectionViewLayout {
    return (CHTCollectionViewWaterfallLayout *)self.collectionNode.view.collectionViewLayout;
}

#pragma mark - collection view datasource and delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.itemsArray count];
}

- (ASCellNodeBlock)collectionView:(ASTableView *)tableView nodeBlockForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ASCellNode *(^ASCellNodeBlock)() = ^ASCellNode *() {
        ItemCellNode *node = [ItemCellNode new];
        
        [node setupWithItem:self.itemsArray[indexPath.item]];
        
        return node;
    };
    
    return ASCellNodeBlock;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath {
    return ASSizeRangeMake(CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], 0.0f), CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], CGFLOAT_MAX));
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ItemDetails" bundle:nil];
    ItemDetailsViewController *vc = storyboard.instantiateInitialViewController;
    vc.item = self.itemsArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(CHTCollectionViewWaterfallLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionNode.view calculatedSizeForNodeAtIndexPath:indexPath];
}

- (void)reloadWishlist
{
    if(!self.itemsArray)
    {
        [SVProgressHUD show];
    }
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"userId == %d", self.shopOwner.userId]];
    [request setPredicate:searchPredicate];
    NSAsynchronousFetchRequest *asyncRequest = [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request completionBlock:^(NSAsynchronousFetchResult *result) {
        [SVProgressHUD dismiss];
        if (!result)
        {
            UIAlertController *alert = [alertViewController errorRetrievingShopListings];
            [self presentViewController:alert animated:YES completion:nil];
        }
        self.itemsArray = [[NSMutableArray alloc] initWithArray:[result finalResult]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionNode reloadData];
        });
    }];
    [[self privateContext] executeRequest:asyncRequest error:nil];
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[ViewShopViewController managedObjectContext]];
    return concurrentContext;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

@end
