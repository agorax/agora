//
//  InitialMessageViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/10/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "InitialMessageViewController.h"
#import "alertViewController.h"
#import "SVProgressHUD.h"
#import "MessageSentConfirmationViewController.h"
#import <SSKeychain/SSKeychain.h>

@interface InitialMessageViewController () <DismissViewControllerDelegate>
@property (strong, nonatomic) QBChatDialog *chatDialog;
@property (strong, nonatomic) QBUUser *qbUser;
@end

@implementation InitialMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    [self setupButtons];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // Do any additional setup after loading the view.
}

- (void)setupButtons
{
    self.messageButton.layer.borderWidth = 1;
    self.messageButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark - keyboard stuff

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan && self.messageTextView.isFirstResponder)
    {
        [self.messageTextView resignFirstResponder];
        [self resetViewOffset];
    }
}

- (void)resetViewOffset
{
    int movement = self.view.frame.origin.y;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement);
    }];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    if (0 != self.view.frame.origin.y)
    {
        return;
    }
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    int movement = 0;
    movement = [UIScreen mainScreen].bounds.size.height - self.messageButton.frame.size.height - self.messageButton.frame.origin.y - keyboardSize.height - 20;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [SVProgressHUD dismiss];
}

#pragma mark - send message logic

- (void) sendMessage:(QBChatMessage*)message {//fromUser:(QBUUser *)curUser{
    //WE NEED TEN RESPONSES PER PAGE?
    [QBRequest usersWithLogins:@[self.seller.email] page:[QBGeneralResponsePage responsePageWithCurrentPage:1 perPage:10]
                  successBlock:^(QBResponse *response, QBGeneralResponsePage *page, NSArray *users) {
                      [self createChatDialogToUser:users[0] withMessage:message];
                  } errorBlock:^(QBResponse *response) {
                      UIAlertController *alert = [alertViewController sendFailed];
                      [self presentViewController:alert animated:YES completion:nil];
                      self.messageButton.userInteractionEnabled = YES;
                      self.navigationItem.hidesBackButton = NO;
                      [SVProgressHUD dismiss];
                  }];
}

-(void) createChatDialogToUser: (QBUUser*)sellerQbUser withMessage:(QBChatMessage *)qbMessage{
//    self.chatDialog = [[QBChatDialog alloc] initWithDialogID:nil type:QBChatDialogTypeGroup];
//    NSNumber *sellerId = [NSNumber numberWithUnsignedInteger:sellerQbUser.ID];
//    self.chatDialog.occupantIDs = @[sellerId];
//    self.chatDialog.data = @{@"class_name": @"QBDialogWithItem", @"itemId":self.item.objectID, @"itemName": self.item[@"name"], @"buyerId": [PFUser currentUser].objectId, @"sellerId": self.seller.objectID, @"pictureURLasString": (NSString *)((PFFile *)((NSArray *)self.item[@"pictureFileArray"])[0]).url, @"buyerName": ([[PFUser currentUser][@"name"] isEqualToString:@""]) ? [PFUser currentUser].email : [PFUser currentUser][@"name"], @"sellerName": ([self.seller[@"name"] isEqualToString:@""]) ? self.seller.email : self.seller[@"name"], @"itemDeleted": @false};
//    self.chatDialog.name = self.item.name;
//    [QBRequest createDialog:self.chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
//        [createdDialog joinWithCompletionBlock:^(NSError * _Nullable error) {
//            if (error)
//            {
//                self.messageButton.userInteractionEnabled = YES;
//                self.navigationItem.hidesBackButton = NO;
//                [SVProgressHUD dismiss];
//                UIAlertController *alert = [alertViewController loginTryAgain];
//                [self presentViewController:alert animated:YES completion:nil];
//                
//            }
//            //RACE CONDITION HERE... IS THE OTHER USER JOINED AS WELL? PEOPLE CANT SEE MESSAGES SENT BEFORE THEYVE JOINED
//            
//            if(createdDialog.isJoined) {
//                [createdDialog sendMessage:qbMessage completionBlock:^(NSError * _Nullable error) {
//                    [SVProgressHUD dismiss];
//                    if(error){
//                        self.messageButton.userInteractionEnabled = YES;
//                        self.navigationItem.hidesBackButton = NO;
//                        UIAlertController *alert = [alertViewController sendFailed];
//                        [self presentViewController:alert animated:YES completion:nil];
//                    }
//                    else {
//                        [self sendPushWithText:qbMessage.text];
//                        NSArray *notificationArray = [NSArray arrayWithObjects:qbMessage,createdDialog.ID, nil];
//                        [[NSNotificationCenter defaultCenter] postNotificationName:@"Chats Need Refresh Initial" object:notificationArray];
//                        [self performSegueWithIdentifier:@"Message Sent Confirmation" sender:self];
//                    }
//                }];
//            } else {
//                //EXPLAIN WHAT THIS CASE IS TO ME
//                [QBRequest deleteDialogsWithIDs:[NSSet setWithObject:createdDialog.ID] forAllUsers:YES successBlock:^(QBResponse *response, NSArray *deletedObjectsIDs, NSArray *notFoundObjectsIDs, NSArray *wrongPermissionsObjectsIDs)
//                 {
//                     self.messageButton.userInteractionEnabled = YES;
//                     self.navigationItem.hidesBackButton = NO;
//                     [SVProgressHUD dismiss];
//                     UIAlertController *alert = [alertViewController loginTryAgain];
//                     [self presentViewController:alert animated:YES completion:nil];
//                 } errorBlock:^(QBResponse *response) {
//                     self.messageButton.userInteractionEnabled = YES;
//                     self.navigationItem.hidesBackButton = NO;
//                     [SVProgressHUD dismiss];
//                     UIAlertController *alert = [alertViewController loginTryAgain];
//                     [self presentViewController:alert animated:YES completion:nil];
//                 }];
//                
//            }
//        }];
//        
//    } errorBlock:^(QBResponse *response) {
//        self.messageButton.userInteractionEnabled = YES;
//        self.navigationItem.hidesBackButton = NO;
//        [SVProgressHUD dismiss];
//        UIAlertController *alert = [alertViewController sendFailed];
//        [self presentViewController:alert animated:YES completion:nil];
//    }];
    
}

//UNNECESSARY METHOD?
- (QBChatMessage *)createChatNotificationForGroupChatCreation:(QBChatDialog *)dialog
{
    // create message:
    QBChatMessage *inviteMessage = [QBChatMessage message];
    
    NSMutableDictionary *customParams = [NSMutableDictionary new];
    customParams[@"xmpp_room_jid"] = dialog.roomJID;
    customParams[@"name"] = dialog.name;
    customParams[@"_id"] = dialog.ID;
    customParams[@"type"] = @(dialog.type);
    customParams[@"occupants_ids"] = [dialog.occupantIDs componentsJoinedByString:@","];
    
    // Add notification_type=1 to extra params when you created a group chat
    //
    customParams[@"notification_type"] = @"1";
    
    inviteMessage.customParameters = customParams;
    
    
    return inviteMessage;
}

- (IBAction)send:(id)sender
{
    if ([self.messageTextView.text length] == 0)
    {
        UIAlertController *alert = [alertViewController emptyMessage];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    self.messageButton.userInteractionEnabled = NO;
    self.navigationItem.hidesBackButton = YES;
    [SVProgressHUD showWithStatus:@"Sending"];
    [self connectUsers];
}

- (void) connectUsers
{
//    [[ChatManager sharedInstance] createConversation:self.seller.userId buyer:[SessionManager getCurrentUser].userId item:self.item.itemId message:self.messageTextView.text];
    [SVProgressHUD dismiss];
    self.messageButton.userInteractionEnabled = YES;
    self.navigationItem.hidesBackButton = NO;
//    [[QBChat instance] addDelegate:self];
//    self.qbUser = [QBSession currentSession].currentUser;
//    if(![QBChat instance].isConnected) {
//        _qbUser.password = [SSKeychain passwordForService:@"QBUser" account:@"com.AgoraExchange.keychain"];;
//        [[QBChat instance] connectWithUser:_qbUser completion:^(NSError * _Nullable error) {
//            if (error)
//            {
//                UIAlertController *alert = [alertViewController sendFailed];
//                [self presentViewController:alert animated:YES completion:nil];
//                self.messageButton.userInteractionEnabled = YES;
//                self.navigationItem.hidesBackButton = NO;
//                [SVProgressHUD dismiss];
//            }
//            else
//            {
//                QBChatMessage *qbMessage = [QBChatMessage message];
//                [qbMessage setText:self.messageTextView.text];
//                
//                NSMutableDictionary *params = [NSMutableDictionary dictionary];
//                params[@"save_to_history"] = @YES;
//                PFUser *currentUser = [PFUser currentUser];
//                params[@"displayName"] = ([currentUser[@"name"] isEqualToString:@""]) ? currentUser.email : currentUser[@"name"];
//                [qbMessage setCustomParameters:params];
//                [self sendMessage:qbMessage];
//            }
//        }];
//    } else {
//        QBChatMessage *qbMessage = [QBChatMessage message];
//        [qbMessage setText:self.messageTextView.text];
//        
//        NSMutableDictionary *params = [NSMutableDictionary dictionary];
//        params[@"save_to_history"] = @YES;
//        PFUser *currentUser = [PFUser currentUser];
//        params[@"displayName"] = ([currentUser[@"name"] isEqualToString:@""]) ? currentUser.email : currentUser[@"name"];
//        [qbMessage setCustomParameters:params];
//        [self sendMessage:qbMessage];
//    }
}

- (void)checkForExistingDialog
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    [QBRequest dialogsForPage:page extendedRequest:nil successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        if ([dialogObjects count] >= 99)
        {
            UIAlertController *alert = [alertViewController tooManyChats];
            [self presentViewController:alert animated:YES completion:nil];
            self.messageButton.userInteractionEnabled = YES;
            self.navigationItem.hidesBackButton = NO;
            [SVProgressHUD dismiss];
            return;
        }
        for (int i = 0; i < [dialogObjects count]; i++)
        {
            if ([((NSString *)((QBChatDialog *)dialogObjects[i]).data[@"itemId"]) isEqualToString:self.item.objectID])
            {
                UIAlertController *alert = [alertViewController existingConversation];
                [self presentViewController:alert animated:YES completion:nil];
                self.messageButton.userInteractionEnabled = YES;
                self.navigationItem.hidesBackButton = NO;
                [SVProgressHUD dismiss];
                return;
            }
        }
        [self connectUsers];
    } errorBlock:^(QBResponse *response) {
        UIAlertController *alert = [alertViewController sendFailed];
        [self presentViewController:alert animated:YES completion:nil];
        self.messageButton.userInteractionEnabled = YES;
        self.navigationItem.hidesBackButton = NO;
        [SVProgressHUD dismiss];
    }];
    
}

#pragma mark - push notifications

- (void) sendPushWithText: (NSString *) text
{
    if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    NSMutableDictionary *aps = [NSMutableDictionary dictionary];
    [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
    
    NSString *name;
    if (![PFUser currentUser][@"name"] || [[PFUser currentUser][@"name"] length] == 0)
    {
        name = [PFUser currentUser][@"email"];
    }
    else
    {
        name = [PFUser currentUser][@"name"];
    }
    
    [aps setObject:name forKey:@"name"];
    
    NSString *message;
    if ([text isEqualToString:@"Sent you a picture!"])
    {
        message = [NSString stringWithFormat:@"%@ sent you a picture!", name];
        [aps setObject:@"YES" forKey:@"picture"];
    }
    else
    {
        message = [NSString stringWithFormat:@"%@: %@", name, text];
    }
    [aps setObject:message forKey:QBMPushMessageAlertKey];
    
    [payload setObject:aps forKey:QBMPushMessageApsKey];
    
    QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
    
    NSMutableArray <NSString *> *userIds = [[NSMutableArray alloc] initWithCapacity:self.chatDialog.occupantIDs.count];
    
    if (self.chatDialog.occupantIDs[0].integerValue == [QBSession currentSession].currentUser.ID)
    {
        [userIds addObject:self.chatDialog.occupantIDs[1].stringValue];
    }
    else{
        [userIds addObject:self.chatDialog.occupantIDs[0].stringValue];
    }
    
    
    [QBRequest sendPush:pushMessage toUsers:userIds successBlock:^(QBResponse *response, QBMEvent *event) {
        // Successful response with event
    } errorBlock:^(QBError *error) {
        // Handle error
    }];
}


#pragma mark - dismiss delegate

- (void)dismissVC
{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Message Sent Confirmation"])
    {
        ((MessageSentConfirmationViewController *) [segue destinationViewController]).delegate = self;
    }
}

@end
