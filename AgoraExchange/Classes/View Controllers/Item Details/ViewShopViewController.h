//
//  ViewShopViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/25/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+CoreDataClass.h"

@interface ViewShopViewController : UIViewController

@property (strong, nonatomic) User *shopOwner;

@end
