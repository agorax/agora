//
//  ViewProfileViewController.m
//  AgoraX
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ProfileInfoViewController.h"
#import "ViewReviewsViewController.h"
#import "ViewShopViewController.h"
#import "FollowController.h"
#import "alertViewController.h"
#import "SessionManager.h"

@interface ProfileInfoViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation ProfileInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    [self refreshPage];
    [self setupButtons];
    [self setupPicture];
    [self setupRefreshControl];
}

- (void)refreshPage {
    if (!self.user) {
        self.user = [SessionManager getCurrentUser];
    }
    
    [self setupLabels];
    [self setupRating];
}

- (void)refreshPage:(UIRefreshControl *)refreshControl {
    [self refreshPage];
    [refreshControl endRefreshing];
}

- (void)setupRefreshControl {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshPage:) forControlEvents:UIControlEventValueChanged];
    [self.myScrollView addSubview:refreshControl];
}

- (void)setupPicture
{
    self.profilePictureImageView.layer.cornerRadius = self.profilePictureImageView.frame.size.height/2.0;
}

- (void)setupButtons
{
    self.reviewButton.layer.borderWidth = 1;
    self.reviewButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.followButton.layer.borderWidth = 1;
    self.followButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.shopButton.layer.borderWidth = 1;
    self.shopButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self.ratingsButton addTarget:self
                               action:@selector(unhighlight)
                     forControlEvents:UIControlEventTouchUpInside];
    [self.ratingsButton addTarget:self
                               action:@selector(highlight)
                     forControlEvents:UIControlEventTouchDown];
    [self.ratingsButton addTarget:self
                               action:@selector(unhighlight)
                     forControlEvents:UIControlEventTouchUpOutside];
    [self.ratingsButton addTarget:self
                               action:@selector(unhighlight)
                     forControlEvents:UIControlEventTouchCancel];
}

- (void)setupRating
{
    self.numberRatings.text = [NSString stringWithFormat:@"(%d)", self.user.numRatings];
    NSNumber *rating = [NSNumber numberWithFloat: self.user.rating];
    double roundedRating = roundf([rating doubleValue] * 2.0f) / 2.0f;

    if (roundedRating <= .75)
    {
        return; 
    }
    if (roundedRating <= 1.25)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 1.75)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    if (roundedRating <= 2.25)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 2.75)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    if (roundedRating <= 3.25)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 3.75)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
        
    }
    if (roundedRating <= 4.25)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 4.75)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
}

- (IBAction)viewRatings:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Review" bundle:nil];
    ViewReviewsViewController *vc = storyboard.instantiateInitialViewController;
    vc.user = self.user;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)highlight
{
    self.ratingsContainerView.alpha = 0.7f;
}

- (void)unhighlight
{
    self.ratingsContainerView.alpha = 1.0f;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if ([(NSArray <NSString *> *)[PFUser currentUser][@"followingArray"] containsObject:self.user.objectId])
//    {
//        [self.followButton setTitle:@"Un-Follow" forState:UIControlStateNormal];
//    }
}

- (void)setupLabels
{
    self.emailLabel.text = self.user.email;
    self.navigationItem.title = self.user.name;
    if ([self.navigationItem.title length] == 0)
    {
        self.navigationItem.title = @"No Name";
    }
    self.locationLabel.text = self.user.location;
    if ([self.locationLabel.text length] == 0)
    {
        self.locationLabel.text = @"None Specified";
    }
    self.bioTextView.text = self.user.bio;
    if ([self.bioTextView.text length] == 0)
    {
        [self.bioTextView setText:@"This user does not have a bio yet!"];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"View Listings"])
    {
        ((ViewShopViewController *)segue.destinationViewController).shopOwner = self.user;
    }
}


- (IBAction)toggleFollow:(id)sender
{
//    if ([self.user.objectId isEqualToString:[PFUser currentUser].objectId])
//    {
//        UIAlertController *alert = [alertViewController followYourself];
//        [self presentViewController:alert animated:YES completion:nil];
//        return;
//    }
//    if ([self.followButton.titleLabel.text isEqualToString:@"Follow"])
//    {
//        [sender setTitle:@"Un-Follow" forState:UIControlStateNormal];
//        [FollowController followUserWithObjectId:self.user.objectId];
//    }
//    else
//    {
//        [sender setTitle:@"Follow" forState:UIControlStateNormal];
//        [FollowController unfollowUserWithObjectId:self.user.objectId];
//    }
}

#pragma mark - core data shit

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[ProfileInfoViewController managedObjectContext]];
    return concurrentContext;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
