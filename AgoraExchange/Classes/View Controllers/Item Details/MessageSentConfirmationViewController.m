//
//  MessageSentConfirmationViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/12/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MessageSentConfirmationViewController.h"

@interface MessageSentConfirmationViewController ()

@end

@implementation MessageSentConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismiss:(id)sender
{
    [self.delegate dismissVC];
}
@end
