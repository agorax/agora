//
//  ProfileListingsViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/1/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "ProfileListingsViewController.h"
#import "Item+CoreDataClass.h"
#import "MyListingsTableViewCell.h"
#import "ItemDetailsViewController.h"
#import "dateConverter.h"
#import "SessionManager.h"
#import "ProfileListingsPresenter.h"
#import "AsyncDisplayKit/ASPINRemoteImageDownloader.h"
#import "PinCache.h"
#import <PINRemoteImage/PINRemoteImageCaching.h>
#import "AgoraImageManager.h"

@interface ProfileListingsViewController () <UITableViewDelegate, UITableViewDataSource, ListingTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *PlaceholderLabel;
@property (strong ,nonatomic) NSMutableArray <Item *>*items;
@property (strong, nonatomic) ProfileListingsPresenter *profileListingsPresenter;

@property (strong, nonatomic) NSMutableSet <NSNumber *>*selectedItemIds;

@end

static NSString *cellID = @"cellID";
static int cellHeight = 85;

@implementation ProfileListingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDataModelChange:)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:nil];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    [self.tableView setSeparatorColor:[UIColor whiteColor]];
    self.selectedItemIds = [[NSMutableSet alloc] initWithCapacity:self.items.count];
    self.profileListingsPresenter = [[ProfileListingsPresenter alloc] init];
    [self setupTableView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupTableView
{
    [self.tableView setBackgroundView:nil];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView registerNib:[UINib nibWithNibName:@"MyListingsTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshListings:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self refreshListings];
}

- (void)deleteSelectedItems {
    [self.profileListingsPresenter deleteItems:self.selectedItemIds.allObjects];
    [self unselectAllItems];
}

- (void)unselectAllItems {
    [self.selectedItemIds removeAllObjects];
    [self.tableView reloadData];
}

- (void)refreshListings:(UIRefreshControl *)refreshControl {
    [self refreshListings];
    [refreshControl endRefreshing];
}

- (void)refreshListings {
    [self.items removeAllObjects];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"userId == %d", self.user.userId];
    [request setPredicate:searchPredicate];
    ProfileListingsViewController* __weak weakSelf = self;
    NSAsynchronousFetchRequest *asyncRequest = [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request completionBlock:^(NSAsynchronousFetchResult *results) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.items = [NSMutableArray arrayWithArray:[results finalResult]];
            if (!weakSelf.items || [weakSelf.items count] == 0) {
                [weakSelf.PlaceholderLabel setHidden:NO];
                [weakSelf.tableView setHidden:YES];
            } else {
                [weakSelf.PlaceholderLabel setHidden:YES];
                [weakSelf.tableView setHidden:NO];
                [weakSelf.tableView reloadData];
            }
        });
    }];
    [[self privateContext] executeRequest:asyncRequest error:nil];
}

#pragma mark - Table view data source & delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyListingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.nameLabel.text = self.items[indexPath.row].name;
    cell.dateLabel.text = [dateConverter dateToString:self.items[indexPath.row].createdAt];
    cell.priceLabel.text = [NSString stringWithFormat:@"$%d", self.items[indexPath.row].price];
    cell.itemId = self.items[indexPath.row].itemId;
    cell.delegate = self;
    cell.checkbox.on = [self.selectedItemIds containsObject:@(self.items[indexPath.row].itemId)];
    [cell.checkbox setHidden:[SessionManager getCurrentUser].userId != self.user.userId];
    ProfileListingsViewController* __weak weakSelf = self;
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *imData = [[[[ASPINRemoteImageDownloader sharedDownloader] sharedPINRemoteImageManager] defaultImageCache] objectFromDiskForKey:[AgoraImageManager urlForItem:weakSelf.items[indexPath.row] atIndex:1]];
        if (imData) {
            UIImage *im = [UIImage imageWithData:imData];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.picture.image = im;
            });
        } else {
            NSURL *url = [NSURL URLWithString:[AgoraImageManager urlForItem:weakSelf.items[indexPath.row] atIndex:1]];
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            cell.picture.image = image;
                        });
                    }
                }
            }];
            [task resume];
        }
    });
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ItemDetails" bundle:nil];
    ItemDetailsViewController *vc = storyboard.instantiateInitialViewController;
    vc.item = self.items[indexPath.row];
    [self.realNavigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return CGFLOAT_MIN;
    return tableView.sectionHeaderHeight;
}

#pragma mark - core data shit

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[ProfileListingsViewController managedObjectContext]];
    return concurrentContext;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)handleDataModelChange:(NSNotification *)note {
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    for (NSManagedObject *mob in insertedObjects) {
        if ([mob.entity.name isEqualToString:@"Item"]) {
            if (((Item *)mob).userId == self.user.userId) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self refreshListings];
                });
                return;
            }
        }
    }
    
    for (NSManagedObject *mob in deletedObjects) {
        if ([mob.entity.name isEqualToString:@"Item"]) {
            if (((Item *)mob).userId == self.user.userId) {
                [self refreshListings];
                return;
            }
        }
    }
    
    for (NSManagedObject *mob in updatedObjects) {
        if ([mob.entity.name isEqualToString:@"Item"]) {
            if (((Item *)mob).userId == self.user.userId) {
                [self refreshListings];
                return;
            }
        }
    }
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - listingtableviewcell delegate

-(void)checkboxDidChangeTo:(BOOL)checked onItemId:(int)itemId {
    if (checked) {
        [self.selectedItemIds addObject:@(itemId)];
    } else {
        [self.selectedItemIds removeObject:@(itemId)];
    }
}

@end
