//
//  ProfileReviewsViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/9/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+CoreDataClass.h"
#import "ProfileViewController.h"
#import "Review+CoreDataClass.h"

@interface ProfileReviewsViewController : UIViewController

@property (strong, nonatomic) User *user;
@property (weak, nonatomic) UINavigationController *realNavigationController;
@property (strong, nonatomic) NSMutableArray <Review *>*reviews;

- (BOOL)currentUserAlreadyReviewedUser;
- (void)loadReviews;

@end
