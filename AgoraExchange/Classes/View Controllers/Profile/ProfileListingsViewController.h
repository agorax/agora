//
//  ProfileListingsViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/1/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+CoreDataClass.h"
#import "ProfileViewController.h"

@interface ProfileListingsViewController : UIViewController

@property (strong, nonatomic) User *user;
@property (weak, nonatomic) UINavigationController *realNavigationController;

- (void)deleteSelectedItems;
- (void)unselectAllItems;

- (void)refreshListings;

@end
