//
//  ProfileViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/28/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+CoreDataClass.h"

@interface ProfileViewController : UIViewController

@property (strong, nonatomic) User *user;

@end
