//
//  ProfileViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/28/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "ProfileViewController.h"
#import <PageMenu/PageMenu-Swift.h>
#import "ProfileInfoViewController.h"
#import "ProfileListingsViewController.h"
#import "ProfileReviewsViewController.h"
#import <SCLAlertView_Objective_C/SCLAlertView.h>
#import "FabButton.h"
#import "SessionManager.h"
#import "ProfilePresenter.h"
#import "alertViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AddReviewViewController.h"
#import "camera.h"
#import "AgoraImageManager.h"

typedef NS_ENUM(NSInteger, optionType) {
    Location,
    Bio,
    Name,
    Picture,
    DeleteListings
};

@interface ProfileViewController () <UIScrollViewDelegate, CAPSPageMenuDelegate, AddedReviewControllerDelegate, ImageCropViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *tabBarView;
@property (strong, nonatomic) CAPSPageMenu *pageController;
@property (weak, nonatomic) ProfileInfoViewController *profileInfoViewController;
@property (weak, nonatomic) ProfileListingsViewController *listingsViewController;
@property (weak, nonatomic) ProfileReviewsViewController *reviewsViewController;

@property (weak, nonatomic) IBOutlet FabButton *fabButton;
@property (strong, nonatomic) SCLAlertView *editFieldSCLAlertView;
@property (strong, nonatomic) SCLAlertView *pictureOptionsAlertView;
@property (strong, nonatomic) SCLAlertView *optionsSCLAlertView;
@property (strong, nonatomic) SCLAlertView *settingsSCLAlertView;
@property (strong, nonatomic) ProfilePresenter *profilePresenter;
@property optionType editFieldType;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.profilePresenter = [[ProfilePresenter alloc] init];
    if (!self.user) {
        self.user = [SessionManager getCurrentUser];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDataModelChange:)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:nil];
    self.navigationItem.title = self.user.name;
    [self configureFabForIndex:0];
    [self loadProfilePicture];
    [self setupNavigationBar];
    // Do any additional setup after loading the view.
}

- (void)setupNavigationBar {
    if ([SessionManager getCurrentUser].userId == self.user.userId) {
        UIImage* image3 = [UIImage imageNamed:@"Settings"];
        CGRect frameimg = CGRectMake(0, 0, self.navigationController.navigationBar.bounds.size.height - 8, self.navigationController.navigationBar.bounds.size.height - 8);
        UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
        [someButton setImage:image3 forState:UIControlStateNormal];
        [someButton addTarget:self action:@selector(displaySettingsAlert:)
             forControlEvents:UIControlEventTouchUpInside];
        [someButton setShowsTouchWhenHighlighted:YES];
        
        UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
        self.navigationItem.rightBarButtonItem=mailbutton;
    }
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void) displaySettingsAlert:(id)sender {
    self.settingsSCLAlertView = [[SCLAlertView alloc] init];
    [self.settingsSCLAlertView setShouldDismissOnTapOutside:YES];
    ProfileViewController* __weak weakSelf = self;
    [self.settingsSCLAlertView addButton:@"About Agora" actionBlock:^(void) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"About"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    [self.settingsSCLAlertView addButton:@"Terms & Conditions" actionBlock:^(void) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Terms And Conditions"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    [self.settingsSCLAlertView addButton:@"Privacy Policy" actionBlock:^(void) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Privacy Policy"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    [self.settingsSCLAlertView addButton:@"Logout" actionBlock:^(void) {
        [SessionManager logout];
    }];
    [self.settingsSCLAlertView showNotice:self title:@"Settings" subTitle:nil closeButtonTitle:@"Cancel" duration:0.0f];
    [self.settingsSCLAlertView showNotice:self title:@"Settings" subTitle:nil closeButtonTitle:nil duration:0.0f];
}

- (void)loadProfilePicture {
    ProfileViewController* __weak weakSelf = self;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[AgoraImageManager urlForUser:self.user]]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   if (image) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           weakSelf.profileInfoViewController.profilePictureImageView.image = image;
                                       });
                                   }
                               }
                           }];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (!self.pageController) {
        [self setupPageController];
    }
}

- (void)configureFabForIndex:(int)index {
    if (index == 0) {
        if (self.user.userId == [SessionManager getCurrentUser].userId) {
            [self.fabButton setFabType:EditProfile];
        } else {
            [self.fabButton setFabType:None];
        }
    } else if (index == 1) {
        if (self.user.userId == [SessionManager getCurrentUser].userId) {
            [self.fabButton setFabType:EditListings];
        } else {
            [self.fabButton setFabType:None];
        }
    } else if (index == 2) {
        if (self.user.userId == [SessionManager getCurrentUser].userId) {
            [self.fabButton setFabType:None];
        } else {
            [self.fabButton setFabType:AddReview];
        }
    }
}

- (void)setupPageController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ViewProfile" bundle:nil];
    self.profileInfoViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileInfoViewController"];
    self.profileInfoViewController.title = @"Info";
    self.profileInfoViewController.user = self.user;
    
    self.listingsViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileListingsViewController"];
    self.listingsViewController.title = @"Listings";
    self.listingsViewController.user = self.user;
    self.listingsViewController.realNavigationController = self.navigationController;
    
    self.reviewsViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileReviewsViewController"];
    self.reviewsViewController.title = @"Reviews";
    self.reviewsViewController.user = self.user;
    self.reviewsViewController.realNavigationController = self.navigationController;
    
    NSArray <UIViewController *>*viewControllers = @[self.profileInfoViewController, self.listingsViewController, self.reviewsViewController];

    self.pageController = [[CAPSPageMenu alloc] initWithViewControllers:viewControllers frame:self.tabBarView.frame];
    [self.pageController setDelegate:self];
    [self.view insertSubview:self.pageController.view aboveSubview:self.tabBarView];
}

-(void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    [self configureFabForIndex:self.pageController.currentPageIndex];
}

#pragma mark - fab stuff

- (IBAction)didTapFabButton:(id)sender {
    if (self.fabButton.type == EditListings) {
        [self showFabOptionsAlertView:EditListings];
    } else if (self.fabButton.type == EditProfile) {
        [self showFabOptionsAlertView:EditProfile];
    } else if (self.fabButton.type == AddReview) {
        AddReviewViewController *vc = [[UIStoryboard storyboardWithName:@"Review" bundle:nil] instantiateViewControllerWithIdentifier:@"AddReview"];
        vc.user = self.user;
        vc.alreadyReviewed = [self.reviewsViewController currentUserAlreadyReviewedUser];
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)showFabOptionsAlertView:(fabType)type {
    self.optionsSCLAlertView = [[SCLAlertView alloc] init];
    [self.optionsSCLAlertView setShouldDismissOnTapOutside:YES];
    ProfileViewController* __weak weakSelf = self;
    if (type == EditProfile) {
        [self.optionsSCLAlertView addButton:@"Edit Name" actionBlock:^(void) {
            [weakSelf showfabEditorAlertView:Name];
        }];
        [self.optionsSCLAlertView addButton:@"Edit Location" actionBlock:^(void) {
            [weakSelf showfabEditorAlertView:Location];
        }];
        [self.optionsSCLAlertView addButton:@"Edit Bio" actionBlock:^(void) {
            [weakSelf showfabEditorAlertView:Bio];
        }];
        [self.optionsSCLAlertView addButton:@"Edit Profile Picture" actionBlock:^(void) {
            [weakSelf showEditPictureAlertView];
        }];
        [self.optionsSCLAlertView showEdit:self title:@"Edit Profile" subTitle:@"Select a field you would like to edit." closeButtonTitle:@"Cancel" duration:0.0f];
        
        [self.optionsSCLAlertView showEdit:self title:@"Edit Profile" subTitle:@"Select a field you would like to edit." closeButtonTitle:nil duration:0.0f];
    } else if (type == EditListings) {
        [self.optionsSCLAlertView addButton:@"Delete" actionBlock:^(void) {
            [weakSelf.listingsViewController deleteSelectedItems];
        }];
        [self.optionsSCLAlertView addButton:@"Unselect All" actionBlock:^(void) {
            [weakSelf.listingsViewController unselectAllItems];
        }];
        [self.optionsSCLAlertView showEdit:self title:@"Edit Listings" subTitle:@"Select an action on the selected items." closeButtonTitle:@"Cancel" duration:0.0f];
        
        [self.optionsSCLAlertView showEdit:self title:@"Edit Listings" subTitle:@"Select an action on the selected items." closeButtonTitle:nil duration:0.0f];
    }

}

- (void)showEditPictureAlertView {
    self.pictureOptionsAlertView = [[SCLAlertView alloc] init];
    [self.pictureOptionsAlertView setShouldDismissOnTapOutside:YES];
    ProfileViewController* __weak weakSelf = self;

    [self.pictureOptionsAlertView addButton:@"Take Photo" actionBlock:^(void) {
        [weakSelf presentViewController:[camera takePhoto:weakSelf] animated:YES completion:NULL];
    }];
    [self.pictureOptionsAlertView addButton:@"Choose Existing" actionBlock:^(void) {
        [weakSelf presentViewController:[camera chooseExisting:weakSelf] animated:YES completion:NULL];
    }];
    [self.pictureOptionsAlertView showEdit:self title:@"Edit Profile Picture" subTitle:@"Choose a new photo." closeButtonTitle:@"Cancel" duration:0.0f];
    
    [self.pictureOptionsAlertView showEdit:self title:@"Edit Profile" subTitle:@"Select a field you would like to edit." closeButtonTitle:nil duration:0.0f];

}

- (void)showfabEditorAlertView:(optionType)type {
    NSString *title;
    NSString *subTitle;
    NSString *placeholderText;
    
    self.editFieldType = type;
    switch (type) {
        case Location:
            title = @"Edit Location";
            subTitle = @"Enter a new meetup-location.";
            placeholderText = @"KSL, Nord, etc.";
            break;
        case Bio:
            title = @"Edit Bio";
            subTitle = @"Enter a new bio.";
            placeholderText = @"Tell everyone about yourself!";
            break;
        case Name:
            title = @"Edit Name";
            subTitle = @"Enter a new name.";
            placeholderText = @"What name do you want to go by?";
            break;
        default:
            return;
    }
    
    self.editFieldSCLAlertView = [[SCLAlertView alloc] init];
    [self.editFieldSCLAlertView setShouldDismissOnTapOutside:YES];
    
    UITextField *textField = [self.editFieldSCLAlertView addTextField:placeholderText];
    
    ProfileViewController* __weak weakSelf = self;
    [self.editFieldSCLAlertView addButton:@"Save" actionBlock:^(void) {
        [weakSelf disableUI];
        switch (weakSelf.editFieldType) {
            case Location: {
                [weakSelf.profilePresenter updateLocation:textField.text completionBlock:^(AgoraError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf enableUI];
                        if (error) {
                            UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                            [weakSelf displayAlert:alert];
                        } else {
                            User *user = [User getUserWithId:weakSelf.user.userId inManagedObjectContext:[ProfileViewController managedObjectContext]];
                            user.location = textField.text;
                            [[ProfileViewController managedObjectContext] save:nil];
                            weakSelf.profileInfoViewController.locationLabel.text = textField.text;
                        }
                    });
                }];
                break;
            }
            case Bio: {
                [weakSelf.profilePresenter updateBio:textField.text completionBlock:^(AgoraError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf enableUI];
                        if (error) {
                            UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                            [weakSelf displayAlert:alert];
                        } else {
                            User *user = [User getUserWithId:weakSelf.user.userId inManagedObjectContext:[ProfileViewController managedObjectContext]];
                            user.bio = textField.text;
                            [[ProfileViewController managedObjectContext] save:nil];
                            weakSelf.profileInfoViewController.bioTextView.text = textField.text;
                        }
                    });
                }];
            }
            case Name: {
                [weakSelf.profilePresenter updateName:textField.text completionBlock:^(AgoraError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf enableUI];
                        if (error) {
                            UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                            [weakSelf displayAlert:alert];
                        } else {
                            User *user = [User getUserWithId:weakSelf.user.userId inManagedObjectContext:[ProfileViewController managedObjectContext]];
                            user.name = textField.text;
                            [[ProfileViewController managedObjectContext] save:nil];
                            weakSelf.navigationItem.title = textField.text;
                        }
                    });
                }];
            }
            default:
                break;
        }
    }];
    
    [self.editFieldSCLAlertView showEdit:self title:title subTitle:subTitle closeButtonTitle:@"Cancel" duration:0.0f];
}

-(void)enableUI {
    [self.view setUserInteractionEnabled:YES];
    [SVProgressHUD dismiss];
}

-(void)disableUI {
    [self.editFieldSCLAlertView resignFirstResponder];
    [self.view setUserInteractionEnabled:NO];
    [SVProgressHUD show];
}

-(void)displayAlert:(UIAlertController *)alert {
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - camera stuff

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:info[UIImagePickerControllerOriginalImage]];
    controller.delegate = self;
    controller.blurredBackground = NO;
    controller.hidesBottomBarWhenPushed = YES;
    [[self navigationController] pushViewController:controller animated:YES];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)ImageCropViewControllerSuccess:(UIViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    [AgoraImageManager uploadToS3:croppedImage onUser:self.user];
    self.profileInfoViewController.profilePictureImageView.image = croppedImage;
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)ImageCropViewControllerDidCancel:(UIViewController *)controller {
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark - review added delegate

-(void)reviewAdded {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - core data stuff

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[ProfileViewController managedObjectContext]];
    return concurrentContext;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)handleDataModelChange:(NSNotification *)note {
    if (!self.user) {
        self.user = [SessionManager getCurrentUser];
    }
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    for (NSManagedObject *mob in updatedObjects) {
        if ([mob.entity.name isEqualToString:@"User"]) {
            if (((User *)mob).userId == self.user.userId) {
                self.user = (User *)mob;
                self.profileInfoViewController.user = (User *)mob;
                self.listingsViewController.user = (User *)mob;
                self.reviewsViewController.user = (User *)mob;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.profileInfoViewController refreshPage];
                    [self.listingsViewController refreshListings];
                    [self.reviewsViewController loadReviews];
                });
                return;
            }
        }
    }
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
