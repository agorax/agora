//
//  ProfileReviewsViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/9/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "ProfileReviewsViewController.h"
#import "ItemDetailsViewController.h"
#import "ReviewTableViewCell.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "dateConverter.h"
#import "SessionManager.h"

@interface ProfileReviewsViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;

@end

static NSString *cellID = @"cellID";

@implementation ProfileReviewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDataModelChange:)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:nil];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    [self setupTableView];
    [self loadReviews];
    // Do any additional setup after loading the view.
}

- (void)loadReviews:(UIRefreshControl *)refreshControl {
    [self loadReviews];
    [refreshControl endRefreshing];
}

- (void)loadReviews
{
    [self.reviews removeAllObjects];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Review"];
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"reviewedUserId == %d", self.user.userId];
    [request setPredicate:searchPredicate];
    ProfileReviewsViewController* __weak weakSelf = self;
    NSAsynchronousFetchRequest *asyncRequest = [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request completionBlock:^(NSAsynchronousFetchResult *results) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.reviews = [NSMutableArray arrayWithArray:[results finalResult]];
            if (!weakSelf.reviews || [weakSelf.reviews count] == 0) {
                [weakSelf.placeholderLabel setHidden:NO];
                [weakSelf.tableView setHidden:YES];
            } else {
                [weakSelf.placeholderLabel setHidden:YES];
                [weakSelf.tableView setHidden:NO];
                [weakSelf.tableView reloadData];
            }
        });
    }];
    [[self privateContext] executeRequest:asyncRequest error:nil];
}

- (BOOL)currentUserAlreadyReviewedUser {
    for (Review *review in self.reviews) {
        if (review.reviewerUserId == [SessionManager getCurrentUser].userId) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - table view setup

- (void)setupTableView
{
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"ReviewTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
    self.tableView.estimatedRowHeight = 150;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(loadReviews:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

- (void)setupRating:(ReviewTableViewCell *)cell onReview:(Review *)review
{
    CGFloat roundedRating = review.rating;
    if (roundedRating <= .75)
    {
        return;
    }
    if (roundedRating <= 1.25)
    {
        cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 1.75)
    {
        cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    if (roundedRating <= 2.25)
    {
        cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 2.75)
    {
        cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    if (roundedRating <= 3.25)
    {
        cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 3.75)
    {
        cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
        
    }
    if (roundedRating <= 4.25)
    {
        cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        return;
    }
    if (roundedRating <= 4.75)
    {
        cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        cell.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Half Star"];
        return;
    }
    cell.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    cell.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    cell.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    cell.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    cell.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
}

- (void)setupUser:(ReviewTableViewCell *)cell onReview:(Review *)review {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"userId == %d", review.reviewerUserId];
    [request setPredicate:searchPredicate];
    [request setFetchLimit:1];
    NSAsynchronousFetchRequest *asyncRequest = [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request completionBlock:^(NSAsynchronousFetchResult *results) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([results finalResult] && [[results finalResult] firstObject]) {
                cell.reviewer = ((User *)[[results finalResult] firstObject]);
                if (!((User *)[[results finalResult] firstObject]).name || [((User *)[[results finalResult] firstObject]).name length] == 0) {
                    cell.reviewerLabel.text = ((User *)[[results finalResult] firstObject]).email;
                } else {
                    cell.reviewerLabel.text = ((User *)[[results finalResult] firstObject]).name;
                }
            }
        });
    }];
    [[self privateContext] executeRequest:asyncRequest error:nil];
}

#pragma mark - table view delegate & datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.reviews count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.ratingText.text = ([[self.reviews objectAtIndex:indexPath.row].reviewDescription length] > 0) ? [self.reviews objectAtIndex:indexPath.row].reviewDescription : @"No comment.";
    [self setupRating:cell onReview:[self.reviews objectAtIndex:indexPath.row]];
    [self setupUser:cell onReview:[self.reviews objectAtIndex:indexPath.row]];
    cell.reviewDateLabel.text = [dateConverter dateToString:self.reviews[indexPath.row].createdAt];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return CGFLOAT_MIN;
    return tableView.sectionHeaderHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ProfileViewController *vc = [[UIStoryboard storyboardWithName:@"ViewProfile" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    vc.user = ((ReviewTableViewCell *)[tableView cellForRowAtIndexPath:indexPath]).reviewer;
    [self.realNavigationController pushViewController:vc animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

#pragma mark - core data shit

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[ProfileReviewsViewController managedObjectContext]];
    return concurrentContext;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)handleDataModelChange:(NSNotification *)note {
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    for (NSManagedObject *mob in insertedObjects) {
        if ([mob.entity.name isEqualToString:@"Review"]) {
            if (((Review *)mob).reviewedUserId == self.user.userId) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self loadReviews];
                });
                return;
            }
        }
    }
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
