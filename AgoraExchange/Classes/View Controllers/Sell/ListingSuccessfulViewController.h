//
//  ListingSuccessfulViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/19/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissViewControllerDelegate <NSObject>

- (void)dismissVC;

@end

@interface ListingSuccessfulViewController : UIViewController

@property (nonatomic, weak) id<DismissViewControllerDelegate> delegate;
- (IBAction)dismiss:(id)sender;

@end
