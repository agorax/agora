//
//  SellStepOneViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/18/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@protocol resetImages <NSObject>

- (void)reset;

@end

@interface SellStepTwoViewController : UIViewController

@property (nonatomic, weak) id<resetImages> delegate;

@property (strong, nonatomic) IBOutlet UITextField *itemNameField;
@property (strong, nonatomic) IBOutlet UITextField *itemPriceField;
@property (strong, nonatomic) IBOutlet UITextView *itemDescriptionTextView;
@property (strong, nonatomic) NSArray <UIImage *> *pictureArray;
@property (strong, nonatomic) IBOutlet UIButton *listItemButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *charactersLeftLabel;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end
