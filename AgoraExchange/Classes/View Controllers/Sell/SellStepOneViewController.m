//
//  SellStepOneViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/18/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "SellStepOneViewController.h"
#import "SellStepTwoViewController.h"
#import "OBDragDrop.h"
#import "camera.h"
#import "alertViewController.h"

@interface SellStepOneViewController () <resetImages>

@property int numberOfPictures;

@end

static int shimmerSpeed = 50;

@implementation SellStepOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    [self setupDragDrop];
    self.numberOfPictures = 0;
    [self setupShimmer];
    [self updateHighlightedView];
    [self setupButtons];
    // Do any additional setup after loading the view.
}

- (void)setupDragDrop
{
    //if we have time, we should implement drag drop
}

- (void)setupShimmer
{
    self.firstView.layer.borderWidth = 3.0;
    self.firstView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.secondView.layer.borderWidth = 3.0;
    self.secondView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.thirdView.layer.borderWidth = 3.0;
    self.thirdView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.fourthView.layer.borderWidth = 3.0;
    self.fourthView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.fifthView.layer.borderWidth = 3.0;
    self.fifthView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.firstView.shimmeringSpeed = shimmerSpeed;
    self.firstView.shimmeringDirection = FBShimmerDirectionDown;
    self.secondView.shimmeringSpeed = shimmerSpeed;
    self.thirdView.shimmeringSpeed = shimmerSpeed;
    self.fourthView.shimmeringSpeed = shimmerSpeed;
    self.fifthView.shimmeringSpeed = shimmerSpeed;
    
    self.firstView.contentView = self.firstImageView;
    self.secondView.contentView = self.secondImageView;
    self.thirdView.contentView = self.thirdImageView;
    self.fourthView.contentView = self.fourthImageView;
    self.fifthView.contentView = self.fifthImageView;
    
    [self.firstView addSubview:self.firstDeleteButton];
    [self.secondView addSubview:self.secondDeleteButton];
    [self.thirdView addSubview:self.thirdDeleteButton];
    [self.fourthView addSubview:self.fourthDeleteButton];
    [self.fifthView addSubview:self.fifthDeleteButton];

}

- (void)setupButtons
{
    self.takePhotoButton.layer.borderWidth = 1;
    self.takePhotoButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.chooseExistingButton.layer.borderWidth = 1;
    self.chooseExistingButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.clearImagesButton.layer.borderWidth = 1;
    self.clearImagesButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.addDetailsButton.layer.borderWidth = 1;
    self.addDetailsButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [self setUserInteraction];
}

#pragma mark - camera stuff

- (IBAction)chooseExisting:(id)sender
{
    if (self.numberOfPictures >= 5)
    {
        UIAlertController *alert = [alertViewController tooManyPictures];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    [self presentViewController:[camera chooseExisting:self] animated:YES completion:NULL];
}

- (IBAction)takePhoto:(id)sender
{
    if (self.numberOfPictures >= 5)
    {
        UIAlertController *alert = [alertViewController tooManyPictures];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    [self presentViewController:[camera takePhoto:self] animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:info[UIImagePickerControllerOriginalImage]];
    controller.delegate = self;
    controller.blurredBackground = YES;
    controller.hidesBottomBarWhenPushed = YES;
    [[self navigationController] pushViewController:controller animated:YES];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)ImageCropViewControllerSuccess:(UIViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    [self addImage:croppedImage];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)ImageCropViewControllerDidCancel:(UIViewController *)controller {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - highlighting images/ adding images etc

- (void) updateHighlightedView
{
    self.firstView.shimmering = NO;
    self.secondView.shimmering = NO;
    self.thirdView.shimmering = NO;
    self.fourthView.shimmering = NO;
    self.fifthView.shimmering = NO;
    if (self.numberOfPictures == 0)
    {
        self.firstView.shimmering = YES;
    }
    else if (self.numberOfPictures == 1)
    {
        self.secondView.shimmering = YES;
    }
    else if (self.numberOfPictures == 2)
    {
        self.thirdView.shimmering = YES;
    }
    else if (self.numberOfPictures == 3)
    {
        self.fourthView.shimmering = YES;
    }
    else if (self.numberOfPictures == 4)
    {
        self.fifthView.shimmering = YES;
    }
}

- (void) addImage: (UIImage *) image
{
    if (self.numberOfPictures == 0)
    {
        self.firstImageView.image = image;
    }
    else if (self.numberOfPictures == 1)
    {
        self.secondImageView.image = image;
    }
    else if (self.numberOfPictures == 2)
    {
        self.thirdImageView.image = image;
    }
    else if (self.numberOfPictures == 3)
    {
        self.fourthImageView.image = image;
    }
    else if (self.numberOfPictures == 4)
    {
        self.fifthImageView.image = image;
    }
    self.numberOfPictures++;
    [self updateHighlightedView];
    [self setUserInteraction];
}

#pragma mark - deleting images

- (IBAction)deleteFirstImage:(id)sender
{
    if (self.firstImageView.image == NULL)
    {
        return;
    }
    self.firstImageView.image = NULL;
    [self shiftImagesUpStartingAtIndex:1];
    self.numberOfPictures--;
    [self updateHighlightedView];
    [self setUserInteraction];
}

- (IBAction)deleteSecondImage:(id)sender
{
    if (self.secondImageView.image == NULL)
    {
        return;
    }
    self.secondImageView.image = NULL;
    [self shiftImagesUpStartingAtIndex:2];
    self.numberOfPictures--;
    [self updateHighlightedView];
}

- (IBAction)deleteThirdImage:(id)sender
{
    if (self.thirdImageView.image == NULL)
    {
        return;
    }
    self.thirdImageView.image = NULL;
    [self shiftImagesUpStartingAtIndex:3];
    self.numberOfPictures--;
    [self updateHighlightedView];
}

- (IBAction)deleteFourthImage:(id)sender
{
    if (self.fourthImageView.image == NULL)
    {
        return;
    }
    self.fourthImageView.image = NULL;
    [self shiftImagesUpStartingAtIndex:4];
    self.numberOfPictures--;
    [self updateHighlightedView];
}

- (IBAction)deleteFifthImage:(id)sender
{
    if (self.fifthImageView.image == NULL)
    {
        return;
    }
    self.fifthImageView.image = NULL;
    self.numberOfPictures--;
    [self updateHighlightedView];
}

- (void)shiftImagesUpStartingAtIndex: (int) index
{
    if (index == 1)
    {
        self.firstImageView.image = self.secondImageView.image;
        self.secondImageView.image = self.thirdImageView.image;
        self.thirdImageView.image = self.fourthImageView.image;
        self.fourthImageView.image = self.fifthImageView.image;
        self.fifthImageView.image = NULL;
    }
    if (index == 2)
    {
        self.secondImageView.image = self.thirdImageView.image;
        self.thirdImageView.image = self.fourthImageView.image;
        self.fourthImageView.image = self.fifthImageView.image;
        self.fifthImageView.image = NULL;
    }
    if (index == 3)
    {
        self.thirdImageView.image = self.fourthImageView.image;
        self.fourthImageView.image = self.fifthImageView.image;
        self.fifthImageView.image = NULL;
    }
    if (index == 4)
    {
        self.fourthImageView.image = self.fifthImageView.image;
        self.fifthImageView.image = NULL;
    }
}
- (IBAction)resetImages:(id)sender
{
    UIAlertController *alert = [alertViewController verifyPictureReset:self];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)reset
{
    self.firstImageView.image = nil;
    self.secondImageView.image = nil;
    self.thirdImageView.image = nil;
    self.fourthImageView.image = nil;
    self.fifthImageView.image = nil;
    self.numberOfPictures = 0;
    [self updateHighlightedView];
    [self setUserInteraction];
}

- (void)populateImageArray:(SellStepTwoViewController *) vc
{
    NSMutableArray<UIImage *> *imageArray = [[NSMutableArray alloc] initWithCapacity:5];
    if (self.firstImageView.image)
    {
        [imageArray addObject:self.firstImageView.image];
    }
    if (self.secondImageView.image)
    {
        [imageArray addObject:self.secondImageView.image];
    }
    if (self.thirdImageView.image)
    {
        [imageArray addObject:self.thirdImageView.image];
    }
    if (self.fourthImageView.image)
    {
        [imageArray addObject:self.fourthImageView.image];
    }
    if (self.fifthImageView.image)
    {
        [imageArray addObject:self.fifthImageView.image];
    }
    vc.pictureArray = imageArray;
}

#pragma mark - user interaction control on buttons

- (void)setUserInteraction
{
    if (self.numberOfPictures == 0)
    {
        self.clearImagesButton.alpha = .5;
        [self.clearImagesButton setUserInteractionEnabled:NO];
        self.addDetailsButton.alpha = .5;
        [self.addDetailsButton setUserInteractionEnabled:NO];
        return;
    }
    self.clearImagesButton.alpha = 1;
    [self.clearImagesButton setUserInteractionEnabled:YES];
    self.addDetailsButton.alpha = 1;
    [self.addDetailsButton setUserInteractionEnabled:YES];
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Sell Step Two"])
    {
        if (self.numberOfPictures == 0)
        {
            UIAlertController *alert = [alertViewController mustAddItemPicture];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Sell Step Two"])
    {
        SellStepTwoViewController *nextVC = segue.destinationViewController;
        nextVC.delegate = self;
        [self populateImageArray:nextVC];
    }
}

@end
