//
//  SellStepOneViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/18/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "SellStepOneViewController.h"
#import "SellStepTwoViewController.h"
#import "pickerData.h"
#import "ListingSuccessfulViewController.h"
#import "alertViewController.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "SVProgressHUD.h"
#import "AgoraColors.h"
#import "AgoraImageManager.h"
#import "Item+CoreDataClass.h"
#import "SessionManager.h"
#import "SellPresenter.h"

@interface SellStepTwoViewController () <UITextViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, DismissViewControllerDelegate>

@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *lastNumber;
@property (strong, nonatomic) SellPresenter *sellPresenter;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *textbookLabels;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *textbookContentViews;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *namePriceVerticalConstraint;

@property (weak, nonatomic) IBOutlet UITextField *isbnTextField;
@property (weak, nonatomic) IBOutlet UITextField *editionTextField;
@property (weak, nonatomic) IBOutlet UITextField *authorTextField;

@end

NSArray * categoryViewArray;
NSString * category;
BOOL textbookFieldsHidden = YES;

@implementation SellStepTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sellPresenter = [[SellPresenter alloc] init];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    categoryViewArray = [pickerData getPickerElementsForScreen:@"Sell Item"];
    category = [categoryViewArray firstObject];
    [self setupTextFields];
    [self setupButtons];
    [self addGestureRecognizer];
    // Do any additional setup after loading the view.
}

#pragma mark - view stuff

- (void)setupTextFields
{
    self.itemPriceField.keyboardType = UIKeyboardTypeNumberPad;
    [self.itemPriceField addTarget:self
                            action:@selector(priceFieldDidChange:)
                  forControlEvents:UIControlEventEditingChanged];
    [self.itemNameField addTarget:self
                           action:@selector(nameFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    _lastNumber = @"$0";
    //change this back to empty after testing is done
    _lastName = @"";
    self.itemPriceField.text = _lastNumber;
    self.itemNameField.text = _lastName;
    
    self.itemNameField.delegate = self;
    self.itemPriceField.delegate = self;
    self.itemDescriptionTextView.delegate = self;
    [self registerForKeyboardNotifications];
}

- (void)setupButtons
{
    self.listItemButton.layer.borderWidth = 1;
    self.listItemButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark - gesture stuff

- (void)addGestureRecognizer
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.contentView addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.itemNameField resignFirstResponder];
    [self.itemPriceField resignFirstResponder];
    [self.itemDescriptionTextView resignFirstResponder];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (self.itemDescriptionTextView.isFirstResponder || self.itemPriceField.isFirstResponder || self.itemNameField.isFirstResponder)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark - keyboard stuff

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height, 0, self.tabBarController.tabBar.frame.size.height, 0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.itemNameField.isFirstResponder)
    {
        [self.itemNameField resignFirstResponder];
        [self.itemPriceField becomeFirstResponder];
    }
    else if (self.itemPriceField.isFirstResponder)
    {
        [self.itemPriceField resignFirstResponder];
        [self.itemDescriptionTextView becomeFirstResponder];
    }
    return NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - text manipulation methods

-(void) nameFieldDidChange: (UITextField *) theTextField
{
    if([theTextField.text length] > 20)
    {
        theTextField.text = _lastName;
    }
    else
    {
        _lastName = theTextField.text;
    }
}

-(void)priceFieldDidChange:(UITextField *)theTextField
{
    NSLog(@"text changed: %@", theTextField.text);
    
    NSString *textFieldText = theTextField.text;
    
    if([textFieldText length] == 0)
    {
        textFieldText = [@"$" stringByAppendingString: textFieldText];
    }
    
    if([textFieldText length] >= 8)
    {
        textFieldText = _lastNumber;
    }
    
    if('$' != [textFieldText characterAtIndex:0])
    {
        textFieldText = [@"$" stringByAppendingString: textFieldText];
        if([textFieldText length] > 2 && [textFieldText characterAtIndex:2] == '$')
        {
            if ([textFieldText length] == 3)
            {
                textFieldText = [textFieldText substringToIndex:2];
            }
            else
            {
                textFieldText = [[textFieldText substringToIndex:1] stringByAppendingString:[textFieldText  substringFromIndex:3]];
            }
        }
    }
    
    if([textFieldText length] >1)
    {
        if('0' == [textFieldText characterAtIndex:1] && [textFieldText length] > 2)
        {
            textFieldText = [@"$" stringByAppendingString: [textFieldText substringFromIndex:2]];
        }
    }
    _lastNumber = textFieldText;
    self.itemPriceField.text = textFieldText;
}

-(void)textViewDidChange:(UITextView *)textView
{
    int charsLeft = 150 - (int)[self.itemDescriptionTextView.text length];
    if (charsLeft < 0)
    {
        self.charactersLeftLabel.textColor = [UIColor blackColor];
        self.charactersLeftLabel.text = @"Over character limit";
    }
    else
    {
        self.charactersLeftLabel.textColor = [AgoraColors agoraGreenColor];
        self.charactersLeftLabel.text = [NSString stringWithFormat:@"%d characters remaining", charsLeft];
    }
}

#pragma mark - picker delegate and data source

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    category = [categoryViewArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    if ([category isEqualToString:@"Textbooks"]) {
        [self showTextbookFields];
    } else {
        [self hideTextbookFields];
    }
}

- (void)showTextbookFields {
    for (UILabel *label in self.textbookLabels) {
        [label setHidden:NO];
    }
    for (UIView *view in self.textbookContentViews) {
        [view setHidden:NO];
    }
    if (textbookFieldsHidden) {
        [self.namePriceVerticalConstraint setPriority:997];
        textbookFieldsHidden = NO;
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }
}

- (void)hideTextbookFields {
    for (UILabel *label in self.textbookLabels) {
        [label setHidden:YES];
    }
    for (UIView *view in self.textbookContentViews) {
        [view setHidden:YES];
    }
    if (!textbookFieldsHidden) {
        [self.namePriceVerticalConstraint setPriority:999];
        textbookFieldsHidden = YES;
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return categoryViewArray.count;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSAttributedString *) pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = categoryViewArray[row];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
}

#pragma mark - checks before listing

//returns null if price is not valid
- (NSString *) checkPrice: (NSString *) price
{
    if([price length] <= 1)
    {
        return NULL;
    }
    
    int nonZeroNumbers = 0;
    
    for (int k = 1; k < [price length]; k++)
    {
        if ([price characterAtIndex:k] != '0' && [price characterAtIndex:k] != '.')
        {
            nonZeroNumbers++;
        }
        if ([price characterAtIndex:k] != '.' && [price characterAtIndex:k] != '0' && [price characterAtIndex:k] != '1' && [price characterAtIndex:k] != '2' && [price characterAtIndex:k] != '3' && [price characterAtIndex:k] != '4' && [price characterAtIndex:k] != '5' && [price characterAtIndex:k] != '6' && [price characterAtIndex:k] != '7' && [price characterAtIndex:k] != '8' && [price characterAtIndex:k] != '9')
        {
            return NULL;
        }
    }
    
    if(nonZeroNumbers == 0)
    {
        return @"$0";
    }
    
    return price;
}

#pragma mark - Navigation

- (BOOL)validateSellable
{
    if ([self.itemNameField.text length] == 0)
    {
        UIAlertController *alert = [alertViewController mustEnterName];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    NSString *itemPrice = [self checkPrice:self.itemPriceField.text];
    if (itemPrice == NULL)
    {
        UIAlertController *alert = [alertViewController enterValidPrice];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    self.itemPriceField.text = itemPrice;
    
    if([self.itemDescriptionTextView.text length] > 150)
    {
        UIAlertController *alert = [alertViewController itemDescriptionTooLong];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    if ([category isEqualToString:@"Free"])
    {
        if (![self.itemPriceField.text isEqualToString:@"$0"])
        {
            UIAlertController *alert = [alertViewController freeItem];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
    }
    return [self validateTextbook];
}

- (BOOL)validateTextbook {
    if ([category isEqualToString:@"Textbooks"]) {
        if ([self.isbnTextField.text length] != 13) {
            UIAlertController *alert = [alertViewController isbnLength];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
        if ([self.editionTextField.text length] > 2) {
            UIAlertController *alert = [alertViewController editionLength];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
        if ([self.authorTextField.text length] > 45) {
            UIAlertController *alert = [alertViewController authorLength];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
    }
    return YES;
}

#pragma mark - put item
- (IBAction)sell:(id)sender {
    if ([self validateSellable] == NO) {
        return;
    }
    [self sellItem:[self constructItem]];
}

- (Item *)constructItem {
    Item *item = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:[SellStepTwoViewController managedObjectContext]];
    item.name = self.itemNameField.text;
    item.category = category;
    item.itemDescription = self.itemDescriptionTextView.text;
    item.price = [[NSNumber numberWithInteger:[[self.itemPriceField.text substringFromIndex:1] intValue]] intValue];
    item.school = [SessionManager getCurrentUser].school;
    item.userId = [SessionManager getCurrentUser].userId;
    item.author = self.authorTextField.text;
    item.isbn = [self.isbnTextField.text integerValue];
    item.edition = [self.editionTextField.text integerValue];
    item.numberOfPictures = [self.pictureArray count];
    item.createdAt = [NSDate date];
    return item;
}

- (void)sellItem:(Item *)item {
    [self.sellPresenter sellItem:item images:self.pictureArray withCompletionBlock:^(AgoraError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                [self displayAlert:alert];
            } else {
                [[SellStepTwoViewController managedObjectContext] save:nil];
                [self performSegueWithIdentifier:@"Listing Successful" sender:self];
            }
        });
    }];
}

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[SellStepTwoViewController managedObjectContext]];
    return concurrentContext;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - delegate methods
- (void)dismissVC
{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [self.delegate reset];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Listing Successful"])
    {
        ListingSuccessfulViewController *nextVC = segue.destinationViewController;
        nextVC.delegate = self;
    }
}

-(void)displayAlert:(UIAlertController *)alert {
    [self presentViewController:alert animated:YES completion:nil];
}

@end
