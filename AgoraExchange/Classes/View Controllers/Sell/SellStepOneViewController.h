//
//  SellStepOneViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/18/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shimmer/FBShimmeringView.h"
#import "ImageCropView.h"

@interface SellStepOneViewController : UIViewController <ImageCropViewControllerDelegate>
@property (strong, nonatomic) IBOutlet FBShimmeringView *firstView;
@property (strong, nonatomic) IBOutlet FBShimmeringView *secondView;
@property (strong, nonatomic) IBOutlet FBShimmeringView *thirdView;
@property (strong, nonatomic) IBOutlet FBShimmeringView *fourthView;
@property (strong, nonatomic) IBOutlet FBShimmeringView *fifthView;

- (IBAction)takePhoto:(id)sender;
- (IBAction)chooseExisting:(id)sender;
- (IBAction)resetImages:(id)sender;
- (void)reset;

@property (strong, nonatomic) IBOutlet UIImageView *firstImageView;
@property (strong, nonatomic) IBOutlet UIImageView *secondImageView;
@property (strong, nonatomic) IBOutlet UIImageView *thirdImageView;
@property (strong, nonatomic) IBOutlet UIImageView *fourthImageView;
@property (strong, nonatomic) IBOutlet UIImageView *fifthImageView;

- (IBAction)deleteFirstImage:(id)sender;
- (IBAction)deleteSecondImage:(id)sender;
- (IBAction)deleteThirdImage:(id)sender;
- (IBAction)deleteFourthImage:(id)sender;
- (IBAction)deleteFifthImage:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (strong, nonatomic) IBOutlet UIButton *chooseExistingButton;
@property (strong, nonatomic) IBOutlet UIButton *clearImagesButton;
@property (strong, nonatomic) IBOutlet UIButton *addDetailsButton;

@property (strong, nonatomic) IBOutlet UIButton *firstDeleteButton;
@property (strong, nonatomic) IBOutlet UIButton *secondDeleteButton;
@property (strong, nonatomic) IBOutlet UIButton *thirdDeleteButton;
@property (strong, nonatomic) IBOutlet UIButton *fourthDeleteButton;
@property (strong, nonatomic) IBOutlet UIButton *fifthDeleteButton;


@end
