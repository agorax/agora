//
//  AgoraTabBarController.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/11/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "AgoraTabBarController.h"
#import "ProfileViewController.h"
#import "SessionManager.h"

@interface AgoraTabBarController ()

@end

@implementation AgoraTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIViewController *buy = [UIStoryboard storyboardWithName:@"Buy" bundle:nil].instantiateInitialViewController;
    UIViewController *sell = [UIStoryboard storyboardWithName:@"Sell" bundle:nil].instantiateInitialViewController;
    UIViewController *chat = [UIStoryboard storyboardWithName:@"Talk" bundle:nil].instantiateInitialViewController;
    UIViewController *me = [UIStoryboard storyboardWithName:@"ViewProfile" bundle:nil].instantiateInitialViewController;
    NSArray <UIViewController *> *vcArray = [[NSArray alloc] initWithObjects:buy, sell, chat, me, nil];
    self.viewControllers = vcArray;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatNotificationReceived) name:@"chatNotification" object:nil];
    if ([[SessionManager totalUnreadCount] intValue] > 0) {
        [[self.tabBar.items objectAtIndex:2] setBadgeValue:[NSString stringWithFormat:@"%@",[SessionManager totalUnreadCount]]];
    } else {
        [[self.tabBar.items objectAtIndex:2] setBadgeValue:nil];
    }
    
    // Do any additional setup after loading the view.
}

- (void)chatNotificationReceived {
    if ([[SessionManager totalUnreadCount] intValue] > 0) {
        [[self.tabBar.items objectAtIndex:2] setBadgeValue:[NSString stringWithFormat:@"%@",[SessionManager totalUnreadCount]]];
    } else {
        [[self.tabBar.items objectAtIndex:2] setBadgeValue:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
