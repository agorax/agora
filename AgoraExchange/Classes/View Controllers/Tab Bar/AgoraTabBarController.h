//
//  AgoraTabBarController.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/11/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgoraTabBarController : UITabBarController

@end
