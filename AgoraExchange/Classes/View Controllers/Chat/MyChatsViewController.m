//
//  MyChatsViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/10/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MyChatsViewController.h"
#import "ChatTableViewCell.h"
#import <Quickblox/Quickblox.h>
#import "SVProgressHUD.h"
#import "ChatViewController.h"
#import "dateConverter.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "alertViewController.h"
#import <TSMessages/TSMessageView.h>
#import "NetworkManager.h"
#import "User+CoreDataClass.h"
#import "SessionManager.h"
#import "NetworkManager.h"
#import "Conversation+CoreDataClass.h"
#import "ChatTableCellViewModel.h"

@interface MyChatsViewController () <
UITableViewDelegate,
UITableViewDataSource,
UISearchResultsUpdating,
UISearchBarDelegate
>

@property (strong, nonatomic) UISearchController *searchController;
//one array of conversations, and one filtered array of conversations too
@property (strong, nonatomic) NSMutableArray <Conversation *> *dialogs;
@property (strong, nonatomic) NSMutableArray <ChatTableCellViewModel *> *filteredViewModels;
@property (strong, nonatomic) NSMutableArray <ChatTableCellViewModel *> *viewModels;
@property (strong, nonatomic) Conversation *dialogSelected;
@end

static NSString *cellID = @"cellID";
static int cellHeight = 85;
int resendCount = 0;

@implementation MyChatsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self setupSearch];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Chats Need Refresh" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refresh)
                                                 name:@"Chats Need Refresh"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Chats Need Refresh Initial" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(initialMessageRefresh:)
                                                 name:@"Chats Need Refresh Initial"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDataModelChange:)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:nil];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [self refresh];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Chats Need Refresh" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Chats Need Refresh Initial" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - setup views

- (void)setupSearch
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    //self.searchController.searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)setupTableView
{
    self.viewModels = [[NSMutableArray alloc] init];
    self.filteredViewModels = [[NSMutableArray alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"ChatTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
    self.tableView.backgroundColor = [UIColor clearColor];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshListings:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    [self refresh];
}

#pragma mark - refresh control

- (void)refreshListings:(UIRefreshControl *)refreshControl {
    [self refresh];
    [refreshControl endRefreshing];
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


- (void) initialMessageRefresh: (NSNotification *) notification {
    NSLog(@"%@", [SessionManager getCurrentUser]);
    [NetworkManager getConversationWithUserId:[SessionManager getCurrentUser].userId andCompletionBlock:^(AgoraError *getError){
        if(getError){}else{
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
            NSSortDescriptor *conversationSort = [[NSSortDescriptor alloc] initWithKey:@"updatedAt" ascending:NO];
            [request setSortDescriptors:@[conversationSort]];
            NSError *error = nil;
            NSArray *results = [[MyChatsViewController managedObjectContext] executeFetchRequest:request error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
            [self populateViewModelsWithConverations:results];
            });
        }

    }];
}

- (void) populateViewModelsWithConverations: (NSArray *)conversations {
    [self.viewModels removeAllObjects];
    for (Conversation *conversation in conversations) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
        Item *currentItem = [Item getItemWithId:conversation.itemId inManagedObjectContext: [MyChatsViewController managedObjectContext] andFetchRequest:request];
        ChatTableCellViewModel *viewModel = [[ChatTableCellViewModel alloc] initWithConversation:conversation item:currentItem];
        [self.viewModels addObject:viewModel];
    }
    [self.tableView reloadData];
    [SVProgressHUD dismiss];

}

- (void) sendLocalMessageFailedNotification: (QBChatDialog *) dialog{
    NSString *message = [NSString stringWithFormat:@"Your initial message about %@ failed to send. Select %@ conversation to re-send.",dialog.data[@"itemName"], dialog.data[@"itemName"]];
    [TSMessage showNotificationInViewController:[self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController] title:@"Message Failed!" subtitle:message type:TSMessageNotificationTypeError];
    resendCount = 0;
}

- (void) resendMessageWithDialog:(QBChatDialog *) dialog andMessage: (QBChatMessage *) message {
    NSLog(@"This thing failed -==-=-=-=-==-=-==-=-=-");
    resendCount++;
    [dialog sendMessage:message completionBlock:^(NSError *error) {
        if (error) {
            [self sendLocalMessageFailedNotification:dialog];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Chats Need Refresh Initial" object:message];
        }
    }];

}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

- (void)refresh
{
    if (!self.dialogs || [self.dialogs count] == 0)
    {
        [SVProgressHUD show];
    }
    NSLog(@"%hd", [SessionManager getCurrentUser].userId);
    [NetworkManager getConversationWithUserId:[SessionManager getCurrentUser].userId andCompletionBlock:^(AgoraError *getError){
        if(getError){}else{
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
            NSSortDescriptor *conversationSort = [[NSSortDescriptor alloc] initWithKey:@"updatedAt" ascending:NO];
            [request setSortDescriptors:@[conversationSort]];
            self.dialogs = [[Conversation getConversationsWithUserId:[SessionManager getCurrentUser].userId inManagedObjectContext:[MyChatsViewController managedObjectContext] andFetchRequest:request] copy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self populateViewModelsWithConverations:self.dialogs];
                NSDictionary* userInfo = @{@"total": [SessionManager totalUnreadCount]};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"chatNotification" object:nil userInfo:userInfo];
            });
        }
        
    }];
}

#pragma mark - table view datasource and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.searchController.active)
    {
        return [self.filteredViewModels count];
    }
    else
    {
        return self.viewModels.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatTableCellViewModel *viewModel = self.viewModels[indexPath.row];
    if (self.searchController.active)
    {
        viewModel = self.filteredViewModels[indexPath.row];
    }
    ChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    [cell setViewModel:viewModel];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.searchController.active)
    {
        self.dialogSelected = [self conversationFromViewModel:self.filteredViewModels[indexPath.row]];
    }
    else
    {
        self.dialogSelected = [self conversationFromViewModel:self.viewModels[indexPath.row]];
    }
    ([self.dialogSelected respondsToSelector:NSSelectorFromString(@"unReadMessages")]) ? self.dialogSelected.unReadMessages = 0 : NSLog(@"no unread");
    [NetworkManager zeroUnreadCountforConversation:self.dialogSelected];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"chatNotification" object:nil userInfo:nil];
    [tableView reloadData];
    [self performSegueWithIdentifier:@"Chat" sender:self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Conversation *dialogToDelete;
        if (self.searchController.active)
        {
            dialogToDelete = [self conversationFromViewModel:self.filteredViewModels[indexPath.row]];
        }
        else
        {
            dialogToDelete = [self conversationFromViewModel:self.viewModels[indexPath.row]];
        }
        NSNumber *recipientId = ([SessionManager getCurrentUser].userId == dialogToDelete.sellerId) ? [NSNumber numberWithInt:dialogToDelete.buyerId] : [NSNumber numberWithInt:dialogToDelete.sellerId];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
        Item *currentItem = [Item getItemWithId:dialogToDelete.itemId inManagedObjectContext: [MyChatsViewController managedObjectContext] andFetchRequest:request];
        [NetworkManager deleteConversationWithId:dialogToDelete.conversationId recipientId:recipientId senderDisplayName:[SessionManager getCurrentUser].name itemName:currentItem.name andCompletionBlock:^(AgoraError *error) {
            if (error) {
                UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                [self displayAlert:alert];
            } else {
                NSError * error = nil;
                NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
                [Message deleteMessagesForConversationId:dialogToDelete.conversationId inManagedObjectContext:[MyChatsViewController managedObjectContext] andFetchRequest:request];
                [[MyChatsViewController managedObjectContext] deleteObject:dialogToDelete];
                [[MyChatsViewController managedObjectContext] save:&error];
                [self refresh];
            }
        }];
    }
}

#pragma mark - search stuff

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self.filteredViewModels removeAllObjects];
    if ([searchController.searchBar.text length] == 0)
    {
        [self.filteredViewModels addObjectsFromArray:self.viewModels];
    }
    
    NSPredicate *itemNamePredicate = [[NSPredicate alloc] init];
    itemNamePredicate = [NSPredicate predicateWithFormat:@"SELF.itemName CONTAINS[c] %@", searchController.searchBar.text];
    
    NSPredicate *buyerNamePredicate = [[NSPredicate alloc] init];
    buyerNamePredicate = [NSPredicate predicateWithFormat:@"SELF.name CONTAINS[c] %@", searchController.searchBar.text];
    
    NSArray <NSPredicate *> *namePredicateArray = [NSArray arrayWithObjects:buyerNamePredicate, itemNamePredicate, nil];
    
    NSCompoundPredicate *nameCompoundPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:namePredicateArray];
    
    [self.filteredViewModels addObjectsFromArray:[self.viewModels filteredArrayUsingPredicate:nameCompoundPredicate]];
    [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

- (Conversation *)conversationFromViewModel: (ChatTableCellViewModel *)viewModel {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
    return [Conversation getConversationWithId:viewModel.conversation.conversationId inManagedObjectContext:[MyChatsViewController managedObjectContext] andFetchRequest:request];
}

#pragma mark - navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ((ChatViewController *) [segue destinationViewController]).chatDialog = self.dialogSelected;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

-(void)displayAlert:(UIAlertController *)alert {
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)handleDataModelChange:(NSNotification *)note {
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    
    for (NSManagedObject *mob in insertedObjects) {
        if ([mob.entity.name isEqualToString:@"Conversation"]) {
            if (((Conversation *)mob).sellerId == [SessionManager getCurrentUser].userId || ((Conversation *)mob).buyerId == [SessionManager getCurrentUser].userId) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self refresh];
                });
                return;
            }
        }
    }
    
    for (NSManagedObject *mob in deletedObjects) {
        if ([mob.entity.name isEqualToString:@"Conversation"]) {
            if (((Conversation *)mob).sellerId == [SessionManager getCurrentUser].userId || ((Conversation *)mob).buyerId == [SessionManager getCurrentUser].userId) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self refresh];
                });
                return;
            }
        }
    }
    
    for (NSManagedObject *mob in updatedObjects) {
        if ([mob.entity.name isEqualToString:@"Conversation"]) {
            if (((Conversation *)mob).sellerId == [SessionManager getCurrentUser].userId || ((Conversation *)mob).buyerId == [SessionManager getCurrentUser].userId) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self refresh];
                });
                return;
            }
        }
    }
}

@end
