  //
//  ChatViewController.m
//
//
//  Created by Travis Chambers on 7/6/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//


#import "ChatViewController.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <SSKeychain/SSKeychain.h>
#import <JSQMessagesViewController/JSQMessagesAvatarImage.h>
#import <JSQMessagesViewController/JSQMessagesAvatarImageFactory.h>
#import "SVProgressHUD.h"
#import "Constants.h"
#import "ItemDetailsViewController.h"
#import "alertViewController.h"
#import "ProfileInfoViewController.h"
#import "camera.h"
#import <TSMessages/TSMessageView.h>
#import "SessionManager.h"
#import "NetworkManager.h"
#import "alertViewController.h"
#import <PINRemoteImage/PINRemoteImageCaching.h>
#import "AsyncDisplayKit/ASPINRemoteImageDownloader.h"

@interface ChatViewController ()
@property (weak, nonatomic) IBOutlet UINavigationItem *nameNavItem;
@property (strong, nonatomic) NSMutableDictionary *profPics;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) QBUUser *qbUser;
@property (strong, nonatomic) NSString *senderId;
@property (strong, nonatomic) NSString *senderDisplayName;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) UIImage *sentImage;
@end

int refreshCount;

@implementation ChatViewController

#pragma mark - view lifecycle

-(void) viewDidLoad {
    [super viewDidLoad];
    self.messages = [[NSMutableArray alloc] init];
    [SVProgressHUD show];
    [self setUserInformation];
    if(!self.didStartNewConversation) {
        [self setUpChat];
    } else {
        [SVProgressHUD dismiss];
    }
    self.automaticallyScrollsToMostRecentMessage = YES;
    refreshCount = 0;
    JSQMessagesBubbleImageFactory *bubble = [[JSQMessagesBubbleImageFactory alloc] init];
    self.incomingBubbleImageView = [bubble incomingMessagesBubbleImageWithColor:[UIColor lightGrayColor]];
    self.outgoingBubbleImageView = [bubble outgoingMessagesBubbleImageWithColor:[UIColor blueColor]];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshMessages:) forControlEvents:UIControlEventValueChanged];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userIsNotTyping) name:UIKeyboardDidHideNotification object:nil];
    [self getPictures];
    [self.collectionView layoutSubviews];
    [self.collectionView addSubview:self.refreshControl];
    [self addListeners];
}

- (void) viewWillAppear:(BOOL)animated {
    [self setUserInformation];
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [self setUserInformation];
    [super viewDidAppear:animated];
    [self scrollToBottomAnimated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self userIsNotTyping];
    [super viewWillDisappear:animated];
}

- (void)refreshMessages:(UIRefreshControl *)refreshControl {
    [self.refreshControl beginRefreshing];
    [self.collectionView setContentOffset:CGPointMake(0, self.collectionView.contentOffset.y-self.refreshControl.frame.size.height) animated:YES];
    [self getQBMessagesWithRefresh:YES];
    [refreshControl endRefreshing];
}

- (void) setUpChat {
    [[ChatManager sharedInstance] subscribeToRoom:self.chatDialog.room];
    [self getItem];
    [self getQBMessagesWithRefresh:NO];
}

- (void) addListeners {
    [[ChatManager sharedInstance] getMessages:^(Message *message) {
        [self receiveMessage:message];
    }];
    [[ChatManager sharedInstance] listenForTyping:^(bool isTyping) {
        if (isTyping) {
            self.showTypingIndicator = YES;
            [self scrollToBottomAnimated:YES];
        } else {
            self.showTypingIndicator = NO;
        }
    }];
    [[ChatManager sharedInstance] reconnectOnDisconnect:^(){
        [[ChatManager sharedInstance] subscribeToRoom:self.chatDialog.room];
    }];
    
}

- (void) userIsNotTyping {
    [[ChatManager sharedInstance] userStoppedTypingInRoom:self.chatDialog.room];
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if(!self.didStartNewConversation) {
        NSString *currentString = [textView.text substringWithRange:NSMakeRange(0,range.location)];
        currentString = [currentString stringByAppendingString:text];
        if(!currentString || [currentString isEqualToString:@""]) {
            [[ChatManager sharedInstance] userStoppedTypingInRoom:self.chatDialog.room];
        } else {
            [[ChatManager sharedInstance] userIsTypingInRoom:self.chatDialog.room];
        }
    }
    return YES;
}

- (void) sendLocalMessageFailedNotification {
    NSString *message = [NSString stringWithFormat:@"Your Message Failed to Send"];
    [TSMessage showNotificationInViewController:[self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController] title:@"Message Failed!" subtitle:message type:TSMessageNotificationTypeError];
}


- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}


#pragma mark - loading Information

- (NSString *)senderDisplayName
{
    return (!self.currentUser.name || [self.currentUser.name isEqualToString:@""]) ? self.currentUser.email : self.currentUser.name;
}

- (NSString *)senderId
{
    return [NSString stringWithFormat:@"%d", self.currentUser.userId];
}

- (void)jsq_updateCollectionViewInsets
{
    [self jsq_setCollectionViewInsetsTopValue:60
                                  bottomValue:CGRectGetHeight(self.collectionView.frame) - CGRectGetMinY(self.inputToolbar.frame)];
}

- (void)jsq_setCollectionViewInsetsTopValue:(CGFloat)top bottomValue:(CGFloat)bottom {
    UIEdgeInsets insets = UIEdgeInsetsMake(60, 0.0f, bottom, 0.0f);
    self.collectionView.contentInset = insets;
    self.collectionView.scrollIndicatorInsets = insets;
}

- (void) setUserInformation {
    self.profPics = [[NSMutableDictionary alloc] initWithCapacity:2];
    self.currentUser = [SessionManager getCurrentUser];
    self.senderId = [self senderId];
    
    self.senderDisplayName = [self senderDisplayName];
    NSString *idString;
    if(self.currentUser.userId == self.chatDialog.sellerId)
    {
        self.otherUser = (self.otherUser) ? self.otherUser : [User getUserWithId:self.chatDialog.buyerId inManagedObjectContext:[ChatViewController managedObjectContext]];
        self.nameNavItem.title = (self.otherUser.name && [self.otherUser.name length] > 0) ? self.otherUser.name : self.otherUser.email;
        idString = @"buyerId";
    }
    else {
        self.otherUser = (self.otherUser) ? self.otherUser : [User getUserWithId:self.chatDialog.sellerId inManagedObjectContext:[ChatViewController managedObjectContext]];
        self.nameNavItem.title = (self.otherUser.name && [self.otherUser.name length] > 0) ? self.otherUser.name : self.otherUser.email;
        idString = @"sellerId";
    }
}

- (void) getPictures
{
    UIImage *sqImg = [UIImage imageNamed:@"Profile Place Holder"];
    dispatch_async(dispatch_get_main_queue(), ^{
        JSQMessagesAvatarImageFactory *avatarFact = [[JSQMessagesAvatarImageFactory alloc] initWithDiameter:sqImg.size.height/2.0];
        [self imageForUser:self.currentUser andCompletionBlock:^(UIImage *image) {
            UIImage *profilePicture = [avatarFact circularAvatarImage:image];
            JSQMessagesAvatarImage *currentUserProfPic = [JSQMessagesAvatarImage avatarWithImage:profilePicture];
            [self.profPics setValue:currentUserProfPic forKey:[NSString stringWithFormat:@"%hd", self.currentUser.userId]];
            [self imageForUser:self.currentUser andCompletionBlock:^(UIImage *imageOther) {
                UIImage *profilePictureOther = [avatarFact circularAvatarImage:imageOther];
                JSQMessagesAvatarImage *userProfPic = [JSQMessagesAvatarImage avatarWithImage:profilePictureOther];
                [self.profPics setValue:userProfPic forKey:[NSString stringWithFormat:@"%hd", self.otherUser.userId]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionView reloadData];
                });
            }];
        }];
    });
}

- (void)imageForUser:(User *) user andCompletionBlock:(void(^)(UIImage *image))completionBlock {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[AgoraImageManager urlForUser:user]]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   if (image) {
                                       completionBlock(image);
                                   }
                               }
                           }];
}

- (void) receiveMessage:(Message *) message {
    [self scrollToBottomAnimated:YES];
    JSQMessage *jsqMessage = [self messageFromMessage:message];
    [self.messages addObject:jsqMessage];
    [self finishReceivingMessage];
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)getItem
{
    self.viewItemBarButton.enabled = NO;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    self.item = [Item getItemWithId:self.chatDialog.itemId inManagedObjectContext:[ChatViewController managedObjectContext] andFetchRequest:request];
    self.viewItemBarButton.enabled = YES;
}

-(void)getImageForMessage:(Message *)message completionBlock:(void(^)(UIImage *)) completionBlock {
    NSData *imData = [[[[ASPINRemoteImageDownloader sharedDownloader] sharedPINRemoteImageManager] defaultImageCache] objectFromDiskForKey:[AgoraImageManager urlForMessage:message]];
    if (imData) {
        completionBlock([UIImage imageWithData:imData]);
    }
    else
    {
        NSURL *url = [NSURL URLWithString:[AgoraImageManager urlForMessage:message]];
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *downloadedImage = [UIImage imageWithData:data];
                if (downloadedImage) {
                    completionBlock(downloadedImage);
                }
            }
        }];
        [task resume];
    }

}
-(JSQMessage *) messageFromMessage: (Message *) message {
    NSDateFormatter *gmtDateFormatter = [[NSDateFormatter alloc] init];
    gmtDateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    gmtDateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSDate *created_at = (message.createdAt) ? message.createdAt : [NSDate date];
    if(message.numberOfPictures > 0) {
        JSQPhotoMediaItem *attachmentImage = [[JSQPhotoMediaItem alloc] initWithImage:nil];
        attachmentImage.appliesMediaViewMaskAsOutgoing = (self.senderId == [NSString stringWithFormat:@"%lu", (unsigned long)message.senderId]) ? YES : NO;
        [self getImageForMessage:message completionBlock:^(UIImage* image ){
            dispatch_async(dispatch_get_main_queue(), ^{
                attachmentImage.image = image;
                [self.collectionView reloadData];
            });
        }];
        return [[JSQMessage alloc] initWithSenderId: [NSString stringWithFormat:@"%hd",message.senderId] senderDisplayName:[NSString stringWithFormat:@"%@",message.senderDisplayName] date:created_at media:attachmentImage];
    } else {
        return [[JSQMessage alloc] initWithSenderId:[NSString stringWithFormat:@"%hd",message.senderId] senderDisplayName:[NSString stringWithFormat:@"%@",message.senderDisplayName] date:created_at text:message.message];
    }
}

-(void) getQBMessagesWithRefresh:(BOOL) refresh {
    //get the most recent 20 messages
    
    [NetworkManager getMessagesWithConversationId:self.chatDialog.conversationId andCompletionBlock:^(AgoraError *getError){
        if(getError){
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
            Conversation *dialogToDelete = [Conversation getConversationWithId:self.chatDialog.conversationId inManagedObjectContext:[ChatViewController managedObjectContext] andFetchRequest:request];
            [Conversation deleteConversation:dialogToDelete inManagedObjectContext:[ChatViewController managedObjectContext]];
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alert = [alertViewController conversationDeletedAndExit:self.navigationController];
                [self presentViewController:alert animated:YES completion:nil];
            });
        } else {
            if(refresh) {
                refreshCount++;
            }
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
            NSArray *results = [Message getMessagesWithConversationId:self.chatDialog.conversationId andRefreshCount:refreshCount inManagedObjectContext:[ChatViewController managedObjectContext] andFetchRequest:request];
            Message *previousMessage;
            for (Message *message in results) {;
                // QUICK-FIX: Better solution needed
                if(message.text && (!previousMessage || previousMessage.messageId != message.messageId)) {
                    JSQMessage *jsqMessage = [self messageFromMessage:message];
                    [self.messages insertObject:jsqMessage atIndex:0];
                    previousMessage = message;
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
            });
            [SVProgressHUD dismiss];
            if(!refresh) {
                [self scrollToBottomAnimated:YES];
            }
        }
        
    }];

}

#pragma mark - send message

-(void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date
{
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId senderDisplayName:self.senderDisplayName date:date text:text];
    if(self.didStartNewConversation) {
        [[ChatManager sharedInstance] createConversation:self.otherUser.userId buyer:[SessionManager getCurrentUser].userId item:self.item.itemId message:text senderDisplayName:self.senderDisplayName itemName:self.item.name completionBlock:^(NSNumber *conversationId){
            [self createConversationWithMessage: message.text andConversationId:[conversationId intValue]];
            [self.messages addObject:message];
            self.didStartNewConversation = NO;
            self.navigationItem.hidesBackButton = NO;
            [self finishSendingMessage];
        }];
    } else {
        [self.messages addObject:message];
        [[ChatManager sharedInstance] sendMessageToRoom:self.chatDialog.room event:@"message" message:text from:[SessionManager getCurrentUser].userId convo:self.chatDialog.conversationId senderDisplayName:self.senderDisplayName recipientId: self.otherUser.userId numberOfPictures:0 completionBlock:^(NSNumber *messageId){
            [self createMessageWithMessage:text andMessageId:[messageId intValue] andJSQMessage: message];
            self.chatDialog.lastMessage = text;
            [self finishSendingMessage];
        }];

    }
}

- (void) createMessageWithMessage: (NSString *)text andMessageId: (int) messageId andJSQMessage: (JSQMessage *)jsqMessage {
    [self createMessageWithMessage:text andMessageId:messageId andJSQMessage:jsqMessage withMedia:0];
    [[ChatViewController managedObjectContext] save:nil];
}

- (Message *) createMessageWithMessage: (NSString *)text andMessageId: (int) messageId andJSQMessage:(JSQMessage *)jsqMessage withMedia: (int) numberOfPictures {
    Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:[ChatViewController managedObjectContext]];
    message.message = text;
    message.senderId = [jsqMessage.senderId intValue];
    message.conversationId = self.chatDialog.conversationId;
    message.numberOfPictures = numberOfPictures;
    message.messageId = messageId;
    return message;
}

- (void) createConversationWithMessage: (NSString *)message andConversationId:(int) conversationId {
    Conversation *dialog = [NSEntityDescription insertNewObjectForEntityForName:@"Conversation" inManagedObjectContext:[ChatViewController managedObjectContext]];
    dialog.buyerId = [SessionManager getCurrentUser].userId;
    dialog.sellerId = self.otherUser.userId;
    dialog.itemId = self.item.itemId;
    dialog.conversationId = conversationId;
    dialog.room = [NSString stringWithFormat:@"%hd%hd%hd",dialog.itemId, dialog.sellerId, dialog.buyerId];
    dialog.lastMessage = message;
    self.chatDialog = dialog;
    [[ChatViewController managedObjectContext] save:nil];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    if(self.didStartNewConversation) {
        UIAlertController *alert = [alertViewController startWithAMessage];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    [self presentViewController:[camera chooseExisting:self] animated:YES completion:NULL];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:info[UIImagePickerControllerOriginalImage]];
    controller.delegate = self;
    controller.blurredBackground = NO;
    self.imageURL = info[UIImagePickerControllerReferenceURL];
    [[self navigationController] pushViewController:controller animated:YES];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)ImageCropViewControllerSuccess:(UIViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    JSQPhotoMediaItem *attachmentImage = [[JSQPhotoMediaItem alloc] initWithImage:croppedImage];
    attachmentImage.appliesMediaViewMaskAsOutgoing = YES;
    
    JSQMessage *jsqMessage = [[JSQMessage alloc] initWithSenderId:self.senderId senderDisplayName:self.senderDisplayName date:[NSDate new] media:attachmentImage];
    [self.messages addObject:jsqMessage];
    [self.collectionView reloadData];
    self.chatDialog.lastMessage = @"An Image Was Sent";
    [self finishSendingMessage];
    [[ChatManager sharedInstance] sendMessageToRoom:self.chatDialog.room event:@"media" message:@"" from:[SessionManager getCurrentUser].userId convo:self.chatDialog.conversationId senderDisplayName:self.senderDisplayName recipientId: self.otherUser.userId numberOfPictures:1 completionBlock:^(NSNumber *messageId){
        Message *message = [self createMessageWithMessage:@"" andMessageId:[messageId intValue] andJSQMessage: jsqMessage withMedia:1];
        [AgoraImageManager uploadToS3:croppedImage onMessage:message andCompletionBlock:^(){
            [[ChatManager sharedInstance] sendMediaMessageToRoom:self.chatDialog.room event:@"sendMediaMessage" messageId:messageId message:@"" from:[SessionManager getCurrentUser].userId convo:self.chatDialog.conversationId senderDisplayName:self.senderDisplayName recipientId: self.otherUser.userId numberOfPictures:1 completionBlock:^(NSNumber *mediaMessageId) {
                NSLog(@"Message sent: %@",mediaMessageId);
            }];
        }];
    }];

    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)ImageCropViewControllerDidCancel:(UIViewController *)controller {
    [[self navigationController] popViewControllerAnimated:YES];
}




#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messages objectAtIndex:indexPath.item];
}

- (BOOL)isOutgoingMessage:(id<JSQMessageData>)messageItem
{
    return [messageItem.senderId isEqualToString:[self senderId]];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *mess = [self.messages objectAtIndex:indexPath.item];
    if ([mess.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubbleImageView;
    }
    else {
        return self.incomingBubbleImageView;
    }
}

-(id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
 
    if ([message.senderId isEqualToString:self.senderId] && [self.profPics objectForKey:[NSString stringWithFormat:@"%hd",self.currentUser.userId]] != nil) {
        return [self.profPics objectForKey:[NSString stringWithFormat:@"%hd",self.currentUser.userId]];
    }
    else if([self.profPics objectForKey:[NSString stringWithFormat:@"%hd",self.otherUser.userId]] != nil) {
        return [self.profPics objectForKey:[NSString stringWithFormat:@"%hd",self.otherUser.userId]];
    }
    UIImage *sqImg = [UIImage imageNamed:@"Profile Place Holder"];
    JSQMessagesAvatarImageFactory *avatarFact = [[JSQMessagesAvatarImageFactory alloc] initWithDiameter:sqImg.size.height/2.0];
    UIImage *circImg = [avatarFact circularAvatarImage:sqImg];
    JSQMessagesAvatarImage *userProfPic = [JSQMessagesAvatarImage avatarWithImage:circImg];
    return userProfPic;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.messages count];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    if ([message.senderId isEqualToString:self.senderId]) {
        self.clickedUser = self.currentUser;
    }
    else{
        self.clickedUser = self.otherUser;
    }
    ProfileViewController *vc = [[UIStoryboard storyboardWithName:@"ViewProfile" bundle:nil] instantiateViewControllerWithIdentifier:@"Profile"];
    vc.user = self.clickedUser;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    if(message.isMediaMessage) {
        CGRect  viewRect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        UIImageView *iv = [[PFImageView alloc] initWithFrame:viewRect];
        iv.backgroundColor = [UIColor colorWithWhite:.5 alpha:.5];
        if ([message.media isKindOfClass:[JSQPhotoMediaItem class]]) {
            iv.image = ((JSQPhotoMediaItem *)message.media).image;
        }
        iv.contentMode = UIViewContentModeScaleAspectFit;
        
        NSArray *windows = [[UIApplication sharedApplication] windows];
        UIWindow *window = [windows lastObject];
        UIView *keyboard = [window.subviews lastObject];
        
        [keyboard addSubview:iv];
        [keyboard bringSubviewToFront:iv];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissFullscreenImage:)];
        [iv addGestureRecognizer:tap];
        iv.userInteractionEnabled = YES;
    }
}

- (void) dismissFullscreenImage: (UITapGestureRecognizer *) sender
{
    [sender.view removeFromSuperview];
}

- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}

- (void)chatDidAccidentallyDisconnect{
    UIAlertController *alert = [alertViewController connectionError];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)chatDidReconnect{
    [self.messages removeAllObjects];
    refreshCount = 0;
    [SVProgressHUD show];
    [self getQBMessagesWithRefresh:NO];
}

- (void) chatDidNotConnectWithError:(NSError *)error {
    UIAlertController *alert = [alertViewController connectionError];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) chatDidFailWithStreamError:(NSError *)error {
    UIAlertController *alert = [alertViewController connectionError];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Navigation

- (IBAction)viewItem:(id)sender
{
    if (!self.item)
    {
        UIAlertController *alert = [alertViewController itemDoesNotExist];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ItemDetails" bundle:nil];
        ItemDetailsViewController *vc = storyboard.instantiateInitialViewController;
        vc.item = self.item;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
@end


