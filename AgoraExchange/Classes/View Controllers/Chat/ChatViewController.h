//
//  ChatViewController.h
//  
//
//  Created by Travis Chambers on 7/6/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessages.h>
#import <JSQMessagesViewController/JSQMessage.h>
#import <Quickblox/Quickblox.h>
#import "ImageCropView.h"
#import "User+CoreDataClass.h"
#import "Item+CoreDataClass.h"
#import "ChatManager.h"
#import "Conversation+CoreDataClass.h"
#import "Message+CoreDataClass.h"
#import "AgoraImageManager.h"

@interface ChatViewController : JSQMessagesViewController < ImageCropViewControllerDelegate >

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageView;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageView;
@property (strong, nonatomic) Conversation *chatDialog;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *viewItemBarButton;
@property (strong, nonatomic) Item *item;
@property (strong, nonatomic) User *otherUser;
@property (strong,nonatomic) User *currentUser;
@property (strong, nonatomic) User *clickedUser;
@property bool didStartNewConversation;

- (IBAction)viewItem:(id)sender;

@end
