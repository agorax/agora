//
//  PasswordSentViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissViewControllerDelegate <NSObject>

- (void)dismissVC;

@end

@interface PasswordSentViewController : UIViewController
- (IBAction)dismiss:(id)sender;
@property (nonatomic, weak) id<DismissViewControllerDelegate> delegate;

@end
