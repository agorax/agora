//
//  RegistrationSuccessfulViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "RegistrationSuccessfulViewController.h"
#import <SAConfettiView/SAConfettiView-Swift.h>

@interface RegistrationSuccessfulViewController ()

@end

@implementation RegistrationSuccessfulViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    SAConfettiView *confetti = [[SAConfettiView alloc] initWithFrame:self.view.frame];
    [confetti startConfetti];
    [self.view insertSubview:confetti atIndex:0];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dismiss:(id)sender
{
    [self.delegate dismissVC];
}
@end
