//
//  ForgotPasswordViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UIButton *sendNewPasswordButton;

@end