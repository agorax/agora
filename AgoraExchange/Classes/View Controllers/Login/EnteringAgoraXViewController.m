//
//  EnteringAgoraXViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "EnteringAgoraXViewController.h"
#import "NetworkManager.h"
#import "SessionManager.h"
#import <Onboard/OnboardingViewController.h>
#import <Onboard/OnboardingContentViewController.h>

@interface EnteringAgoraXViewController ()

@end

@implementation EnteringAgoraXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    if (self.name == nil || [self.name length] == 0)
    {
        self.welcomeLabel.text = @"Welcome.";
    }
    else
    {
        self.welcomeLabel.text = [[@"Welcome " stringByAppendingString:self.name] stringByAppendingString:@"."];
    }
    [self enterAgora];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self setupShimmerView];
}

- (void) setupShimmerView
{
    self.shimmeringView = [[FBShimmeringView alloc] initWithFrame:self.shimmeringPlaceholder.frame];
    [self.view addSubview:self.shimmeringView];
    
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:self.shimmeringView.bounds];
    [loadingLabel setTextColor:[UIColor whiteColor]];
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    loadingLabel.text = NSLocalizedString(@"Loading your marketplace...", nil);
    self.shimmeringView.contentView = loadingLabel;
    [self.shimmeringView setShimmeringSpeed:180];
    [self.shimmeringView setShimmeringHighlightLength:1];
    [self.shimmeringView setShimmeringAnimationOpacity:0.2];
    
    // Start shimmering.
    self.shimmeringView.shimmering = YES;
}


- (void)enterAgora {
    [NetworkManager reloadMarketplaceWithToken:[SessionManager fetchAgoraToken] school: [SessionManager getCurrentUser].school andCompletionBlock:^(AgoraError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                [SessionManager logout];
            } else {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Tabs" bundle:nil];
                UITabBarController *tabBarController = storyboard.instantiateInitialViewController;
                [tabBarController setModalPresentationStyle:UIModalPresentationCustom];
                [tabBarController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [self.delegate presentViewController:tabBarController];
            }
        });
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
