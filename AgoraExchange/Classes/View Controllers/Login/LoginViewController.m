    //
//  LoginViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "LoginViewController.h"
#import "EnteringAgoraXViewController.h"
#import "decorateTextFields.h"
#import "SVProgressHUD.h"
#import "alertViewController.h"
#import "Constants.h"
#import <Quickblox/Quickblox.h>
#import "RegisterViewController.h"
#import "NetworkManager.h"
#import "LoginPresenter.h"
#import "SessionManager.h"
#import <Onboard/OnboardingViewController.h>
#import <Onboard/OnboardingContentViewController.h>
#import <SSKeychain/SSKeychain.h>
#import "ChatManager.h"

@interface LoginViewController () <UITextFieldDelegate, EnteringAgoraXDelegate>
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) LoginPresenter *loginPresenter;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.loginPresenter = [[LoginPresenter alloc] init];
    [self setupNotificationCenter];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    [self setTextFields];
    [self setButtons];

    if ([SessionManager fetchAgoraToken] && [SessionManager getCurrentUser]) {
        [self authenticationSuccess];
    }
}

#pragma mark - keyboard stuff

- (void)keyboardWasShown:(NSNotification *)notification
{
    if (0 != self.view.frame.origin.y)
    {
        return;
    }
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    int movement = 0;
    movement = [UIScreen mainScreen].bounds.size.height - self.enterAgoraButton.frame.size.height - self.enterAgoraButton.frame.origin.y - keyboardSize.height - 20;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.emailField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    [self resetViewOffset];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan && (self.emailField.isFirstResponder || self.passwordField.isFirstResponder))
    {
        [self.emailField resignFirstResponder];
        [self.passwordField resignFirstResponder];
        [self resetViewOffset];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.emailField.isFirstResponder)
    {
        [self.emailField resignFirstResponder];
        [self.passwordField becomeFirstResponder];
    }
    else
    {
        [self.passwordField resignFirstResponder];
        [self authenticate];
    }
    return NO;
}

- (void)resetViewOffset
{
    int movement = self.view.frame.origin.y;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement);
    }];
}

#pragma mark - notification stuff & view lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupNotificationCenter
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userLoggedOut:)
                                                 name:@"Logout"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void) userLoggedOut:(NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

//No Nav bar on first page
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - signin stuff

- (void) authenticate
{
    [self disableUI];
    NSString *email = self.emailField.text;
    NSString *password = self.passwordField.text;
    NSString *apnsToken =  [SessionManager fetchAPNSToken];
    [self.loginPresenter loginWithEmail:email password:password apnsToken:apnsToken andCompletionBlock:^(int userId, AgoraError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self enableUI];
            if (error) {
                UIAlertController *alert = ((unsigned long)error.statusCode == 502) ? [alertViewController createAccount: self]
                :[alertViewController dismissableAlertWithError:error];
                [self displayAlert:alert];
            } else {
                [self authenticationSuccess];
            }
        });
    }];
}

-(void)authenticationSuccess {
    self.name = [SessionManager getCurrentUser].name;
    [[ChatManager sharedInstance] establishConnection];
    NSString *onboarded = [SSKeychain passwordForService:@"Onboarding" account:@"com.AgoraExchange.keychain"];
    ([onboarded isEqualToString:@"YES"]) ? [self performSegueWithIdentifier:@"EnterAgora" sender:self] : [self initiateOnboarding];
}

-(void)displayAlert:(UIAlertController *)alert {
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)initiateOnboarding {
    [SSKeychain setPassword:@"YES" forService:@"Onboarding" account:@"com.AgoraExchange.keychain"];
    NSMutableArray<OnboardingContentViewController *> *contentViewControllers = [[NSMutableArray alloc] init];
    OnboardingContentViewController *safety = [OnboardingContentViewController contentWithTitle:@"Safety" body:@"Access only granted with .edu email." image:[UIImage imageNamed:@"OnboardingSafe"] buttonText:nil action:^{
        // do something here when users press the button, like ask for location services permissions, register for push notifications, connect to social media, or finish the onboarding process
    }];
    OnboardingContentViewController *community = [OnboardingContentViewController contentWithTitle:@"Build Community" body:@"Promote academic, philanthropic, or social events." image:[UIImage imageNamed:@"OnboardingCommunity"] buttonText:nil action:^{
        // do something here when users press the button, like ask for location services permissions, register for push notifications, connect to social media, or finish the onboarding process
    }];
    OnboardingContentViewController *sell = [OnboardingContentViewController contentWithTitle:@"Snap, Sell, Simple" body:@"Make money FAST- snap a pic, add a description, and sell.  " image:[UIImage imageNamed:@"OnboardingSell"] buttonText:nil action:^{
        // do something here when users press the button, like ask for location services permissions, register for push notifications, connect to social media, or finish the onboarding process
    }];
    OnboardingContentViewController *smart = [OnboardingContentViewController contentWithTitle:@"Shop Smart" body:@"Search through thousands of textbooks starting at just one dollar." image:[UIImage imageNamed:@"OnboardingSmart"] buttonText:nil action:^{
        // do something here when users press the button, like ask for location services permissions, register for push notifications, connect to social media, or finish the onboarding process
    }];
    OnboardingContentViewController *search = [OnboardingContentViewController contentWithTitle:@"Search For Anything" body:@"dozens of categories to find whatever you are looking for" image:[UIImage imageNamed:@"OnboardingSearch"] buttonText:nil action:^{
        // do something here when users press the button, like ask for location services permissions, register for push notifications, connect to social media, or finish the onboarding process
    }];
    OnboardingContentViewController *profile = [OnboardingContentViewController contentWithTitle:@"Customize your Profile" body:@"choose a profile picture, bio, and a safe meet-up location." image:[UIImage imageNamed:@"OnboardingProfile"] buttonText:nil action:^{
        // do something here when users press the button, like ask for location services permissions, register for push notifications, connect to social media, or finish the onboarding process
    }];
    OnboardingContentViewController *message = [OnboardingContentViewController contentWithTitle:@"Message other Users" body:@"Easily chat with users in-app to bargain, buy, and sell." image:[UIImage imageNamed:@"OnboardingMessage"] buttonText:nil action:^{
        // do something here when users press the button, like ask for location services permissions, register for push notifications, connect to social media, or finish the onboarding process
    }];
    [contentViewControllers addObjectsFromArray:@[safety, community, sell, smart, search, profile, message]];
    [self styleOnboardingContentViewControllers:contentViewControllers];

    OnboardingViewController *onboardingVC = [OnboardingViewController onboardWithBackgroundImage:[UIImage imageNamed:@"BinaryWalkway"] contents:contentViewControllers];
    [onboardingVC setShouldBlurBackground:NO];
    [onboardingVC setShouldFadeTransitions:YES];
    [onboardingVC setAllowSkipping:YES];
    [onboardingVC setSkipHandler:^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        EnteringAgoraXViewController *enteringAgoraX = (EnteringAgoraXViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EnterAgora"];
        enteringAgoraX.name = self.name = [SessionManager getCurrentUser].name;
        enteringAgoraX.delegate = self;
        [self presentViewControllerDismissingPrevious:enteringAgoraX animated:YES completion:nil];
    }];
    [self presentViewController:onboardingVC animated:YES completion:nil];
}

- (void)styleOnboardingContentViewControllers:(NSArray<OnboardingContentViewController *>*)contentViewControllers {
    for(OnboardingContentViewController *vc in contentViewControllers) {
        [vc setTopPadding:40];
        [vc setBottomPadding:20];
        [vc setUnderIconPadding:10];
        [vc setUnderTitlePadding:150];
    }
}

-(void)enableUI {
    [self.view setUserInteractionEnabled:YES];
    [SVProgressHUD dismiss];
}

-(void)disableUI {
    [self.emailField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    [self resetViewOffset];
    [self.view setUserInteractionEnabled:NO];
    [SVProgressHUD show];
}

- (void)registerForRemoteNotifications{
    if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        return;
    }
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
#endif
}

#pragma mark - Setup Views

- (void)setTextFields
{
    [decorateTextFields decorateLoginTextField:_emailField];
    [decorateTextFields decorateLoginPasswordTextField:_passwordField];
    self.emailField.delegate = self;
    self.passwordField.delegate = self;
    self.emailField.keyboardType = UIKeyboardTypeEmailAddress;
}

- (void)setButtons
{
    self.enterAgoraButton.layer.borderWidth = 1;
    self.enterAgoraButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark - delegate methods

- (void)dismissVC
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EnterAgora"])
    {
        EnteringAgoraXViewController * vc = segue.destinationViewController;
        vc.name = self.name;
        vc.delegate = self;
        self.passwordField.text = @"";
    }
    if ([segue.identifier isEqualToString:@"Register"])
    {
        if (self.createAccount)
        {
            self.createAccount = NO;
            RegisterViewController * rvc = segue.destinationViewController;
            rvc.email = self.emailField.text;
            rvc.password = self.passwordField.text;
        }
    }
}
- (IBAction)login:(id)sender {
    [self authenticate];
}

#pragma mark - entering agorax delegate method

-(void)presentViewController:(UIViewController *)presentVC {
    [self presentViewControllerDismissingPrevious:presentVC animated:YES completion:nil];
}

-(void)presentViewControllerDismissingPrevious:(UIViewController* _Nonnull)controller animated:(BOOL)animated completion:(void (^ __nullable)(void))completion {

    UIViewController* visibleController = self;
    {
        UIViewController* temp;
        while( ( temp = visibleController.presentedViewController ) != nil ) {
            visibleController = temp;
        }
    }

    if( visibleController == self ) {
        // no previous controller to dismiss
        [self presentViewController:controller animated:animated completion:completion];
    } else {
        // create a temporary snapshot of the visible controller's entire window
        // and add to the current view controller's window until animation completed
        UIWindow* visibleWindow = visibleController.view.window;
        UIView* tempView = [visibleWindow snapshotViewAfterScreenUpdates:NO];
        UIView* rootView = self.view.window.subviews[0];
        tempView.frame = [rootView convertRect:visibleWindow.bounds fromView:visibleWindow];
        [rootView addSubview:tempView];

        [self dismissViewControllerAnimated:NO completion:^(){
            [self presentViewController:controller animated:animated completion:^(){
                [tempView removeFromSuperview];
                if( completion ) {
                    completion();
                }
            }];
        }];
    }
}

@end
