//
//  RegisterViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController

//picker
@property (strong, nonatomic) IBOutlet UIPickerView *schoolPicker;
@property (strong, nonatomic) NSString *schoolSelected;
@property (strong, nonatomic) NSArray *pickerArray;

@property (strong, nonatomic) IBOutlet UITextField *emailField;

@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UITextField *retypePasswordField;
@property (strong, nonatomic) IBOutlet UIImageView *profilePicture;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
- (IBAction)register:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (nonatomic) BOOL buttonIsBlue;
- (IBAction)viewTermsAndConditions:(id)sender;
- (IBAction)viewPrivacyPolicy:(id)sender;

@end

