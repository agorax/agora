//
//  ForgotPasswordViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "PasswordSentViewController.h"
#import "decorateTextFields.h"
#import "SVProgressHUD.h"
#import "alertViewController.h"
#import <Parse/Parse.h>
#import "SVProgressHUD.h"

@interface ForgotPasswordViewController () <DismissViewControllerDelegate>
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [decorateTextFields decorateLoginTextField:_emailField];
    self.emailField.keyboardType = UIKeyboardTypeEmailAddress;
    self.sendNewPasswordButton.layer.borderWidth = 1;
    self.sendNewPasswordButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan && self.emailField.isFirstResponder)
    {
        [self.emailField resignFirstResponder];
    }
}


#pragma mark - Delegate Methods

- (void)dismissVC
{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"ForgotPasswordComplete"])
    {
        [self.view setUserInteractionEnabled:NO];
        self.navigationItem.hidesBackButton = YES;
        [SVProgressHUD show];
        [PFUser requestPasswordResetForEmailInBackground: self.emailField.text block:^(BOOL succeeded, NSError * _Nullable error) {
            [self.view setUserInteractionEnabled:YES];
            self.navigationItem.hidesBackButton = NO;
            [SVProgressHUD dismiss];
            if (error && error.code == 204)
            {
                UIAlertController *alert = [alertViewController mustEnterEmail];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            if (error && error.code == 125)
            {
                UIAlertController *alert = [alertViewController emailDoesNotExist];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            if (error)
            {
                UIAlertController *alert = [alertViewController errorResettingPassword];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            else
            {
                [self performSegueWithIdentifier:@"ForgotPasswordComplete" sender:self];
            }
        }];
    }
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    if ([segue.identifier isEqualToString:@"ForgotPasswordComplete"])
    {
        PasswordSentViewController *nextVC = segue.destinationViewController;
        nextVC.delegate = self;
    }
}

@end
