
//
//  RegisterViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterPresenter.h"
#import "pickerData.h"
#import "camera.h"
#import "decorateTextFields.h"
#import "alertViewController.h"
#import "SVProgressHUD.h"
#import "Constants.h"
#import <Parse/Parse.h>
#import "RegistrationSuccessfulViewController.h"
#import <Quickblox/Quickblox.h>
#import <SSKeychain/SSKeychain.h>

@interface RegisterViewController () <UITextFieldDelegate, DismissViewControllerDelegate>
@property (nonatomic, strong) NSString* sentTo;
@property (nonatomic) BOOL shouldSegue;
@property (nonatomic, strong) NSMutableDictionary *collegeNameToEmail;
@property (nonatomic, strong) RegisterPresenter *registerPresenter;
@end

@implementation RegisterViewController

- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    self.registerPresenter = [[RegisterPresenter alloc] init];
    [self setupPicker];
    [self setTextFields];
    [self setButtons];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup Views

- (void)setTextFields
{
    [decorateTextFields decorateLoginTextField:_emailField];
    [decorateTextFields decorateLoginPasswordTextField:_passwordField];
    [decorateTextFields decorateLoginPasswordTextField:_retypePasswordField];
    self.emailField.delegate = self;
    self.passwordField.delegate = self;
    self.retypePasswordField.delegate = self;
    self.emailField.keyboardType = UIKeyboardTypeEmailAddress;
    
    self.emailField.placeholder = [[pickerData collegeNameToEmail] objectForKey:_schoolSelected];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    
    self.emailField.text = self.email;
    self.passwordField.text = self.password;
}

- (void)setupPicker
{
    self.pickerArray = [pickerData getPickerElementsForScreen:@"Register"];
    if ([_pickerArray count] != 0)
    {
        _schoolSelected = _pickerArray [0];
    }
}

- (void)setButtons
{
    self.registerButton.layer.borderWidth = 1;
    self.registerButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark - keyboard stuff

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    if (0 != self.view.frame.origin.y)
    {
        return;
    }
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    int movement = 0;
    movement = [UIScreen mainScreen].bounds.size.height - self.registerButton.frame.origin.y - self.registerButton.bounds.size.height - keyboardSize.height - 50;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan && (self.emailField.isFirstResponder || self.passwordField.isFirstResponder || self.retypePasswordField.isFirstResponder))
    {
        [self.emailField resignFirstResponder];
        [self.passwordField resignFirstResponder];
        [self.retypePasswordField resignFirstResponder];
        [self resetViewOffset];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.emailField.isFirstResponder)
    {
        [self.emailField resignFirstResponder];
        [self.passwordField becomeFirstResponder];
    }
    else if (self.passwordField.isFirstResponder)
    {
        [self.passwordField resignFirstResponder];
        [self.retypePasswordField becomeFirstResponder];
    }
    else
    {
        [self.retypePasswordField resignFirstResponder];
        [self resetViewOffset];
        if ([self verify] == YES)
        {
            [self signUp];
        }
    }
    return NO;
}

- (void) resetViewOffset
{
    int movement = self.view.frame.origin.y;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement);
    }];
}

#pragma mark - Sign Up User
- (void)signUp
{
    [self disableUI];
    [self.registerPresenter registerUserWithEmail:self.emailField.text password:self.passwordField.text school:self.schoolSelected andCompletionBlock:^(AgoraError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self enableUI];
            if (error) {
                UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                [self displayAlert:alert];
            } else {
                [self performSegueWithIdentifier:@"Registration Complete" sender:self];
            }
        });
    }];
}

-(void)displayAlert:(UIAlertController *)alert{
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)disableUI {
    [self.view setUserInteractionEnabled:NO];
    self.navigationItem.hidesBackButton = YES;
    [SVProgressHUD show];
}

-(void)enableUI {
    [self.view setUserInteractionEnabled:YES];
    self.navigationItem.hidesBackButton = NO;
    [SVProgressHUD dismiss];
}
#pragma mark - Picker Delegate Methods

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.schoolSelected = [_pickerArray objectAtIndex:row];
    self.emailField.placeholder = [NSString stringWithFormat:@"%@ email", [[pickerData collegeNameToEmail] objectForKey:self.schoolSelected]];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerArray.count;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSAttributedString *) pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = self.pickerArray[row];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
}

#pragma mark - Delegate Methods

- (void)dismissVC
{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}



#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Registration Complete"])
    {
        [self.emailField resignFirstResponder];
        [self.passwordField resignFirstResponder];
        [self.retypePasswordField resignFirstResponder];
        [self resetViewOffset];
        if ([self verify]) {
            [self signUp];
        }
    }
    return NO;
}

-(BOOL)verify {
    AgoraError *error = [self.registerPresenter validateRegistrationWithEmail:self.emailField.text password:self.passwordField.text retypePassword:self.retypePasswordField.text school:self.schoolSelected];
    if (error) {
        UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
        [self displayAlert:alert];
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Registration Complete"])
    {
        ((RegistrationSuccessfulViewController *)segue.destinationViewController).delegate = self;
    }
}

- (IBAction)registerButtonClicked:(id)sender
{
    
}

- (IBAction)viewTermsAndConditions:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Me" bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"Terms And Conditions"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)viewPrivacyPolicy:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Me" bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"Privacy Policy"];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
