//
//  EnteringAgoraXViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shimmer/FBShimmeringView.h"

@protocol EnteringAgoraXDelegate <NSObject>

-(void)presentViewController:(UIViewController *)presentVC;

@end

@interface EnteringAgoraXViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *shimmeringPlaceholder;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (strong, nonatomic) FBShimmeringView *shimmeringView;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) id<EnteringAgoraXDelegate> delegate;

@end
