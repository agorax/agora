//
//  MarketplaceViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MarketplaceViewController.h"
#import "MarketplaceTableViewCell.h"
#import <Parse/Parse.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import <CHTCollectionViewWaterfallLayout/CHTCollectionViewWaterfallLayout.h>
#import "ItemCellNode.h"
#import "Shimmer/FBShimmeringView.h"
#import "ItemDetailsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "alertViewController.h"
#import "AgoraColors.h"
#import "pictureNames.h"
#import "pickerData.h"
#import "User+CoreDataClass.h"
#import "SessionManager.h"
#import "AppDelegate.h"
#import <pop/pop.h>
#import "AgoraExchange-Swift.h"

@interface MarketplaceViewController () <ASCollectionDelegate, ASCollectionDataSource, CHTCollectionViewDelegateWaterfallLayout, UIScrollViewDelegate, AKPickerViewDataSource, AKPickerViewDelegate, ModernSearchBarDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userPickerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categorySelectedButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userSelectedButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryPickerHeightConstraint;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *categoryCollabsibleButtonImages;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *userCollapsibleButtonImages;


@property (nonatomic, readonly) CHTCollectionViewWaterfallLayout *collectionViewLayout;
@property (nonatomic, strong) ASCollectionNode *collectionNode;
@property (nonatomic, strong) NSArray *recentItemsArray;
@property (nonatomic, strong) FBShimmeringView *shimmeringView;
@property int currentSchoolPicture;
@property (nonatomic, strong) UIButton *backToTop;
@property (nonatomic, strong) NSArray *pictureNameArray;
@property (nonatomic, strong) User *currentUser;

@property (nonatomic, strong) NSMutableArray <ModernSearchBarModel *> *searchSuggestions;
@property (strong, nonatomic) NSString *category;

@end

static NSString *cellID = @"cellID";
static NSArray *options = nil;
static int optionHeight = 50;
static NSArray *categories = nil;
int i;

@implementation MarketplaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentSchoolPicture = 0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    self.currentUser = [SessionManager getCurrentUser];
    self.schoolLabel.text = self.currentUser.school;
    [self setupCategoryPickerView];
    //[self setupUserPickerView];
    [self setupCollectionView];
    [self setupImageCycle];
    [self setupButtons];
    self.mainScrollView.delegate = self;
    //[self setupShimmerView];
    [self setupSearchBar];
    // Do any additional setup after loading the view.
}

#pragma mark - setup views

- (void)setupButtons {
    self.categorySelectedButton.layer.borderWidth = 0.5;
    //self.userSelectedButton.layer.borderWidth = 0.5;
    [self.categorySelectedButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    //self.userSelectedButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)setupSearchBar {
    self.searchBar.delegateModernSearchBar = self;
    [self repopulateSearchSuggestions];
    [self.searchBar setPlaceholder:@"Search by name or isbn"];
//    self.searchBar.shadowView_alpha = 0.8;
//    self.searchBar.suggestionsView_backgroundColor = [UIColor brownColor];
//    self.searchBar.suggestionsView_contentViewColor = [UIColor redColor];
//    self.searchBar.suggestionsView_separatorStyle = UITableViewCellSelectionStyleGray;
//    self.searchBar.suggestionsView_spaceWithKeyboard = 20;
//    self.searchBar.suggestionsView_verticalSpaceWithSearchBar = 10;
//    self.searchBar.suggestionsView_searchIcon_height = 40;
//    self.searchBar.suggestionsView_searchIcon_width = 40;
}

- (void)repopulateSearchSuggestions {
    if (!self.searchSuggestions) {
        self.searchSuggestions = [[NSMutableArray alloc] init];
    }
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    if (![self.category isEqualToString:@"All Items"]) {
        NSPredicate *itemCategoryPredicate = [NSPredicate predicateWithFormat:@"category == %@", self.category];
        [request setPredicate:itemCategoryPredicate];
    }
    NSAsynchronousFetchRequest *asyncRequest = [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request completionBlock:^(NSAsynchronousFetchResult *results) {
        dispatch_async(dispatch_get_main_queue(), ^{
           // [self.searchSuggestions removeAllObjects];
            NSArray <Item *>*items = [[results finalResult] copy];
            for (Item *item in items) {
                [self.searchSuggestions addObject:[[ModernSearchBarModel alloc] initWithTitle:item.name image:[UIImage imageNamed:item.category]]];
            }
            [self.searchBar setDatasWithUrlWithDatas:self.searchSuggestions];
        });
    }];
    [[self privateContext] executeRequest:asyncRequest error:nil];
}

- (void)setupCollectionView
{
    self.collectionNode = [[ASCollectionNode alloc] initWithCollectionViewLayout:[CHTCollectionViewWaterfallLayout new]];
    self.collectionNode.dataSource = self;
    self.collectionNode.delegate = self;
    self.collectionNode.backgroundColor = [UIColor clearColor];
    self.collectionNode.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.collectionNodeContentView addSubnode:self.collectionNode];
    self.collectionNode.frame = self.collectionNodeContentView.bounds;
    
    
    self.collectionViewLayout.minimumInteritemSpacing = 10;
    self.collectionViewLayout.minimumColumnSpacing = 10;
    self.collectionViewLayout.itemRenderDirection = CHTCollectionViewWaterfallLayoutItemRenderDirectionLeftToRight;
    self.collectionViewLayout.sectionInset = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    self.collectionNode.view.tag = 2;
    [self refreshWithSearchString:@""];
}

- (CHTCollectionViewWaterfallLayout *)collectionViewLayout {
    return (CHTCollectionViewWaterfallLayout *)self.collectionNode.view.collectionViewLayout;
}

- (void)setupImageCycle
{
    self.pictureNameArray = [pictureNames pictureNamesForSchool:self.currentUser.school];
    [self cycleImage:self];
    [NSTimer scheduledTimerWithTimeInterval:10.0
                                     target:self
                                   selector:@selector(cycleImage:)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)setupCategoryPickerView
{
    self.categoryPickerView.delegate = self;
    self.categoryPickerView.dataSource = self;
    self.categoryPickerView.interitemSpacing = 4.0;
    self.categoryPickerView.backgroundColor = [UIColor clearColor];
    self.categoryPickerView.pickerViewStyle = AKPickerViewStyleFlat;
    if (categories == nil) {
        categories = [[NSArray alloc] initWithArray:[pickerData getPickerElementsForScreen:@"Choose Category"]];
    }
    self.category = [categories firstObject];
    [self.categorySelectedButton setTitle:categories[0] forState:UIControlStateNormal];
    
}

//- (void)setupUserPickerView
//{
//    self.userPickerView.delegate = self;
//    self.userPickerView.dataSource = self;
//    self.userPickerView.interitemSpacing = 4.0;
//    self.userPickerView.pickerViewStyle = AKPickerViewStyleFlat;
//    self.userPickerView.backgroundColor = [UIColor clearColor];
//    [self.userSelectedButton setTitle:@"All Users" forState:UIControlStateNormal];
//}

#pragma mark - cycle through case western images

- (IBAction)cycleImage:(id)sender
{
    self.schoolImageView.image = [UIImage imageNamed:self.pictureNameArray[self.currentSchoolPicture]];
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.schoolImageView.layer addAnimation:transition forKey:nil];
    self.currentSchoolPicture++;
    if (self.currentSchoolPicture == self.pictureNameArray.count) {
        self.currentSchoolPicture = 0;
    }
}

#pragma mark - searchbar delegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self refreshWithSearchString:searchText];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self refreshWithSearchString:searchBar.text];
}

-(void)onClickShadowViewWithShadowView:(UIView *)shadowView {
    [self.searchBar resignFirstResponder];
}

- (void)onClickItemWithUrlSuggestionsViewWithItem:(ModernSearchBarModel *)item {
    [self.searchBar setText:item.title];
    [self.searchBar resignFirstResponder];
}

#pragma mark - AKPickerViewDataSource
//categorypickerview has tag == 3, user picker view has tag == 4

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView
{
    return (pickerView.tag == 3) ? categories.count : [self userPickerCount];
}

-(int)userPickerCount
{
    return 8;
}

 - (UIImage *)pickerView:(AKPickerView *)pickerView imageForItem:(NSInteger)item
 {
     if (pickerView.tag == 3) {
         return [UIImage imageNamed:categories[item]];
     } else {
         if (item == 0) {
             return [UIImage imageNamed:@"All Users"];
         }
         return [UIImage imageNamed:@"Profile Picture Small"];
     }
 }
 
#pragma mark - AKPickerViewDelegate

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item
{
    if (pickerView.tag == 3) {
        [self.categorySelectedButton setTitle:categories[item] forState:UIControlStateNormal];
        self.category = categories[item];
        [self repopulateSearchSuggestions];
        [self refreshWithSearchString:self.searchBar.text];
        ([self.category isEqualToString:@"Textbooks"]) ? [self.searchBar setPlaceholder:@"Search by name or isbn"] : [self.searchBar setPlaceholder:@"Search by name"];
    }
//    else if (pickerView.tag == 4)
//    {
//        [self.userSelectedButton setTitle:@"Justin" forState:UIControlStateNormal];
//    }
}

- (void)itemButtonPressedAtIndex:(NSInteger)item withPicker:(AKPickerView *)pickerView{
    UIButton *button = self.categorySelectedButton;
    NSString *selectedButtonTitle = (pickerView.tag == 3) ? categories[item] : @"Justin";
    [button setTitle:selectedButtonTitle forState:UIControlStateNormal];
    NSLayoutConstraint *pickerConstraint = (pickerView.tag == 3) ? self.categoryPickerHeightConstraint : self.userPickerHeightConstraint;
    NSLayoutConstraint *buttonConstraint = (pickerView.tag == 3) ? self.categorySelectedButtonHeightConstraint : self.userSelectedButtonHeightConstraint;
    NSArray <UIImageView *> *collapsibleButtons = (pickerView.tag == 3) ? self.categoryCollabsibleButtonImages : self.userCollapsibleButtonImages;
    [self toggleCategorySelectWithPickerView:pickerView button:button pickerViewConstraint:pickerConstraint buttonConstraint:buttonConstraint andCollapsibleButtons:collapsibleButtons];
}

- (void)toggleCategorySelectWithPickerView:(AKPickerView *)pickerView
                                    button:(UIButton *)button
                      pickerViewConstraint:(NSLayoutConstraint *)pickerViewConstraint
                          buttonConstraint:(NSLayoutConstraint *)buttonConstraint
                     andCollapsibleButtons:(NSArray <UIImageView *>*)collabsibleButtons{
    button.contentMode = UIViewContentModeRedraw;
    pickerView.contentMode = UIViewContentModeTop;
    UIColor *color;
    CGFloat alpha;
    CGFloat pickerHeight;
    CGFloat buttonHeight;
    BOOL collapseButton;
    if (pickerView.frame.size.height == 0) {
        pickerHeight = 60.0f;
        buttonHeight = buttonConstraint.constant - 10.0f;
        color = [UIColor clearColor];
        alpha = 1.0f;
        collapseButton = true;
    } else {
        pickerHeight = 0.0f;
        buttonHeight = buttonConstraint.constant + 10.0f;
        color = [UIColor blackColor];
        alpha = 0.0f;
        collapseButton = false;
    }

    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        //[button setBackgroundColor:color];
        [pickerView setAlpha:alpha];
    } completion:^(BOOL finished) {
        pickerViewConstraint.constant = pickerHeight;
        buttonConstraint.constant = buttonHeight;
        [UIView animateWithDuration:0.75 animations:^{
            [self.searchBar setNeedsLayout];
            [self.view layoutIfNeeded];
            for (UIImageView *imageView in collabsibleButtons) {
                imageView.hidden = collapseButton;
            }
        }];
    }];
}

- (IBAction)toggleShowPickerAction:(id)sender {
    AKPickerView *pickerView = self.categoryPickerView;
    NSLayoutConstraint *pickerConstraint = (((UIButton *)sender).tag == 3) ? self.categoryPickerHeightConstraint : self.userPickerHeightConstraint;
    NSLayoutConstraint *buttonConstraint = (((UIButton *)sender).tag == 3) ? self.categorySelectedButtonHeightConstraint : self.userSelectedButtonHeightConstraint;
    NSArray <UIImageView *> *collapsibleButtons = (pickerView.tag == 3) ? self.categoryCollabsibleButtonImages : self.userCollapsibleButtonImages;
    [self toggleCategorySelectWithPickerView:pickerView button:(UIButton *)sender pickerViewConstraint:pickerConstraint buttonConstraint:buttonConstraint andCollapsibleButtons:collapsibleButtons];
}


#pragma mark - collection node datasource and delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.recentItemsArray count];
}

- (ASCellNodeBlock)collectionView:(ASTableView *)tableView nodeBlockForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ASCellNode *(^ASCellNodeBlock)() = ^ASCellNode *() {
        ItemCellNode *node = [ItemCellNode new];
        if (indexPath.item < [self.recentItemsArray count]) {
            [node setupWithItem:self.recentItemsArray[indexPath.item]];
        }
        return node;
    };
    
    return ASCellNodeBlock;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath {
    return ASSizeRangeMake(CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], 0.0f), CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], CGFLOAT_MAX));
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(CHTCollectionViewWaterfallLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionNode.view calculatedSizeForNodeAtIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ItemDetails" bundle:nil];
    ItemDetailsViewController *vc = storyboard.instantiateInitialViewController;
    vc.item = self.recentItemsArray[indexPath.row];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - scrolling animation stuff

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView.tag == 2)
    {
        if (!self.backToTop) {
            self.backToTop = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.backToTop addTarget:self
                       action:@selector(scrollToTop:)
             forControlEvents:UIControlEventTouchUpInside];
            [self.backToTop setTitle:@"Case Western" forState:UIControlStateNormal];
            self.backToTop.frame = CGRectMake(0, 0, 80.0, 20.0);
            self.backToTop.titleLabel.font = [UIFont systemFontOfSize:11.0f];
            self.backToTop.center = CGPointMake(self.view.center.x, self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height + 8.0f + self.backToTop.bounds.size.height/2.0f);
            self.backToTop.backgroundColor = [AgoraColors agoraBlueColor];
            self.backToTop.layer.cornerRadius = 5.0f;
            
            [self.backToTop addTarget:self
                       action:@selector(decreaseButtonAlpha:)
             forControlEvents:UIControlEventTouchDown];
            [self.backToTop addTarget:self
                       action:@selector(increaseButtonAlpha:)
             forControlEvents:UIControlEventTouchUpOutside];
            [self.backToTop addTarget:self
                       action:@selector(increaseButtonAlpha:)
             forControlEvents:UIControlEventTouchCancel];
            
            [UIView transitionWithView:self.view
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self.view addSubview:self.backToTop];
                            } completion:nil];
        }
        else
        {
            [UIView transitionWithView:self.backToTop
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                self.backToTop.hidden = NO;
                            } completion:nil];
        }
    }
    else if(scrollView.tag == 1)
    {
        if (self.backToTop)
        {
            [UIView transitionWithView:self.backToTop
                              duration:0.4f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                self.backToTop.hidden = YES;
                            } completion:nil];
        }
    }
}

- (void) scrollToTop: (UIButton *)sender
{
    sender.alpha = 1.0f;
    [UIView transitionWithView:self.mainScrollView
                      duration:0.4f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                            [self.mainScrollView setContentOffset:CGPointMake(0, 0 - self.navigationController.navigationBar.bounds.size.height - [UIApplication sharedApplication].statusBarFrame.size.height)];
                    } completion:^(BOOL finished) {
                        self.collectionNode.view.scrollEnabled = NO;
                        self.mainScrollView.scrollEnabled = YES;
                    }];
    
    [UIView transitionWithView:self.collectionNode.view
                      duration:0.2f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.collectionNode.view setContentOffset:CGPointMake(0, 0)];
                    } completion:^(BOOL finished) {
                        self.collectionNode.view.scrollEnabled = NO;
                        self.mainScrollView.scrollEnabled = YES;
                    }];
    self.backToTop.hidden = YES;
}

- (IBAction)increaseButtonAlpha:(UIButton *)sender
{
    sender.alpha = 1.0f;
}

- (IBAction)decreaseButtonAlpha:(UIButton *)sender
{
    sender.alpha = 0.75f;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self toggleAlpha: scrollView];
    //tag == 1: main scroll view
    
    if (scrollView.tag == 1 && scrollView.contentOffset.y + self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height < self.collectionNodeContentView.frame.origin.y)
    {
        self.collectionNode.view.scrollEnabled = NO;
    }
    else
    {
        self.collectionNode.view.scrollEnabled = YES;
    }
    
    if (scrollView.tag == 2 && scrollView.contentOffset.y + scrollView.bounds.size.height == scrollView.contentSize.height)
    {
        self.mainScrollView.scrollEnabled = NO;
    }
    else
    {
        self.mainScrollView.scrollEnabled = YES;
    }
}

- (void)toggleAlpha:(UIScrollView *)scrollView
{
    float actualOffset = scrollView.contentOffset.y + self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
    float minLabelAlpha = 0.65f;
    float minImageAlpha = 0.8f;
    //tag = 1 : main scroll view
    //tag = 3 : category collection view
    if (scrollView.tag == 1)
    {
        [self.schoolImageView setAlpha:1.0f - (1.0f - minImageAlpha)*actualOffset / (self.schoolImageView.bounds.size.height - self.labelBackingView.bounds.size.height) ];
        [self.labelBackingView setAlpha:minLabelAlpha + (1.0f - minLabelAlpha)*actualOffset / (self.schoolImageView.bounds.size.height - self.labelBackingView.bounds.size.height)];
        CGPoint labelCenter = CGPointMake(self.labelBackingView.center.x - (self.labelBackingView.center.x - (20.0f + self.schoolLabel.bounds.size.width/2.0f))*actualOffset / (self.schoolImageView.bounds.size.height - self.labelBackingView.bounds.size.height), self.labelBackingView.bounds.size.height / 2.0f);
        [self.schoolLabel setCenter:labelCenter];
    }
}

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[MarketplaceViewController managedObjectContext]];
    return concurrentContext;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - shimmer/refresh

- (IBAction)refreshWithSearchString:(NSString *)searchString
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    [request setFetchLimit:200];
    if (searchString && [searchString length] > 0) {
        NSPredicate *itemNamePredicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@", searchString];
        if (![self.category isEqualToString:@"All Items"]) {
            if ([self.category isEqualToString:@"Textbooks"]) {
                NSPredicate *itemIsbnPredicate = [NSPredicate predicateWithFormat:@"isbn.stringValue contains[cd] %@", searchString];
                NSPredicate *itemCategoryPredicate = [NSPredicate predicateWithFormat:@"category == %@", self.category];
                NSArray <NSPredicate *> *orPredicateArray = [NSArray arrayWithObjects:itemNamePredicate, itemIsbnPredicate, nil];
                NSCompoundPredicate *compoundOrPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:orPredicateArray];
                NSArray <NSPredicate *> *andPredicateArray = [NSArray arrayWithObjects:compoundOrPredicate, itemCategoryPredicate, nil];
                NSCompoundPredicate *compoundAndPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:andPredicateArray];
                [request setPredicate:compoundAndPredicate];
            } else {
                NSPredicate *itemCategoryPredicate = [NSPredicate predicateWithFormat:@"category == %@", self.category];
                
                NSArray <NSPredicate *> *predicateArray = [NSArray arrayWithObjects:itemNamePredicate, itemCategoryPredicate, nil];
                
                NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
                [request setPredicate:compoundPredicate];
            }
        } else {
            [request setPredicate:itemNamePredicate];
        }
    } else {
        if (![self.category isEqualToString:@"All Items"]) {
            NSPredicate *itemCategoryPredicate = [NSPredicate predicateWithFormat:@"category == %@", self.category];
            [request setPredicate:itemCategoryPredicate];
        }
    }
    NSAsynchronousFetchRequest *asyncRequest = [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request completionBlock:^(NSAsynchronousFetchResult *results) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.recentItemsArray = [[results finalResult] copy];
            [self.collectionNode reloadData];
        });
    }];
    [[MarketplaceViewController managedObjectContext] executeRequest:asyncRequest error:nil];
}

- (void) setupShimmerView
{
    self.shimmeringView = [[FBShimmeringView alloc] initWithFrame:self.shimmerPlaceholder.bounds];
    [self.shimmerPlaceholder addSubview:self.shimmeringView];
    self.shimmeringView.contentView = self.shimmerContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setupShimmerView];
    [self.collectionNode.view setBounces:NO];
}

@end
