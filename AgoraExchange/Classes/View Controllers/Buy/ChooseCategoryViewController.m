//
//  ChooseCategoryViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/3/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ChooseCategoryViewController.h"
#import "CategoryTableViewCell.h"
#import "pickerData.h"
#import "SearchViewController.h"
#import "AgoraColors.h"

@interface ChooseCategoryViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSString *category;

@end

static NSString *cellID = @"cellID";
static NSArray *categories = nil;
static int optionHeight = 50;

@implementation ChooseCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    // Do any additional setup after loading the view.
}

- (void) setupTableView
{
    self.tableView.backgroundColor = [UIColor clearColor];
    if (categories == nil) {
        categories = [[NSArray alloc] initWithArray:[pickerData getPickerElementsForScreen:@"Choose Category"]];
    }
    [self.tableView registerNib:[UINib nibWithNibName:@"CategoryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
}

#pragma mark - table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    [cell setupWithCategory:[categories objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return optionHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.category = categories[indexPath.row];
    [self performSegueWithIdentifier:@"Category Selected" sender:self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Category Selected"])
    {
        ((SearchViewController *)segue.destinationViewController).category = self.category;
    }
}

@end
