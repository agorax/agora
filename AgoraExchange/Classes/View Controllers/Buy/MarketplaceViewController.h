//
//  MarketplaceViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKPickerView.h"

@class ModernSearchBar;

@interface MarketplaceViewController : UIViewController
@property (weak, nonatomic) IBOutlet ModernSearchBar *searchBar;
@property (weak, nonatomic) IBOutlet AKPickerView *categoryPickerView;
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *recentListingsView;
@property (weak, nonatomic) IBOutlet UIView *collectionNodeContentView;
@property (weak, nonatomic) IBOutlet UILabel *shimmerContent;
@property (weak, nonatomic) IBOutlet UIView *shimmerPlaceholder;
@property (weak, nonatomic) IBOutlet UIImageView *schoolImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *labelBackingView;
@property (weak, nonatomic) IBOutlet UIButton *categorySelectedButton;



@end
