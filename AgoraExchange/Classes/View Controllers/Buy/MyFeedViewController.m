//
//  MyFeedViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/26/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MyFeedViewController.h"
#import <Parse/Parse.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import <CHTCollectionViewWaterfallLayout/CHTCollectionViewWaterfallLayout.h>
#import "ItemCellNode.h"
#import "alertViewController.h"
#import "ItemDetailsViewController.h"

@interface MyFeedViewController () <ASCollectionDelegate, ASCollectionDataSource, CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic, readonly) CHTCollectionViewWaterfallLayout *collectionViewLayout;
@property (nonatomic, strong) ASCollectionNode *collectionNode;
@property (nonatomic, strong) NSMutableArray <PFObject *> *recentItemsArray;
@property int skipCount;

@end

static int queryLimit = 20;

@implementation MyFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCollectionView];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    // Do any additional setup after loading the view.
}

- (void)setupCollectionView
{
    self.collectionNode = [[ASCollectionNode alloc] initWithCollectionViewLayout:[CHTCollectionViewWaterfallLayout new]];
    self.collectionNode.dataSource = self;
    self.collectionNode.delegate = self;
    self.collectionNode.backgroundColor = [UIColor clearColor];
    self.collectionNode.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.collectionNode.frame = self.view.bounds;
    [self.view addSubnode:self.collectionNode];
    self.collectionViewLayout.minimumInteritemSpacing = 10;
    self.collectionViewLayout.minimumColumnSpacing = 10;
    self.collectionViewLayout.itemRenderDirection = CHTCollectionViewWaterfallLayoutItemRenderDirectionLeftToRight;
    self.collectionViewLayout.sectionInset = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    [self refreshWithCompletionBlock:nil];
}

- (CHTCollectionViewWaterfallLayout *)collectionViewLayout {
    return (CHTCollectionViewWaterfallLayout *)self.collectionNode.view.collectionViewLayout;
}

#pragma mark - collection node datasource and delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.recentItemsArray count];
}

- (ASCellNodeBlock)collectionView:(ASTableView *)tableView nodeBlockForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ASCellNode *(^ASCellNodeBlock)() = ^ASCellNode *() {
        ItemCellNode *node = [ItemCellNode new];
        
        [node setupWithItem:self.recentItemsArray[indexPath.item]];
        
        return node;
    };
    
    return ASCellNodeBlock;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath {
    return ASSizeRangeMake(CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], 0.0f), CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], CGFLOAT_MAX));
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(CHTCollectionViewWaterfallLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionNode.view calculatedSizeForNodeAtIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ItemDetails" bundle:nil];
    ItemDetailsViewController *vc = storyboard.instantiateInitialViewController;
    vc.item = self.recentItemsArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL)shouldBatchFetchForCollectionView:(ASCollectionView *)collectionView
{
    if (self.recentItemsArray.count % queryLimit != 0)
    {
        return NO;
    }
    return YES;
}

- (void)collectionView:(ASCollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    self.skipCount++;
    [self refreshWithCompletionBlock:^(NSError *error) {
        if (!error)
        {
            [context completeBatchFetching:YES];
        }
        else
        {
            self.skipCount--;
        }
    }];
}

- (NSArray *)indexPathsForObjects:(NSArray <PFObject *> *)data {
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (int i = 0; i < data.count; i++)
    {
        NSInteger itemIndex = [self.recentItemsArray indexOfObject:data[i]];
        [indexPaths addObject:[NSIndexPath indexPathForItem:itemIndex inSection:0]];
    }
    return indexPaths;
}

- (void) refreshWithCompletionBlock:(void (^)(NSError *error))completionBlock
{
    PFQuery *query = [[PFQuery alloc] initWithClassName:@"Item"];
    [query whereKey:@"userId" containedIn:[PFUser currentUser][@"followingArray"]];
    query.limit = queryLimit;
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (error)
        {
            if (completionBlock) {
                completionBlock(error);
            }
            UIAlertController *alert = [alertViewController errorRefreshingFeed];
            [self presentViewController:alert animated:YES completion:nil];
        }
        if (objects && [objects count] == 0 && !self.recentItemsArray)
        {
            UIAlertController *alert = [alertViewController followPeople];
            [self presentViewController:alert animated:YES completion:nil];
        }
        if (completionBlock)
        {
            [self.recentItemsArray addObjectsFromArray:objects];
            NSArray *addedIndexPaths = [self indexPathsForObjects:objects];
            [self.collectionNode.view insertItemsAtIndexPaths:addedIndexPaths];
            completionBlock(nil);
        }
        else
        {
            self.recentItemsArray = [[NSMutableArray alloc] initWithArray:objects];
            [self.collectionNode reloadData];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

}


@end
