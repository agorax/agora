//
//  SearchViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/2/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "SearchViewController.h"
#import "ItemDetailsViewController.h"
#import <Parse/Parse.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import <CHTCollectionViewWaterfallLayout/CHTCollectionViewWaterfallLayout.h>
#import "ItemCellNode.h"
#import "IGLDropDownMenu.h"
#import "alertViewController.h"
#import "SVProgressHUD.h"

@interface SearchViewController () <ASCollectionDelegate, ASCollectionDataSource, CHTCollectionViewDelegateWaterfallLayout, UISearchBarDelegate, UISearchControllerDelegate, UIGestureRecognizerDelegate, IGLDropDownMenuDelegate, UISearchResultsUpdating>

@property (nonatomic, readonly) CHTCollectionViewWaterfallLayout *collectionViewLayout;
@property (nonatomic, strong) ASCollectionNode *collectionNode;
@property (nonatomic, strong) NSMutableArray <PFObject *> *itemsArray;
@property (nonatomic) int indexSelected;

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) PFQuery *query;
@property int updatedSearchText;
@property int skipCount;

@property (strong, nonatomic) IGLDropDownMenu *dropDownMenu;

@end

static NSArray *options = nil;
static int queryLimit = 20;
static double delayInSeconds = 0.5;

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    [self setupSearch];
    [self setupDropDownOptions];
    [self setupDropDown];
    [self setupCollectionView];
    if (!self.category)
    {
        self.navigationItem.title = @"Global";
    }
    else
    {
        self.navigationItem.title = self.category;
    }
    // Do any additional setup after loading the view.
}

-(void)viewWillLayoutSubviews
{
    [self.searchController.searchBar setShowsCancelButton:NO animated:NO];
    [super viewWillLayoutSubviews];
}

#pragma mark - search stuff
- (void)setupSearch
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.searchController.delegate = self;
    self.searchController.searchResultsUpdater = self;
    [self.searchBarView addSubview: self.searchController.searchBar];
    self.definesPresentationContext = YES;
    self.searchController.searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.searchController.searchBar sizeToFit];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
}

#pragma mark - search bar delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.skipCount = 0;
    [self reloadResultsWithCompletionBlock:nil];
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    if (!self.searchBarView.contentView)
    {
        self.searchBarView.contentView = self.searchController.searchBar;
    }
    
    self.updatedSearchText++;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                   {
                       self.updatedSearchText--;
                       if (self.updatedSearchText == 0)
                       {
                           self.skipCount = 0;
                           [self reloadResultsWithCompletionBlock:nil];
                       }
                   });
    
    
}

#pragma mark - dropdown stuff

- (void)setupDropDownOptions
{
    if (options == nil) {
        options = [[NSArray alloc] initWithObjects:@"Recent", @"Popular", @"Price $$$", @"Price $", nil];
    }
}

- (void)setupDropDown
{
    //configure dropdown items
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < [options count]; i++)
    {
        IGLDropDownItem *option = [[IGLDropDownItem alloc] init];
        //[option setIconImage:[UIImage imageNamed:@"Buy Tab Icon"]];
        [option setText:options[i]];
        [dropdownItems addObject:option];
    }
    
    //configure dropdown menu
    self.dropDownMenu = [[IGLDropDownMenu alloc] init];
    self.dropDownMenu.delegate = self;
    self.dropDownMenu.dropDownItems = dropdownItems;
    [self.dropDownMenu setFrame:self.dropDownView.bounds];
    [self.dropDownView addSubview:self.dropDownMenu];
    self.dropDownMenu.menuText = options[0];
    self.dropDownMenu.paddingLeft = 15;
    self.dropDownMenu.type = IGLDropDownMenuTypeStack;
    self.dropDownMenu.gutterY = 5;
    self.dropDownMenu.itemAnimationDelay = 0.1;
    self.dropDownMenu.rotate = IGLDropDownMenuRotateRandom;
    [self.dropDownMenu reloadView];
}

#pragma mark - dropdown delegate

- (void) dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    self.dropDownMenu.menuText = options[index];
    self.skipCount = 0;
    [self reloadResultsWithCompletionBlock:nil];
}

- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu expandingChanged:(BOOL)isExpending
{
    //5.6 is pretty arbitrary, but dropdownmenu size hasnt been set yet
    if (isExpending)
    {
        [self.dropDownView setFrame:CGRectMake(self.dropDownView.frame.origin.x, self.dropDownView.frame.origin.y, self.dropDownView.frame.size.width, self.dropDownView.frame.size.height*5.6)];
    }
    else
    {
        [self.dropDownView setFrame:CGRectMake(self.dropDownView.frame.origin.x, self.dropDownView.frame.origin.y, self.dropDownView.frame.size.width, self.dropDownView.frame.size.height/5.6)];
    }
}

#pragma mark - keyboard stuff

- (void) dismissKeyboard
{
    [self.searchController.searchBar resignFirstResponder];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (![self.searchController.searchBar isFirstResponder])
    {
        return NO;
    }
    if ([touch.view isDescendantOfView:self.dropDownView]) {
        return NO;
    }
    return YES;
}

#pragma mark - collection view stuff

- (void)setupCollectionView
{
    self.collectionNode = [[ASCollectionNode alloc] initWithCollectionViewLayout:[CHTCollectionViewWaterfallLayout new]];
    self.collectionNode.dataSource = self;
    self.collectionNode.delegate = self;
    self.collectionNode.backgroundColor = [UIColor clearColor];
    self.collectionNode.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.collectionNode.frame = self.collectionNodeContainerView.bounds;
    [self.collectionNodeContainerView addSubnode:self.collectionNode];
    self.collectionViewLayout.minimumInteritemSpacing = 10;
    self.collectionViewLayout.minimumColumnSpacing = 10;
    self.collectionViewLayout.itemRenderDirection = CHTCollectionViewWaterfallLayoutItemRenderDirectionLeftToRight;
    self.collectionViewLayout.sectionInset = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    self.skipCount = 0;
    [SVProgressHUD show];
    [self reloadResultsWithCompletionBlock:nil];
}

- (CHTCollectionViewWaterfallLayout *)collectionViewLayout {
    return (CHTCollectionViewWaterfallLayout *)self.collectionNode.view.collectionViewLayout;
}

#pragma mark - collection view datasource and delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.itemsArray count];
}

- (ASCellNodeBlock)collectionView:(ASTableView *)tableView nodeBlockForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ASCellNode *(^ASCellNodeBlock)() = ^ASCellNode *() {
        ItemCellNode *node = [ItemCellNode new];
        
        [node setupWithItem:self.itemsArray[indexPath.item]];
        
        return node;
    };
    
    return ASCellNodeBlock;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath {
    return ASSizeRangeMake(CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], 0.0f), CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], CGFLOAT_MAX));
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.indexSelected = indexPath.row;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ItemDetails" bundle:nil];
    ItemDetailsViewController *vc = storyboard.instantiateInitialViewController;
    vc.item = self.itemsArray[self.indexSelected];
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(CHTCollectionViewWaterfallLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionNode.view calculatedSizeForNodeAtIndexPath:indexPath];
}

- (BOOL)shouldBatchFetchForCollectionView:(ASCollectionView *)collectionView
{
    if (self.itemsArray.count % queryLimit != 0)
    {
        return NO;
    }
    return YES;
}

- (void)collectionView:(ASCollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    self.skipCount++;
    [self reloadResultsWithCompletionBlock:^(NSError *error) {
        if (!error)
        {
            [context completeBatchFetching:YES];
        }
        else
        {
            self.skipCount--;
        }
    }];
}

- (void) reloadResultsWithCompletionBlock:(void (^)(NSError *error))completionBlock
{
    [self.searchBarView setShimmering:YES];
    if (!self.query)
    {
        self.query = [[PFQuery alloc] initWithClassName:@"Item"];
        self.query.limit = queryLimit;
        [self.query whereKey:@"school" equalTo:[[PFUser currentUser] objectForKey:@"school"]];
    }
    else
    {
        [self.query cancel];
    }
    
    self.query.skip = self.skipCount * queryLimit;
    
    NSArray *searchTextArray = [self getWordsFromSearchBar];
    for (int i = 0; i < searchTextArray.count; i++)
    {
        NSString *word = [searchTextArray[i] lowercaseString];
        [self.query whereKey:@"lowercaseName" containsString:word];
    }
    if (self.category)
    {
        [self.query whereKey:@"category" equalTo:self.category];
    }
    
    if ([self.dropDownMenu.menuText isEqualToString:@"Price $$$"])
    {
        [self.query orderByDescending:@"price"];
    }
    else if ([self.dropDownMenu.menuText isEqualToString:@"Price $"])
    {
        [self.query orderByAscending:@"price"];
    }
    else if ([self.dropDownMenu.menuText isEqualToString:@"Recent"])
    {
        [self.query orderByDescending:@"createdAt"];
    }
    else
    {
        [self.query orderByDescending:@"numberLikes"];
    }
    [self.query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        [self.searchBarView setShimmering:NO];
        if (error)
        {
            if (completionBlock) {
                completionBlock(error);
            }
            UIAlertController *alert = [alertViewController searchUnsuccessful];
            [self presentViewController:alert animated:YES completion:nil];
        }
        if (completionBlock)
        {
            [self.itemsArray addObjectsFromArray:objects];
            NSArray *addedIndexPaths = [self indexPathsForObjects:objects];
            [self.collectionNode.view insertItemsAtIndexPaths:addedIndexPaths];
            completionBlock(nil);
        }
        else
        {
            self.itemsArray = [[NSMutableArray alloc] initWithArray:objects];
            [self.collectionNode reloadData];
        }
    }];
}

- (NSArray *) getWordsFromSearchBar
{
    NSMutableArray *returnedArray = [[NSMutableArray alloc] init];
    NSString * searchBarText = [self.searchController.searchBar.text lowercaseString];
    int index = 0;
    for(int i = 0; i < searchBarText.length; i++)
    {
        if ([searchBarText characterAtIndex:i] == ' ')
        {
            [returnedArray addObject:[searchBarText substringWithRange:NSMakeRange(index, i - index)]];
            index = i;
        }
    }
    if ([returnedArray count] == 0) {
        [returnedArray addObject:searchBarText];
    }
    return returnedArray;
}

- (NSArray *)indexPathsForObjects:(NSArray <PFObject *> *)data {
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (int i = 0; i < data.count; i++)
    {
        NSInteger itemIndex = [self.itemsArray indexOfObject:data[i]];
        [indexPaths addObject:[NSIndexPath indexPathForItem:itemIndex inSection:0]];
    }
    return indexPaths;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.searchBarView setShimmering:NO];
}

@end
