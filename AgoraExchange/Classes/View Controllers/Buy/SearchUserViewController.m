//
//  SearchUserViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/26/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "SearchUserViewController.h"
#import "SVProgressHUD.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "alertViewController.h"
#import "UserTableViewCell.h"
#import "ProfileInfoViewController.h"

@interface SearchUserViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchResultsUpdating>

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) NSArray <PFUser *> *userArray;
@property (strong, nonatomic) PFQuery *compoundQuery;
@property int updatedSearchText;

@end

static NSString *cellID = @"cellID";
static int cellHeight = 70;
static double delayInSeconds = 0.5;

@implementation SearchUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self setupSearch];
    [self refresh];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    // Do any additional setup after loading the view.
}

- (void)setupSearch
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    //self.searchController.searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.searchController.searchBar.placeholder = @"Search by Email or Name";
}

- (void)setupTableView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"UserTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
    self.tableView.backgroundColor = [UIColor clearColor];
}

#pragma mark - search bar delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self refresh];
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    self.updatedSearchText++;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                   {
                       self.updatedSearchText--;
                       if (self.updatedSearchText == 0)
                       {
                           [self refresh];
                       }
                   });
}

#pragma mark - table view datasource and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.userArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.profilePictureImageView.file = self.userArray[indexPath.row][@"picture"];
    cell.nameLabel.text = self.userArray[indexPath.row][@"name"];
    cell.emailLabel.text = self.userArray[indexPath.row][@"email"];
    [cell.profilePictureImageView loadInBackground:^(UIImage * _Nullable image, NSError * _Nullable error) {
        //hardcoded corner radius
        cell.profilePictureImageView.layer.cornerRadius = 25;
    }];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ViewProfile" bundle:nil];
    UIViewController *vc = storyboard.instantiateInitialViewController;
    //vc.user = self.userArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (void)refresh
{
    [SVProgressHUD show];
    [self.compoundQuery cancel];
    NSString *lowercaseSearch = [self.searchController.searchBar.text lowercaseString];
    PFQuery *queryEmail = [PFUser query];
    [queryEmail whereKey:@"username" containsString: lowercaseSearch];
    
    PFQuery *queryName = [PFUser query];
    [queryName whereKey:@"lowercaseName" containsString:lowercaseSearch];
    
    NSArray <PFQuery *> *array = [[NSArray alloc] initWithObjects:queryEmail, queryName, nil];
    
    self.compoundQuery = [PFQuery orQueryWithSubqueries:array];
    [self.compoundQuery whereKey:@"school" equalTo:[[PFUser currentUser] objectForKey:@"school"]];
    
    self.compoundQuery.limit = 20;
    [self.compoundQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (error) {
            UIAlertController *alert = [alertViewController errorFindingUsers];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        self.userArray = [NSArray arrayWithArray:objects];
        [self.tableView reloadData];
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

@end
