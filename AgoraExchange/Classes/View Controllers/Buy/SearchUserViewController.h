//
//  SearchUserViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/26/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchUserViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
