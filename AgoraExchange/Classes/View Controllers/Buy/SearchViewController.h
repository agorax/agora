//
//  SearchViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/2/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shimmer/FBShimmeringView.h"

@interface SearchViewController : UIViewController
@property (strong, nonatomic) IBOutlet FBShimmeringView *searchBarView;
@property (strong, nonatomic) IBOutlet UIView *dropDownView;
@property (strong, nonatomic) IBOutlet UIView *collectionNodeContainerView;
@property (strong, nonatomic) NSString *category;

@end
