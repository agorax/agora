//
//  TermsAndConditionsViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/22/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *containerView;

@end
