//
//  UsersFollowedViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/26/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "UsersFollowedViewController.h"
#import "SVProgressHUD.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "alertViewController.h"
#import "UserTableViewCell.h"
#import "ProfileInfoViewController.h"

@interface UsersFollowedViewController () <
UITableViewDelegate,
UITableViewDataSource,
UISearchResultsUpdating,
UISearchBarDelegate
>

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) NSArray <PFUser *> *userArray;
@property (strong, nonatomic) NSMutableArray <PFUser *> *filteredUserArray;
@property int indexSelected;

@end

static NSString *cellID = @"cellID";
static int cellHeight = 70;

@implementation UsersFollowedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    [self setupTableView];
    [self setupSearch];
    // Do any additional setup after loading the view.
}

- (void)setupSearch
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    //self.searchController.searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)setupTableView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"UserTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
    self.tableView.backgroundColor = [UIColor clearColor];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshControl:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    [self refresh];
}

- (void)refreshControl:(UIRefreshControl *)refreshControl {
    if (self.searchController.active)
    {
        [refreshControl endRefreshing];
        return;
    }
    [self refresh];
    [refreshControl endRefreshing];
}

#pragma mark - search bar delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self updateSearchResultsForSearchController:self.searchController];
}

#pragma mark - search updater

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self.filteredUserArray removeAllObjects];
    NSPredicate *namePredicate = [[NSPredicate alloc] init];
    namePredicate = [NSPredicate predicateWithFormat:@"lowercaseName CONTAINS[c] %@", searchController.searchBar.text];
    NSPredicate *emailPredicate = [[NSPredicate alloc] init];
    emailPredicate = [NSPredicate predicateWithFormat:@"username CONTAINS[c] %@", searchController.searchBar.text];
    
    NSArray <NSPredicate *> *array = [NSArray arrayWithObjects:namePredicate, emailPredicate, nil];
    
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:array];
    
    self.filteredUserArray = [[NSMutableArray alloc] initWithArray:[self.userArray filteredArrayUsingPredicate:compoundPredicate]];
    [self.tableView reloadData];
}

#pragma mark - table view datasource and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.searchController.active)
    {
        return [self.filteredUserArray count];
    }
    return [self.userArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    if (self.searchController.active)
    {
        cell.profilePictureImageView.file = self.filteredUserArray[indexPath.row][@"picture"];
        cell.nameLabel.text = self.filteredUserArray[indexPath.row][@"name"];
        cell.emailLabel.text = self.filteredUserArray[indexPath.row][@"email"];
        [cell.profilePictureImageView loadInBackground:^(UIImage * _Nullable image, NSError * _Nullable error) {
            
        }];
        return cell;
    }
    cell.profilePictureImageView.file = self.userArray[indexPath.row][@"picture"];
    cell.nameLabel.text = self.userArray[indexPath.row][@"name"];
    cell.emailLabel.text = self.userArray[indexPath.row][@"email"];
    [cell.profilePictureImageView loadInBackground:^(UIImage * _Nullable image, NSError * _Nullable error) {
        
    }];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ViewProfile" bundle:nil];
    ProfileInfoViewController *vc = storyboard.instantiateInitialViewController;
//    if (self.searchController.active)
//    {
//        vc.user = self.filteredUserArray[indexPath.row];
//    }
//    else
//    {
//        vc.user = self.userArray[indexPath.row];
//    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (void)refresh
{
    if (!self.userArray || self.userArray.count == 0)
    {
        [SVProgressHUD show];
    }
    PFQuery *query = [PFUser query];
    [query whereKey:@"objectId" containedIn:[PFUser currentUser][@"followingArray"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (error) {
            UIAlertController *alert = [alertViewController errorLoadingUsersFollowed];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        else if (objects.count == 0)
        {
            UIAlertController *alert = [alertViewController noUsersFollowed];
            [self presentViewController:alert animated:YES completion:nil];
        }
        self.userArray = [NSArray arrayWithArray:objects];
        self.filteredUserArray = [NSMutableArray arrayWithArray:objects];
        [self.tableView reloadData];
    }];
}

#pragma mark - Navigation

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

@end
