//
//  ChangePasswordViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/11/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "decorateTextFields.h"
#import "SVProgressHUD.h"
#import "alertViewController.h"
#import "AgoraColors.h"
#import "dismissButton.h"
#import <Parse/Parse.h>
#import <Quickblox/Quickblox.h>
#import <SSKeychain/SSKeychain.h>

@interface ChangePasswordViewController () <UITextFieldDelegate>

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTextFields];
    [self setupButtons];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupTextFields
{
    [decorateTextFields decorateLoginTextField:self.oldPasswordField];
    [decorateTextFields decorateLoginPasswordTextField:self.proposedPasswordField];
    self.oldPasswordField.delegate = self;
    self.proposedPasswordField.delegate = self;
}

- (void)setupButtons
{
    self.changePasswordButton.layer.borderWidth = 1;
    self.changePasswordButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (BOOL)checkPasswordLength: (NSString* ) password
{
    if([password length] < 8)
    {
        UIAlertController *alert = [alertViewController passwordNotLongEnough];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (IBAction)authenticate:(id)sender {
    [SVProgressHUD show];
    [self.view setUserInteractionEnabled:NO];
    [self.navigationItem setHidesBackButton:YES];
    if([self checkPasswordLength:self.proposedPasswordField.text]){
        [PFUser logInWithUsernameInBackground:[PFUser currentUser].email password:self.oldPasswordField.text block:^(PFUser *user, NSError *error){
            if (!error) {
                QBUpdateUserParameters *updateParameters = [QBUpdateUserParameters new];
                updateParameters.oldPassword = self.oldPasswordField.text;
                updateParameters.password =  self.proposedPasswordField.text;
                
                [QBRequest updateCurrentUser:updateParameters successBlock:^(QBResponse *response, QBUUser *user) {
                    // User updated successfully
                    [PFUser currentUser].password = self.proposedPasswordField.text;
                    [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                        [SVProgressHUD dismiss];
                        [self.view setUserInteractionEnabled:YES];
                        [self.oldPasswordField resignFirstResponder];
                        [self.proposedPasswordField resignFirstResponder];
                        [self resetViewOffset];
                        [SSKeychain setPassword:self.proposedPasswordField.text forService:@"QBUser" account:@"com.AgoraExchange.keychain"];
                        [self displayActionConfirmation:@"Successful!" enteringAgoraX: NO];
                        if (error) {
                            [[PFUser currentUser] saveEventually];
                        }

                    }];

                } errorBlock:^(QBResponse *response) {
                    [SVProgressHUD dismiss];
                    [self.view setUserInteractionEnabled:YES];
                    [self.navigationItem setHidesBackButton:NO];
                    UIAlertController *alert = [alertViewController resetUnsuccesful];
                    [self presentViewController:alert animated:YES completion:nil];
                    NSLog(@"response %@", response.error);
                }];
            }
            else {
                [SVProgressHUD dismiss];
                [self.view setUserInteractionEnabled:YES];
                [self.navigationItem setHidesBackButton:NO];
                if (error.code == 100)
                {
                    UIAlertController *alert = [alertViewController checkNetworkConnection];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else
                {
                    UIAlertController *alert = [alertViewController wrongPassword];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
        }];
    }
    else{
        [self.view setUserInteractionEnabled:YES];
        [self.navigationItem setHidesBackButton:NO];
        [SVProgressHUD dismiss];
        UIAlertController *alert = [alertViewController wrongPassword];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void) displayActionConfirmation: (NSString *) confirmationMessage enteringAgoraX: (BOOL)enteringAgoraX
{
    [self.tabBarController.tabBar setUserInteractionEnabled:NO];
    [self.navigationController.navigationBar setHidden:YES];
    UIView *addedView = [[UIView alloc]initWithFrame:self.view.bounds];
    addedView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    addedView.userInteractionEnabled = YES;
    
    UIView *promptView = [[UIView alloc]initWithFrame:CGRectMake(20, addedView.frame.size.height/3.0, addedView.frame.size.width - 40, addedView.frame.size.height/3.0)];
    promptView.backgroundColor = [AgoraColors agoraGrayColor];
    promptView.layer.cornerRadius = 5;
    [addedView addSubview:promptView];
    
    UILabel *message = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, promptView.frame.size.width,2.0*promptView.frame.size.height/5.0)];
    message.textAlignment = NSTextAlignmentCenter;
    message.center = CGPointMake(promptView.frame.size.width/2.0, promptView.frame.size.height/ 10.0);
    message.text = confirmationMessage;
    message.textColor = [UIColor whiteColor];
    message.font = [UIFont systemFontOfSize: 20];
    [promptView addSubview:message];
    
    dismissButton *dismiss = [[dismissButton alloc] initWithFrame:CGRectMake(0, 0, 9.0*promptView.frame.size.width/10.0, 3.0*promptView.frame.size.height/10.0)];
    dismiss.center = CGPointMake(promptView.frame.size.width/2.0, 4.0*promptView.frame.size.height/5.0);
    dismiss.backgroundColor = [AgoraColors agoraGreenColor];
    dismiss.layer.cornerRadius = 5;
    dismiss.viewToBeDismissed = addedView;
    dismiss.enteringAgoraX = enteringAgoraX;
    [dismiss setTitle:@"OK" forState:UIControlStateNormal];
    dismiss.titleLabel.font = [UIFont systemFontOfSize: 30];
    [dismiss addTarget:self
                action:@selector(dismissPrompt:)
      forControlEvents:UIControlEventTouchUpInside];
    [promptView addSubview:dismiss];
    
    [self.view addSubview:addedView];
}

- (IBAction)dismissPrompt:(id)sender
{
    if ([QBSession currentSession].currentUser)
    {
        [QBRequest logOutWithSuccessBlock:^(QBResponse *response) {
            // Successful logout
            [PFUser logOut];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"Logout"
             object:self];
            [self.navigationController.navigationBar setHidden:NO];
        } errorBlock:^(QBResponse *response) {
            // Handle error
            UIAlertController *alert = [alertViewController loginTryAgain];
            [self presentViewController:alert animated:YES completion:nil];
            [self.navigationController.navigationBar setHidden:NO];
            [self.tabBarController.tabBar setUserInteractionEnabled:YES];
        }];
    }
    else
    {
        [PFUser logOut];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Logout"
         object:self];
        [self.navigationController.navigationBar setHidden:NO];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan && (self.oldPasswordField.isFirstResponder || self.proposedPasswordField.isFirstResponder))
    {
        [self.oldPasswordField resignFirstResponder];
        [self.proposedPasswordField resignFirstResponder];
        [self resetViewOffset];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.oldPasswordField.isFirstResponder)
    {
        [self.oldPasswordField resignFirstResponder];
        [self.proposedPasswordField becomeFirstResponder];
    }
    else
    {
        [self authenticate:self];
    }
    return NO;
}

- (void)resetViewOffset
{
    int movement = self.view.frame.origin.y;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement);
    }];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    if (0 != self.view.frame.origin.y)
    {
        return;
    }
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    int movement = 0;
    movement = [UIScreen mainScreen].bounds.size.height - self.changePasswordButton.frame.size.height - self.changePasswordButton.frame.origin.y - keyboardSize.height - 20;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    }];
}

@end
