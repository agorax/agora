//
//  MyReviewsViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyReviewsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
