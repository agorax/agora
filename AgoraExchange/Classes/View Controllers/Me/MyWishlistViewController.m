//
//  MyWishlistViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/11/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MyWishlistViewController.h"
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import <CHTCollectionViewWaterfallLayout/CHTCollectionViewWaterfallLayout.h>
#import <Parse/Parse.h>
#import "ItemCellNode.h"
#import "SVProgressHUD.h"
#import "ItemDetailsViewController.h"
#import "AgoraColors.h"
#import "alertViewController.h"

@interface MyWishlistViewController () <ASCollectionDelegate, ASCollectionDataSource, CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic, readonly) CHTCollectionViewWaterfallLayout *collectionViewLayout;
@property (nonatomic, strong) ASCollectionNode *collectionNode;
@property (nonatomic, strong) NSArray <PFObject *> *itemsArray;
@property (nonatomic) int indexSelected;

@end

@implementation MyWishlistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [AgoraColors agoraGradientColor];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    [self setupCollectionView];
    // Do any additional setup after loading the view.
}

#pragma mark - collection view stuff

- (void)setupCollectionView
{
    self.collectionNode = [[ASCollectionNode alloc] initWithCollectionViewLayout:[CHTCollectionViewWaterfallLayout new]];
    self.collectionNode.dataSource = self;
    self.collectionNode.delegate = self;
    self.collectionNode.backgroundColor = [UIColor clearColor];
    self.collectionNode.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.collectionNode.frame = self.view.bounds;
    [self.view addSubnode:self.collectionNode];
    self.collectionViewLayout.minimumInteritemSpacing = 10;
    self.collectionViewLayout.minimumColumnSpacing = 10;
    self.collectionViewLayout.itemRenderDirection = CHTCollectionViewWaterfallLayoutItemRenderDirectionLeftToRight;
    self.collectionViewLayout.sectionInset = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    [self reloadWishlist];
}

- (CHTCollectionViewWaterfallLayout *)collectionViewLayout {
    return (CHTCollectionViewWaterfallLayout *)self.collectionNode.view.collectionViewLayout;
}

#pragma mark - collection view datasource and delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.itemsArray count];
}

- (ASCellNodeBlock)collectionView:(ASTableView *)tableView nodeBlockForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ASCellNode *(^ASCellNodeBlock)() = ^ASCellNode *() {
        ItemCellNode *node = [ItemCellNode new];
        
        [node setupWithItem:self.itemsArray[indexPath.item]];
        
        return node;
    };
    
    return ASCellNodeBlock;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath {
    return ASSizeRangeMake(CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], 0.0f), CGSizeMake([self.collectionViewLayout itemWidthInSectionAtIndex:indexPath.section], CGFLOAT_MAX));
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ItemDetails" bundle:nil];
    ItemDetailsViewController *vc = storyboard.instantiateInitialViewController;
    vc.item = self.itemsArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(CHTCollectionViewWaterfallLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionNode.view calculatedSizeForNodeAtIndexPath:indexPath];
}

- (void)reloadWishlist
{
    if(!self.itemsArray)
    {
        [SVProgressHUD show];
    }
    [[PFUser currentUser][@"wishlistPointer"]fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if (error)
        {
            [SVProgressHUD dismiss];
            UIAlertController *alert = [alertViewController errorLoadingWishlist];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        NSArray *objectIdArray = object[@"itemIdArray"];
        PFQuery *query = [PFQuery queryWithClassName:@"Item"];
        [query whereKey:@"objectId" containedIn:objectIdArray];
        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            if (error)
            {
                UIAlertController *alert = [alertViewController errorLoadingWishlist];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            if (!objects || [objects count] == 0) {
                UIAlertController *alert = [alertViewController emptyWishlist];
                [self presentViewController:alert animated:YES completion:nil];
            }
            self.itemsArray = objects;
            [self.collectionNode reloadData];
        }];
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
@end
