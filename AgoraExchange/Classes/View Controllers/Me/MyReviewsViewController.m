//
//  MyReviewsViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MyReviewsViewController.h"
#import "ReviewTableViewCell.h"
#import <Parse/Parse.h>
#import "SVProgressHUD.h"
#import "alertViewController.h"

@interface MyReviewsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray <PFObject *> *reviewArray;

@end

static NSString *cellID = @"cellID";

@implementation MyReviewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    [self loadReviews];
    [self setupTableView];
    // Do any additional setup after loading the view.
}

- (void)loadReviews
{
    if (self.reviewArray.count == 0)
    {
        [SVProgressHUD show];
    }
    PFQuery *query = [PFQuery queryWithClassName:@"Review"];
    [query whereKey:@"userId" equalTo:[PFUser currentUser].objectId];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (error)
        {
            UIAlertController *alert = [alertViewController errorLoadingReviews];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        else if (objects.count == 0)
        {
            UIAlertController *alert = [alertViewController userNoReviews];
            [self presentViewController:alert animated:YES completion:nil];
        }
        self.reviewArray = [[NSMutableArray alloc] initWithArray:objects];
        [self.tableView reloadData];
    }];
}

#pragma mark - table view setup

- (void)setupTableView
{
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"ReviewTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshControl:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.tableView.estimatedRowHeight = 150;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)refreshControl:(UIRefreshControl *)refreshControl {
    [self loadReviews];
    [refreshControl endRefreshing];
}

#pragma mark - table view delegate & datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.reviewArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.ratingText.text = [[self.reviewArray objectAtIndex:indexPath.row] objectForKey:@"description"];
    return cell;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
