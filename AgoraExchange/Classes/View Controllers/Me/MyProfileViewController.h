//
//  MyProfileViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/11/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyProfileDataModel.h"
#import <ParseUI/ParseUI.h>
#import "ImageCropView.h"

@protocol DismissViewControllerDelegate <NSObject>

- (void)dismissVC;

@end

@interface MyProfileViewController : UIViewController <ImageCropViewControllerDelegate>
@property (nonatomic, weak) id<DismissViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *profilePicture;
- (IBAction)takePhoto:(id)sender;
- (IBAction)chooseExisting:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *locationField;
@property (strong, nonatomic) IBOutlet UITextView *bioTextView;
- (IBAction)saveChanges:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *saveChangesButton;

@property (strong, nonatomic) MyProfileDataModel *model;
- (void)reloadData;
- (IBAction)editName:(id)sender;
- (IBAction)editLocation:(id)sender;
- (IBAction)editBio:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

//properties needed for textFieldDidChange methods
@property (strong, nonatomic) NSString * lastName;
@property (strong, nonatomic) NSString * lastLocation;
@end
