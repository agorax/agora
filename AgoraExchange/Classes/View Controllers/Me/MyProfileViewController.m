//
//  MyProfileViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/11/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MyProfileViewController.h"
#import "decorateTextFields.h"
#import "Constants.h"
#import "SVProgressHUD.h"
#import "alertViewController.h"
#import "camera.h"
#import <Parse/Parse.h>
#import "AgoraColors.h"

@interface MyProfileViewController () <UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate>
@end

BOOL profilePictureLoaded;

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadData];
    [self setupTextFields];
    [self setupButtons];
    [self addGestureRecognizer];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTextFields
{
    [decorateTextFields decorateLoginTextField:self.nameField];
    [decorateTextFields decorateLoginTextField:self.locationField];
    [decorateTextFields decorateBioTextField:self.bioTextView];
    self.nameField.delegate = self;
    self.locationField.delegate = self;
    self.bioTextView.delegate = self;
    
    [self.nameField addTarget:self
                  action:@selector(nameFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    [self.locationField addTarget:self
                      action:@selector(locationFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
    
    [self registerForKeyboardNotifications];
}

- (void)setupButtons
{
    self.saveChangesButton.layer.borderWidth = 1;
    self.saveChangesButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)reloadData
{
    profilePictureLoaded = NO;
    self.nameField.text = self.model.name;
    self.locationField.text = self.model.location;
//    self.profilePicture.file = self.model.pictureFile;
//    [self.profilePicture loadInBackground:^(UIImage * _Nullable image, NSError * _Nullable error) {
//        if (!error) {
//            self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.height/2.0;
//            profilePictureLoaded = YES;
//        }
//    }];
    
    if ([self.model.bio length] == 0)
    {
        self.bioTextView.text = @"Enter a Bio!";
    }
    else
    {
        self.bioTextView.text = self.model.bio;
    }
}

#pragma mark - on the fly text field editting

- (void)nameFieldDidChange: (UITextField *) name
{
    if ([name.text length] > 15)
    {
        name.text = self.lastName;
        return;
    }
    self.lastName = name.text;
}

- (void)locationFieldDidChange: (UITextField *) location
{
    if ([location.text length] > 20)
    {
        location.text = self.lastLocation;
        return;
    }
    self.lastLocation = location.text;
}

#pragma mark - gesture stuff

- (void)addGestureRecognizer
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.contentView addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.nameField resignFirstResponder];
    [self.locationField resignFirstResponder];
    [self.bioTextView resignFirstResponder];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (self.nameField.isFirstResponder || self.locationField.isFirstResponder || self.bioTextView.isFirstResponder)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark - keyboard stuff

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height, 0, self.tabBarController.tabBar.frame.size.height, 0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.nameField.isFirstResponder)
    {
        [self.nameField resignFirstResponder];
    }
    else if(self.locationField.isFirstResponder)
    {
        [self.locationField resignFirstResponder];
    }
    else
    {
        [self.bioTextView resignFirstResponder];
    }
    return NO;
}

- (IBAction)editName:(id)sender
{
    ((UIButton *)sender).superview.backgroundColor = [AgoraColors agoraGreenColor];
    [UIView animateWithDuration:1.5 animations:^{
        ((UIButton *)sender).superview.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }];
    self.nameField.enabled = YES;
    self.locationField.enabled = NO;
    self.bioTextView.userInteractionEnabled = NO;
    [self.nameField becomeFirstResponder];
}

- (IBAction)editLocation:(id)sender
{
    ((UIButton *)sender).superview.backgroundColor = [AgoraColors agoraGreenColor];
    [UIView animateWithDuration:1.5 animations:^{
        ((UIButton *)sender).superview.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }];
    self.locationField.enabled = YES;
    self.nameField.enabled = NO;
    self.bioTextView.userInteractionEnabled = NO;
    [self.locationField becomeFirstResponder];
}

- (IBAction)editBio:(id)sender
{
    ((UIButton *)sender).superview.backgroundColor = [AgoraColors agoraGreenColor];
    [UIView animateWithDuration:1.5 animations:^{
        ((UIButton *)sender).superview.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }];
    self.bioTextView.userInteractionEnabled = YES;
    self.locationField.enabled = NO;
    self.nameField.enabled = NO;
    [self.bioTextView becomeFirstResponder];
}

#pragma mark - camera stuff

- (IBAction)chooseExisting:(id)sender
{
    [self presentViewController:[camera chooseExisting:self] animated:YES completion:NULL];
}

- (IBAction)takePhoto:(id)sender
{
    [self presentViewController:[camera takePhoto:self] animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:info[UIImagePickerControllerOriginalImage]];
    controller.delegate = self;
    controller.blurredBackground = NO;
    controller.hidesBottomBarWhenPushed = YES;
    [[self navigationController] pushViewController:controller animated:YES];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)ImageCropViewControllerSuccess:(UIViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage {
    self.profilePicture.image = croppedImage;
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)ImageCropViewControllerDidCancel:(UIViewController *)controller {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)saveChanges:(id)sender
{
    if ([self.bioTextView.text length] > 150)
    {
        UIAlertController *alert = [alertViewController bioTooLong];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    NSData *imageData = UIImagePNGRepresentation(self.profilePicture.image);
    if (!imageData)
    {
        UIAlertController *alert = [alertViewController mustHaveProfilePicture];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    PFFile *imageFile = [PFFile fileWithName:@"profilePicture.png" data:imageData];
    PFUser * currentUser = [PFUser currentUser];
    [currentUser setObject:self.nameField.text forKey:@"name"];
    [currentUser setObject:[self.nameField.text lowercaseString] forKey:@"lowercaseName"];
    [currentUser setObject:self.locationField.text forKey:@"location"];
    if (profilePictureLoaded) {
        [currentUser setObject:imageFile forKey:@"picture"];
    }
    if (![self.bioTextView.text isEqualToString:@"Enter a Bio!"])
    {
        [currentUser setObject:self.bioTextView.text forKey:@"bio"];
    }
    [SVProgressHUD show];
    [self.navigationItem setHidesBackButton:YES];
    [self.view setUserInteractionEnabled:NO];
    [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
         [self.navigationItem setHidesBackButton:NO];
         [self.view setUserInteractionEnabled:YES];
         [SVProgressHUD dismiss];
         if(!error)
         {
             [self.delegate dismissVC];
         }
         else
         {
             UIAlertController *alert = [alertViewController errorUpdatingProfile];
             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}

@end
