//
//  ChangePasswordViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/11/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissViewControllerDelegate <NSObject>

- (void)dismissVC;

@end

@interface ChangePasswordViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *oldPasswordField;
@property (strong, nonatomic) IBOutlet UITextField *proposedPasswordField;
@property (nonatomic, weak) id<DismissViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *changePasswordButton;

- (IBAction)authenticate:(id)sender;

@end
