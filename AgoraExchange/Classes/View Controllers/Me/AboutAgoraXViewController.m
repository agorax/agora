//
//  AboutAgoraXViewController.m
//  AgoraExchange
//
//  Created by Justin Wang on 8/22/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "AboutAgoraXViewController.h"

@interface AboutAgoraXViewController ()

@end

@implementation AboutAgoraXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    // Do any additional setup after loading the view.
}

-(void)viewDidLayoutSubviews {
    [self.textView setContentOffset:CGPointZero animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
