//
//  AboutAgoraXViewController.h
//  AgoraExchange
//
//  Created by Justin Wang on 8/22/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutAgoraXViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *textView;

@end
