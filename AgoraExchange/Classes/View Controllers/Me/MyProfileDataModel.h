//
//  MyProfileDataModel.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/15/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ParseUI/ParseUI.h>

@interface MyProfileDataModel : NSObject

@property (strong, nonatomic) PFFile *pictureFile;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *bio;
@property (strong, nonatomic) NSString * location;

@end
