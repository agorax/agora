//
//  ViewReviewsViewController.m
//  AgoraX
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ViewReviewsViewController.h"
#import "AddReviewViewController.h"
#import "ReviewTableViewCell.h"
#import "SVProgressHUD.h"
#import "alertViewController.h"
#import "Review+CoreDataClass.h"
#import "SessionManager.h"

@interface ViewReviewsViewController () <UITableViewDelegate, UITableViewDataSource, AddedReviewControllerDelegate>

@property (strong, nonatomic) NSMutableArray <Review *> *reviewArray;

@end

static NSString *cellID = @"cellID";

@implementation ViewReviewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    [self setupTableView];
    [self loadReviews];
}

- (void)loadReviews
{
    [SVProgressHUD show];
    [self.addReviewButton setUserInteractionEnabled:NO];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Review"];
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"reviewedUserId == %d", self.user.userId]];
    [request setPredicate:searchPredicate];
    NSAsynchronousFetchRequest *asyncRequest = [[NSAsynchronousFetchRequest alloc] initWithFetchRequest:request completionBlock:^(NSAsynchronousFetchResult *result) {
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!result) {
                UIAlertController *alert = [alertViewController errorLoadingReviews];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            else if ([[result finalResult] count] == 0)
            {
                UIAlertController *alert = [alertViewController noReviews];
                [self presentViewController:alert animated:YES completion:nil];
            }
            self.reviewArray = [[NSMutableArray alloc] initWithArray:(NSArray <Review *>*)[result finalResult]];
            [self.addReviewButton setUserInteractionEnabled:YES];
            [self.tableView reloadData];
        });
    }];
    [[self privateContext] executeRequest:asyncRequest error:nil];
}

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[ViewReviewsViewController managedObjectContext]];
    return concurrentContext;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - table view setup

- (void)setupTableView
{
    self.tableView.estimatedRowHeight = 150;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"ReviewTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier: cellID];
}

#pragma mark - table view delegate & datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.reviewArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.ratingText.text = [self.reviewArray objectAtIndex:indexPath.row].reviewDescription;
    return cell;
}

#pragma mark - Navigation helper methods

- (void)ableToAddReview
{
    User *currentUser = [SessionManager getCurrentUser];
    if (currentUser.userId == self.user.userId)
    {
        UIAlertController *alert = [alertViewController reviewOwnItem];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    for (int i = 0; i < [self.reviewArray count]; i++)
    {
        if (self.reviewArray[i].reviewerUserId == currentUser.userId)
        {
            UIAlertController *alert = [alertViewController alreadyReviewed];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    [self performSegueWithIdentifier:@"Add Review" sender:self];
}

- (void) reviewAdded
{
    [self loadReviews];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Add Review"])
    {
        [self ableToAddReview];
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Review"])
    {
        ((AddReviewViewController *) segue.destinationViewController).user = self.user;
        ((AddReviewViewController *) segue.destinationViewController).delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

@end
