//
//  AddReviewViewController.m
//  AgoraX
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "AddReviewViewController.h"
#import "SVProgressHUD.h"
#import "alertViewController.h"
#import "AgoraColors.h"
#import "ReviewPresenter.h"
#import "SessionManager.h"

@interface AddReviewViewController () <UITextViewDelegate>

@property (strong, nonatomic)ReviewPresenter *reviewPresenter;

@end

@implementation AddReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Nav Bar Icon"]];
    [self setButtons];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    self.reviewTextView.delegate = self;
    self.reviewPresenter = [[ReviewPresenter alloc] init];
    [self.slider setContinuous:NO];
    // Do any additional setup after loading the view.
}

#pragma mark - slider stuff

- (IBAction)sliderValueChanged:(id)sender
{
    [self roundSlider];
}

- (void)roundSlider
{
    int sliderValue;
    sliderValue = lroundf(self.slider.value);
    [self.slider setValue:sliderValue animated:YES];
    [self setupRating:sliderValue];
}

- (void)setupRating: (int)roundedRating
{
    if (roundedRating == 1)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        return;
    }
    if (roundedRating == 2)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        return;
    }
    if (roundedRating == 3)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        return;
    }
    if (roundedRating == 4)
    {
        self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
        self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon Outline"];
        return;
    }
    self.firstStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.secondStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.thirdStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.fourthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
    self.fifthStarImageView.image = [UIImage imageNamed:@"Favorite Icon White"];
}

#pragma mark - view stuff

- (void)setButtons
{
    self.submitButton.layer.borderWidth = 1;
    self.submitButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [SVProgressHUD dismiss];
}

#pragma mark - textview delegate

-(void)textViewDidChange:(UITextView *)textView
{
    int charsLeft = 150 - [self.reviewTextView.text length];
    if (charsLeft < 0)
    {
        self.charactersLeftLabel.textColor = [UIColor blackColor];
        self.charactersLeftLabel.text = @"Over character limit";
    }
    else
    {
        self.charactersLeftLabel.textColor = [AgoraColors agoraGreenColor];
        self.charactersLeftLabel.text = [NSString stringWithFormat:@"%d characters remaining", charsLeft];
    }
}

#pragma mark - submit logic

- (IBAction)submit:(id)sender
{
    if ([self.reviewTextView.text length] > 150)
    {
        UIAlertController *alert = [alertViewController reviewTooLong];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if (self.alreadyReviewed) {
        UIAlertController *alert = [alertViewController alreadyReviewed];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    //make post request here
    [self disableUI];
    AddReviewViewController* __weak weakSelf = self;
    [self.reviewPresenter addReview:[self constructReview] completionBlock:^(AgoraError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf enableUI];
            if (error) {
                UIAlertController *alert = [alertViewController dismissableAlertWithError:error];
                [weakSelf displayAlert:alert];
            } else {
                [[AddReviewViewController managedObjectContext] save:nil];
                [weakSelf.delegate reviewAdded];
            }
        });
    }];
}

-(void)displayAlert:(UIAlertController *)alert {
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - keyboard stuff


- (void)keyboardWasShown:(NSNotification *)notification
{
    if (0 != self.view.frame.origin.y)
    {
        return;
    }
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    int movement = 0;
    movement = [UIScreen mainScreen].bounds.size.height - self.submitButton.frame.size.height - self.submitButton.frame.origin.y - keyboardSize.height - 20;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan && (self.reviewTextView.isFirstResponder))
    {
        [self.reviewTextView resignFirstResponder];
        [self resetViewOffset];
    }
}

- (void)resetViewOffset
{
    int movement = self.view.frame.origin.y;
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement);
    }];
}

- (Review *)constructReview {
    Review *review = [NSEntityDescription insertNewObjectForEntityForName:@"Review" inManagedObjectContext:[AddReviewViewController managedObjectContext]];
    review.reviewDescription = self.reviewTextView.text;
    review.reviewedUserId = self.user.userId;
    review.reviewerUserId = [SessionManager getCurrentUser].userId;
    review.rating = self.slider.value;
    
    return review;
}

-(void)enableUI {
    [self.view setUserInteractionEnabled:YES];
    [SVProgressHUD dismiss];
}

-(void)disableUI {
    [self.reviewTextView resignFirstResponder];
    [self resetViewOffset];
    [self.view setUserInteractionEnabled:NO];
    [SVProgressHUD show];
}

#pragma mark - managed object context stuff

- (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[AddReviewViewController managedObjectContext]];
    return concurrentContext;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
