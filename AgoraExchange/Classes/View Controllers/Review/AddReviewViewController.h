//
//  AddReviewViewController.h
//  AgoraX
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+CoreDataClass.h"

@protocol AddedReviewControllerDelegate <NSObject>

- (void)reviewAdded;

@end

@interface AddReviewViewController : UIViewController

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet UITextView *reviewTextView;
- (IBAction)submit:(id)sender;
@property (nonatomic, weak) id<AddedReviewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UILabel *charactersLeftLabel;
- (IBAction)sliderValueChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *firstStarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *secondStarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *thirdStarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *fourthStarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *fifthStarImageView;

@property (nonatomic) BOOL alreadyReviewed;

@end
