//
//  ViewReviewsViewController.h
//  AgoraX
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+CoreDataClass.h"

@interface ViewReviewsViewController : UIViewController

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *addReviewButton;

@end
