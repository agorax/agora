//
//  AgoraImageManager.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/22/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import "Item+CoreDataClass.h"
#import "User+CoreDataClass.h"
#import "Message+CoreDataClass.h"

@interface AgoraImageManager : NSObject

+ (void)uploadToS3:(UIImage *)image onItem:(Item *)item pictureNumber:(int)pictureNumber;
+ (NSString *)urlForItem:(Item *)item atIndex:(int)index;
+ (void)uploadToS3:(UIImage *)image onUser:(User *)user;
+ (void)uploadToS3:(UIImage *)image onMessage:(Message *)message andCompletionBlock:(void(^)()) completionBlock;
+ (NSString *)urlForUser:(User *)user;
+ (NSString *)urlForMessage:(Message *)message;

@end
