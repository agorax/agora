//
//  pickerData.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/2/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "pickerData.h"
#import "Constants.h"

@implementation pickerData

+ (NSArray *)getPickerElementsForScreen: (NSString *)pickerScreen
{
    if ([pickerScreen isEqualToString:@"Register"])
    {
        return [[NSArray alloc] initWithObjects:@"Case Western", nil];
    }
    else if ([pickerScreen isEqualToString:@"Sell Item"])
    {
        return [[NSArray alloc] initWithObjects: @"Appliances", @"Electronics", @"Events", @"Free", @"Furniture", @"Greek", @"Housing", @"Men's Clothing", @"Miscellaneous", @"Services", @"Sporting Goods", @"Textbooks", @"Women's Clothing", nil];
    }
    else if([pickerScreen isEqualToString:@"Choose Category"])
    {
        return [[NSArray alloc] initWithObjects: @"Textbooks", @"All Items", @"Appliances", @"Electronics", @"Events", @"Free", @"Furniture", @"Greek", @"Housing", @"Men's Clothing", @"Miscellaneous", @"Services", @"Sporting Goods", @"Women's Clothing", nil];
    }
    else if ([pickerScreen isEqualToString:@"Report"])
    {
        return [[NSArray alloc] initWithObjects:@"Profanity", @"False Advertisement", @"Spam", @"Illegal or Harmful", @"Controlled Substance", @"Offensive", nil];
    }
    else
    {
        return [[NSArray alloc] init];
    }
}

+ (NSDictionary *) collegeNameToEmail
{
    return [NSDictionary dictionaryWithObjectsAndKeys:@"@mail.bw.edu", @"Baldwin Wallace", @"@case.edu", @"Case Western", @"@ohio.edu", @"Ohio University", nil];
}

@end
