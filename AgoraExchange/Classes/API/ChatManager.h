//
//  ChatManager.h
//  AgoraExchange
//
//  Created by Travis Chambers on 7/21/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message+CoreDataClass.h"

@import SocketIO;

@interface ChatManager : NSObject
@property (strong, nonatomic) SocketIOClient *sioClient;

+ (instancetype)sharedInstance;
- (id) init;
- (void)establishConnection;
- (void)closeConnection;
- (void) createConversation: (int)sellerId buyer: (int) buyerId item: (int)itemId message: (NSString *) message senderDisplayName: (NSString *)senderDisplayName itemName: (NSString *) itemName completionBlock:(void(^)(NSNumber * conversationId)) completionBlock;
- (void) sendMessageToRoom: (NSString *)room event: (NSString *) event message: (NSString *)message from: (int) senderId convo:(int) convoId senderDisplayName: (NSString *) senderDisplayName recipientId: (int) recipientId numberOfPictures: (int) numberOfPictures completionBlock:(void(^)(NSNumber * messageId)) completionBlock;
- (void) sendMediaMessageToRoom: (NSString *)room event: (NSString *) event messageId: (NSNumber *) messageId message: (NSString *)message from: (int) senderId convo:(int) convoId senderDisplayName: (NSString *) senderDisplayName recipientId: (int) recipientId numberOfPictures: (int) numberOfPictures completionBlock:(void(^)(NSNumber * messageId)) completionBlock;
- (void) getMessages: (void (^)(Message *))completionBlock;
- (void) userIsTypingInRoom: (NSString *)room;
- (void) userStoppedTypingInRoom: (NSString *)room;
- (void) listenForTyping: (void(^)(bool))completionBlock;
- (void) subscribeToRoom: (NSString *)room;
- (void) reconnectOnDisconnect: (void (^)())completionBlock;
@end
