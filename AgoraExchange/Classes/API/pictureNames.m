//
//  pictureNames.m
//  AgoraExchange
//
//  Created by Justin Wang on 1/5/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "pictureNames.h"

@implementation pictureNames

+ (NSArray *) pictureNamesForSchool:(NSString *) school
{
    if ([school isEqualToString:@"Baldwin Wallace"])
    {
        return [NSArray arrayWithObjects:@"BW 1", @"BW 2", @"BW 3", @"BW 4", nil];
    }
    if ([school isEqualToString:@"Ohio University"])
    {
        return [NSArray arrayWithObjects:@"OU 1", @"OU 2", @"OU 3", @"OU 4", nil];
    }
    //Case Western image names:
    else
    {
        return [NSArray arrayWithObjects:@"Case Western Birds Eye", @"Case Western Football Stadium", @"Case Western Reserve Image KSL", @"Doc Oc Case Image", nil];
    }
}

@end
