//
//  SessionManager.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/13/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "SessionManager.h"
#import "NetworkManager.h"
#import <SSKeychain/SSKeychain.h>
#import "AppDelegate.h"
#import "ChatManager.h"

@implementation SessionManager
static User *currentUser;
static NSTimer *syncTimer;

+(void)logout {
    [SessionManager setCurrentUserId:0];
    [SessionManager removeUserIdFromKeychain];
    [SessionManager removeAgoraTokenFromKeychain];
    [SessionManager invalidateSyncTimer];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil];
}

//must call from main thread
+(User *)getCurrentUser
{
    if (!currentUser) {
        currentUser = [User getUserWithId:[SessionManager fetchCurrentUserIdFromKeychain] inManagedObjectContext:[SessionManager managedObjectContext]];
    }
    return currentUser;
}

+(NSNumber *)totalUnreadCount {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
    NSArray *conversations = [Conversation getConversationsWithUserId:currentUser.userId inManagedObjectContext:[SessionManager managedObjectContext] andFetchRequest:request];
    int total = 0;
    for (Conversation *conversation in conversations) {
        NSLog(@"%hd",conversation.conversationId);
        ([conversation respondsToSelector:NSSelectorFromString(@"unReadMessages")]) ? total+=conversation.unReadMessages : 0;
    }
    return [NSNumber numberWithInt:total];
}

+(void)setCurrentUserId:(int)userId
{
    [SessionManager storeUserIdInKeychain:userId];
}

+ (void)removeUserIdFromKeychain {
    [SSKeychain deletePasswordForService:@"AgoraUser" account:@"com.AgoraExchange.keychain"];
}

+(void)storeUserIdInKeychain:(int)userId {
    [SSKeychain setPassword:[NSString stringWithFormat:@"%d", userId] forService:@"AgoraUser" account:@"com.AgoraExchange.keychain"];
}

+(int)fetchCurrentUserIdFromKeychain {
    return [[SSKeychain passwordForService:@"AgoraUser" account:@"com.AgoraExchange.keychain"] intValue];
}

+ (void)storeAgoraTokenInKeychain:(NSString *)token
{
    [SSKeychain setPassword:token forService:@"AgoraToken" account:@"com.AgoraExchange.keychain"];
}

+(NSString *)fetchAgoraToken
{
    return [SSKeychain passwordForService:@"AgoraToken" account:@"com.AgoraExchange.keychain"];
}

+ (void)removeAgoraTokenFromKeychain {
    [SSKeychain deletePasswordForService:@"AgoraToken" account:@"com.AgoraExchange.keychain"];
}

+(void)storeAPNSToken: (NSString *)apnsToken {
    [SSKeychain setPassword:apnsToken forService:@"APNSToken" account:@"com.AgoraExchange.keychain"];
}

+(NSString *)fetchAPNSToken {
    return [SSKeychain passwordForService:@"APNSToken" account:@"com.AgoraExchange.keychain"];
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

+ (void)configureSyncTimer {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!syncTimer) {
            syncTimer = [NSTimer timerWithTimeInterval:60.0 target:self selector:@selector(sync) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:syncTimer forMode:NSDefaultRunLoopMode];
        }
    });
}

+(void)sync {
    if ([SessionManager fetchAgoraToken] && [SessionManager getCurrentUser]) {
        [NetworkManager reloadMarketplaceWithToken:[SessionManager fetchAgoraToken] school:[SessionManager getCurrentUser].school andCompletionBlock:^(AgoraError *error) {
            
        }];
    }
}

+ (void)invalidateSyncTimer {
    dispatch_async(dispatch_get_main_queue(), ^{
        [syncTimer invalidate];
        syncTimer = nil;
    });
}

@end
