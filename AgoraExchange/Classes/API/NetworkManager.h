//
//  NetworkManager.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/13/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User+CoreDataClass.h"
#import "Item+CoreDataClass.h"
#import "Review+CoreDataClass.h"
#import "Conversation+CoreDataClass.h"
#import "AgoraError.h"

@interface NetworkManager : NSObject <NSURLSessionDelegate>

+(void)loginWithEmail:(NSString *)email
             password:(NSString *)password
             apnsToken:(NSString *)apnsToken
   andCompletionBlock:(void(^)(int userId, NSString *token, AgoraError *error))completionBlock;

+(void)registerWithEmail:(NSString *)email
                password:(NSString *)password
                  school:(NSString *)school
      andCompletionBlock:(void(^)(AgoraError *error))completionBlock;

+(void)reloadMarketplaceWithToken:(NSString *)token school:(NSString *)school andCompletionBlock:(void(^)(AgoraError *error))completionBlock;
+(void)getConversationWithUserId:(int) userId andCompletionBlock:(void(^)(AgoraError *error)) completionBlock;
+(void)getMessagesWithConversationId:(int) conversationId andCompletionBlock:(void(^)(AgoraError *error)) completionBlock;

+(void)sellItem:(Item *)item withToken:(NSString *)token andCompletionBlock:(void(^)(int itemId, AgoraError *error))completionBlock;

+(void)addReview:(Review *)review withToken:(NSString *)token andCompletionBlock:(void(^)(AgoraError *error))completionBlock;

+(void)deleteItem:(NSNumber *)itemId withToken:(NSString *)token andCompletionBlock:(void(^)(AgoraError *error))completionBlock;

+(void)updateProfileWithDictionary:(NSMutableDictionary *)updatedAttributes andCompletionBlock:(void(^)(AgoraError *error))completionBlock;

+(void)updateItemWithDictionary:(NSMutableDictionary *)updatedAttributes andCompletionBlock:(void(^)(AgoraError *error))completionBlock;

+(void)deleteConversationWithId:(int) conversationId recipientId:(NSNumber *) recipientId senderDisplayName:(NSString *)senderDisplayName itemName:(NSString *)itemName andCompletionBlock:(void(^)(AgoraError *error)) completionBlock;

+(void)zeroBadgeForUserId:(int) userId;

+(void)zeroUnreadCountforConversation:(Conversation *) conversation;

@end
