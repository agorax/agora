//
//  pictureNames.h
//  AgoraExchange
//
//  Created by Justin Wang on 1/5/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface pictureNames : NSObject

+ (NSArray *) pictureNamesForSchool:(NSString *) school;

@end
