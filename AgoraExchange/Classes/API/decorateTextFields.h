//
//  decorateTextFields.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/2/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface decorateTextFields : NSObject

+ (UITextField *)decorateTextField:(UITextField *) field;
+ (UITextField *)decorateLoginTextField:(UITextField *) field;
+ (UITextField *)decoratePasswordTextField:(UITextField *) field;
+ (UITextField *)decorateLoginPasswordTextField:(UITextField *) field;
+ (UITextView *)decorateBioTextField: (UITextView *) textView;

@end
