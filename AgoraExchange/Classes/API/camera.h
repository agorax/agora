//
//  camera.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/2/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface camera : NSObject

+ (UIImagePickerController *)takePhoto: (UIViewController *) vc;
+ (UIImagePickerController *)chooseExisting: (UIViewController *) vc;

@end
