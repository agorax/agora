//
//  AgoraNetworkParser.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/27/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "AgoraNetworkParser.h"
#import "AppDelegate.h"
#import "User+CoreDataClass.h"
#import "Review+CoreDataClass.h"
#import "Item+CoreDataClass.h"
#import "Conversation+CoreDataClass.h"

@implementation AgoraNetworkParser

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

+ (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[AgoraNetworkParser managedObjectContext]];
    return concurrentContext;
}


+(void)parseNetworkJSONwithConversation:(NSDictionary *)json{
    NSManagedObjectContext *context = [AgoraNetworkParser managedObjectContext];
    [AgoraNetworkParser insertConversationsFromArray:[json objectForKey:@"conversations"] intoManagedObjectContext:context];
    [context save:nil];
    
}

+(void)parseNetworkJSONwithMessages:(NSDictionary *)json {
    NSManagedObjectContext *context = [AgoraNetworkParser managedObjectContext];
    [AgoraNetworkParser insertMessagesFromArray:[json objectForKey:@"messages"] intoManagedObjectContext:context];
    [context save:nil];
}

+(Message *)parseNetworkJSONwithSocketMessage: (NSArray *)payload {
    NSManagedObjectContext *context = [AgoraNetworkParser managedObjectContext];
    Message * message = [[Message alloc] initWithDictionary:payload[0] inManagedObjectContext:context];
    [context save:nil];
    return message;
}

+(void)parseNetworkJSONandInsertIntoCoreData:(NSDictionary *)json {
    NSManagedObjectContext *privateContext = [AgoraNetworkParser privateContext];
    [privateContext performBlock:^{
        [AgoraNetworkParser insertUsersFromArray:[json objectForKey:@"users"]intoManagedObjectContext:privateContext];
        [AgoraNetworkParser insertItemsFromArray:[json objectForKey:@"items"]intoManagedObjectContext:privateContext];
        [AgoraNetworkParser insertReviewsFromArray:[json objectForKey:@"reviews"]intoManagedObjectContext:privateContext];
        [AgoraNetworkParser insertConversationsFromArray:[json objectForKey:@"conversations"] intoManagedObjectContext:privateContext];
        [AgoraNetworkParser deleteItemsNotInArray:[json objectForKey:@"items"] intoManagedObjectContext:privateContext];
        [AgoraNetworkParser deleteUsersNotInArray:[json objectForKey:@"users"] intoManagedObjectContext:privateContext];
        [AgoraNetworkParser deleteConversationsNotInArray:[json objectForKey:@"conversations"] intoManagedObjectContext:privateContext];
        NSError *error = nil;
        if (![privateContext save:&error]) {
            NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
            abort();
        }
        [[AgoraNetworkParser managedObjectContext] performBlockAndWait:^{
            NSError *error = nil;
            if (![[AgoraNetworkParser managedObjectContext] save:&error]) {
                NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
                abort();
            }
        }];
    }];
}

+(void)insertMessagesFromArray: (NSArray *)messageArray intoManagedObjectContext:(NSManagedObjectContext *)context {
    if (!messageArray) {
        return;
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
    Message *message;
    for (NSDictionary *messageDict in messageArray) {
        message = [Message getMessageWithId:[[messageDict objectForKey:@"messageId"] integerValue] inManagedObjectContext:context andFetchRequest:request];
        (message) ? [message updateFromAttributes:messageDict] : [[Message alloc] initWithDictionary:messageDict inManagedObjectContext:context];
    }
}

+(void)insertConversationsFromArray: (NSArray *)conversationArray intoManagedObjectContext:(NSManagedObjectContext *)context {
    if (!conversationArray) {
        return;
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
    Conversation *conversation;
    for (NSDictionary *conversationDict in conversationArray) {
        conversation = [Conversation getConversationWithId:[[conversationDict objectForKey:@"conversationId"] integerValue] inManagedObjectContext:context andFetchRequest:request];
        (conversation) ? [conversation updateFromAttributes:conversationDict] : [[Conversation alloc] initWithDictionary:conversationDict inManagedObjectContext:context];
    }
}


+(void)insertUsersFromArray:(NSArray *)userArray intoManagedObjectContext:(NSManagedObjectContext *)context{
    if (!userArray) {
        return;
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    User *user;
    for (NSDictionary *userDict in userArray) {
        user = [User getUserWithId:[[userDict objectForKey:@"userId"] integerValue] inManagedObjectContext:context andFetchRequest:request];
        (user) ? [user updateFromAttributes:userDict] : [[User alloc] initWithDictionary:userDict inManagedObjectContext:context];
    }
}

+(void)insertItemsFromArray:(NSArray *)itemArray intoManagedObjectContext:(NSManagedObjectContext *)context{
    if (!itemArray) {
        return;
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    Item *item;
    for (NSDictionary *itemDict in itemArray) {
        item = [Item getItemWithId:[[itemDict objectForKey:@"itemId"] integerValue]  inManagedObjectContext:context andFetchRequest:request];
        (item) ? [item updateFromAttributes:itemDict] : [[Item alloc] initWithDictionary:itemDict inManagedObjectContext:context];
    }
}

+(void)insertReviewsFromArray:(NSArray *)reviewArray intoManagedObjectContext:(NSManagedObjectContext *)context{
    if (!reviewArray) {
        return;
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Review"];
    Review *review;
    for (NSDictionary *reviewDict in reviewArray) {
        review = [Review getReviewWithReviewerId:[[reviewDict objectForKey:@"reviewerUserId"] integerValue] reviewedId: [[reviewDict objectForKey:@"reviewedUserId"] integerValue] inManagedObjectContext:[AgoraNetworkParser managedObjectContext] andFetchRequest:request];
        (review) ? [review updateFromAttributes:reviewDict] : [[Review alloc]initWithDictionary:reviewDict inManagedObjectContext:context];
    }
}

+(void)deleteConversationsNotInArray:(NSArray *)conversationArray intoManagedObjectContext:(NSManagedObjectContext *)context{
    if (!conversationArray) {
        return;
    }
    NSMutableSet *conversationSet = [[NSMutableSet alloc] init];
    for (NSDictionary *conversationDict in conversationArray) {
        [conversationSet addObject:[conversationDict objectForKey:@"conversationId"]];
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
    NSArray <Conversation *>*results = [context executeFetchRequest:request error:nil];
    for (Conversation *conversation in results) {
        if (![conversationSet containsObject:@(conversation.conversationId)]) {
            [context deleteObject:conversation];
        }
    }
}

+(void)deleteUsersNotInArray:(NSArray *)userArray intoManagedObjectContext:(NSManagedObjectContext *)context{
    if (!userArray) {
        return;
    }
    NSMutableSet *userSet = [[NSMutableSet alloc] init];
    for (NSDictionary *userDict in userArray) {
        [userSet addObject:[userDict objectForKey:@"userId"]];
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSArray <User *>*results = [context executeFetchRequest:request error:nil];
    for (User *user in results) {
        if (![userSet containsObject:@(user.userId)]) {
            [context deleteObject:user];
        }
    }
}

+(void)deleteItemsNotInArray:(NSArray *)itemArray intoManagedObjectContext:(NSManagedObjectContext *)context{
    if (!itemArray) {
        return;
    }
    NSMutableSet *itemSet = [[NSMutableSet alloc] init];
    for (NSDictionary *itemDict in itemArray) {
        [itemSet addObject:[itemDict objectForKey:@"itemId"]];
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    NSArray <Item *>*results = [context executeFetchRequest:request error:nil];
    for (Item *item in results) {
        if (![itemSet containsObject:@(item.itemId)]) {
            [context deleteObject:item];
        }
    }
}
@end
