//
//  AgoraColors.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/6/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AgoraColors : NSObject

+ (UIColor *) agoraGreenColor;
+ (UIColor *) agoraBlueColor;
+ (UIColor *) agoraGrayColor;
+ (UIColor *) agoraGradientColor;

@end
