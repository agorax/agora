//
//  AgoraImageManager.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/22/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "AgoraImageManager.h"


@implementation AgoraImageManager

#pragma mark S3 stuff
+ (void)uploadToS3:(UIImage *)image url:(NSURL *)url folder:(NSString *)folder fileName:(NSString *)fileName andCompletionBlock:(void(^)()) completionBlock {
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = @"agora-exchange-bucket";
    // I want this image to be public to anyone to view it so I'm setting it to Public Read
    uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    // set the image's name that will be used on the s3 server. I am also creating a folder to place the image in
    uploadRequest.key = [NSString stringWithFormat:@"%@/%@",folder, fileName];
    uploadRequest.contentType = @"image/png";
    uploadRequest.body = url;
    
    // iam/aws authentication bullshit stuff is in the app delegate
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id _Nullable(AWSTask * _Nonnull task) {
        if(completionBlock) { completionBlock(); }
        return nil;
    }];
}

+ (void)uploadToS3:(UIImage *)image onItem:(Item *)item pictureNumber:(int)pictureNumber{
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%d-%d", item.itemId, pictureNumber]];
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:path atomically:YES];
    
    // once the image is saved we can use the path to create a local fileurl
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    [AgoraImageManager uploadToS3:image url:url folder:@"Item" fileName:[NSString stringWithFormat:@"%d-%d", item.itemId,pictureNumber] andCompletionBlock:nil];
}

+ (void)uploadToS3:(UIImage *)image onUser:(User *)user {
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%d", user.userId]];
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:path atomically:YES];
    
    // once the image is saved we can use the path to create a local fileurl
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    [AgoraImageManager uploadToS3:image url:url folder:@"User" fileName:[NSString stringWithFormat:@"%d", user.userId] andCompletionBlock:nil];
}

+ (void)uploadToS3:(UIImage *)image onMessage:(Message *)message andCompletionBlock:(void(^)()) completionBlock {
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"mediaMessage-%d", message.messageId]];
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:path atomically:YES];
    
    // once the image is saved we can use the path to create a local fileurl
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    [AgoraImageManager uploadToS3:image url:url folder:[NSString stringWithFormat:@"Chat/Conversation-%d",message.conversationId] fileName:[NSString stringWithFormat:@"mediaMessage-%d", message.messageId] andCompletionBlock:^(){
        completionBlock();
    }];
}


+ (NSString *)urlForItem:(Item *)item atIndex:(int)index {
    return [NSString stringWithFormat:@"https://s3.amazonaws.com/agora-exchange-bucket/Item/%d-%d", item.itemId, index];
}

+ (NSString *)urlForUser:(User *)user {
    return [NSString stringWithFormat:@"https://s3.amazonaws.com/agora-exchange-bucket/User/%d", user.userId];
}

+ (NSString *)urlForMessage:(Message *)message {
    return [NSString stringWithFormat:@"https://s3.amazonaws.com/agora-exchange-bucket/Chat/Conversation-%d/mediaMessage-%d", message.conversationId, message.messageId];
}

@end
