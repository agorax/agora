//
//  generatePrompt.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/6/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "generatePrompt.h"
#import "AgoraColors.h"

@implementation generatePrompt

+ (UIView *) resentConfirmationCode: (CGRect)superviewBounds
{
    UIView *addedView = [[UIView alloc]initWithFrame:superviewBounds];
    addedView.backgroundColor = [UIColor clearColor];
    addedView.userInteractionEnabled = YES;
    
    UIView *promptView = [[UIView alloc]initWithFrame:CGRectMake(20, addedView.frame.size.height/3.0, addedView.frame.size.width - 40, addedView.frame.size.height/3.0)];
    promptView.backgroundColor = [AgoraColors agoraGrayColor];
    promptView.layer.cornerRadius = 5;
    [addedView addSubview:promptView];
    
    UILabel *codeSent = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, promptView.frame.size.width,2.0*promptView.frame.size.height/5.0)];
    codeSent.textAlignment = NSTextAlignmentCenter;
    codeSent.center = CGPointMake(promptView.frame.size.width/2.0, promptView.frame.size.height/ 10.0);
    codeSent.text = @"A new code has been sent.";
    codeSent.textColor = [UIColor whiteColor];
    [promptView addSubview:codeSent];
    
    UIButton *dismiss = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 9.0*promptView.frame.size.width/10.0, 3.0*promptView.frame.size.height/10.0)];
    dismiss.center = CGPointMake(promptView.frame.size.width/2.0, 4.0*promptView.frame.size.height/5.0);
    dismiss.backgroundColor = [AgoraColors agoraGreenColor];
    dismiss.layer.cornerRadius = 5;
    [dismiss setTitle:@"OK" forState:UIControlStateNormal];
    dismiss.titleLabel.font = [UIFont systemFontOfSize: 30];
    [dismiss addTarget:self
                    action:@selector(dismissPrompt:)
          forControlEvents:UIControlEventTouchUpInside];
    [promptView addSubview:dismiss];

    return addedView;
}

- (IBAction)dismissPrompt:(id)sender
{
    NSLog(@"hi");
}

@end
