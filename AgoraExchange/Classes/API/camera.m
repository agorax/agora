//
//  camera.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/2/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "camera.h"
@implementation camera

+ (UIImagePickerController *)takePhoto: (UIViewController *)vc
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = vc;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    return picker;
}

+ (UIImagePickerController *)chooseExisting: (UIViewController *)vc
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = vc;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    return picker;
}

@end