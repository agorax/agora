//
//  SessionManager.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/13/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User+CoreDataClass.h"
#import "Conversation+CoreDataClass.h"

@interface SessionManager : NSObject

+(User *)getCurrentUser;
+(void)setCurrentUserId:(int)userId;
+(void)logout;

+(void)storeAgoraTokenInKeychain:(NSString *)token;
+(NSString *)fetchAgoraToken;
+ (NSManagedObjectContext *)managedObjectContext;
+(NSString *)fetchAPNSToken;
+(void)storeAPNSToken: (NSString *)apnsToken;
+(NSNumber *)totalUnreadCount;

+(void)configureSyncTimer;
+(void)invalidateSyncTimer;

@end
