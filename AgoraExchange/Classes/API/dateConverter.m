//
//  dateConverter.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "dateConverter.h"

@implementation dateConverter

+ (NSString *) dateToString: (NSDate *) originalDate
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MMMM d, yyyy"];
    return [formatter stringFromDate:originalDate];
}

+ (NSDate *)stringToDate:(NSString *)originalString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:[originalString substringToIndex:[originalString length] - 5]];
    return date;
}

+ (int)listingExpires: (NSDate *)listDate
{
    NSDate *twentyEightDaysFromList = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                    value:28
                                                                   toDate:listDate
                                                                  options:0];
    NSDateComponents *components;
    int days;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                 fromDate: [NSDate date] toDate: twentyEightDaysFromList options: 0];
    days = (int)[components day];
    
    return days;
}

@end
