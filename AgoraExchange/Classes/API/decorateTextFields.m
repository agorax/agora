//
//  decorateTextFields.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/2/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "decorateTextFields.h"

@implementation decorateTextFields

+ (UITextField *)decorateTextField:(UITextField *) field
{
    field.layer.borderColor = [[UIColor blackColor] CGColor];
    field.layer.borderWidth = 1.5;
    field.layer.cornerRadius = 5;
    field.clipsToBounds = YES;
    return field;
}

+ (UITextField *)decoratePasswordTextField:(UITextField *) field
{
    field.layer.borderColor = [[UIColor blackColor] CGColor];
    field.layer.borderWidth = 1.5;
    field.layer.cornerRadius = 5;
    field.clipsToBounds = YES;
    field.secureTextEntry = YES;
    return field;
}

//used for login storyboard text fields:
+ (UITextField *)decorateLoginTextField:(UITextField *) field
{
    field.backgroundColor = [UIColor clearColor];
    field.borderStyle = UITextBorderStyleNone;
    field.layer.cornerRadius = 5;
    field.clipsToBounds = YES;
    return field;
}

+ (UITextView *)decorateBioTextField: (UITextView *) textView
{
    textView.backgroundColor = [UIColor clearColor];
    textView.layer.cornerRadius = 5;
    textView.clipsToBounds = YES;
    return textView;
}

+ (UITextField *)decorateLoginPasswordTextField:(UITextField *) field
{
    field.backgroundColor = [UIColor clearColor];
    field.borderStyle = UITextBorderStyleNone;
    field.layer.cornerRadius = 5;
    field.clipsToBounds = YES;
    field.secureTextEntry = YES;
    return field;
}

@end
