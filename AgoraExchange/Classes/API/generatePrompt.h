//
//  generatePrompt.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/6/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface generatePrompt : NSObject

+ (UIView *) resentConfirmationCode: (CGRect)superviewBounds;

@end
