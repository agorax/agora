//
//  pickerData.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/2/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface pickerData : NSObject

+ (NSArray *)getPickerElementsForScreen: (NSString *)pickerScreen;
+ (NSDictionary *)collegeNameToEmail;

@end
