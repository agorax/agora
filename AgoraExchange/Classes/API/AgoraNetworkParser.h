//
//  AgoraNetworkParser.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/27/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message+CoreDataClass.h"

@interface AgoraNetworkParser : NSObject

+(void)parseNetworkJSONandInsertIntoCoreData:(NSDictionary *)json;
+(void)parseNetworkJSONwithConversation:(NSDictionary *)json;
+(void)parseNetworkJSONwithMessages:(NSDictionary *)json;
+(Message *)parseNetworkJSONwithSocketMessage: (NSArray *)payload;
@end
