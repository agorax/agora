//
//  NetworkManager.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/13/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "NetworkManager.h"
#import "SessionManager.h"
#import "AgoraError.h"
#import "AppDelegate.h"
#import "AgoraNetworkParser.h"

//NSString *const domain = @"http://bb41a4ec.ngrok.io"; /// Travis
//NSString *const domain = @"http://localhost:8000";
NSString *const domain = @"http://13.58.246.147"; /// PRODUCTION
//NSString *const domain = @"http://dfca8ad8.ngrok.io"; ///JUSTIN
//restricted routes
NSString *const loginPath = @"/login";
NSString *const registerPath = @"/register";
//unrestricted routes
NSString *const marketplaceReload = @"/api/marketplaceReload";
NSString *const sellItemPath = @"/api/sell/item";
NSString *const addReviewPath = @"/api/review";
NSString *const profileUpdatePath = @"/api/profileUpdate";
NSString *const itemUpdatePath = @"/api/itemUpdate";
NSString *const itemDeletePath = @"/api/itemDelete";
NSString *const chatPath = @"/api/chat";


@implementation NetworkManager

/*
 name: request.body,
 userId: salt,
 price: ,
 itemDescription: ,
 category: ,
 school: request.body.school
 */

+(void)sellItem:(Item *)item withToken:(NSString *)token andCompletionBlock:(void(^)(int itemId, AgoraError *error))completionBlock {
    NSURL *url = [NetworkManager constructURLWithPath:sellItemPath];
    NSDictionary *postDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", item.school, @"school", item.name, @"name", [NSNumber numberWithInt:item.userId], @"userId", [NSNumber numberWithInt:item.price], @"price", item.itemDescription, @"itemDescription", item.category, @"category", @(item.isbn), @"isbn", @(item.edition), @"edition", item.author, @"author", @(item.numberOfPictures), @"numberOfPictures", nil];
    NSMutableURLRequest *request = [NetworkManager createPostRequestWithURL:url andDictionary:postDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(-1, agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data

                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        completionBlock([[json objectForKey:@"itemId"] integerValue], agoraError);
    }];
    [task resume];
}

+(void)deleteItem:(NSNumber *)itemId withToken:(NSString *)token andCompletionBlock:(void(^)(AgoraError *error))completionBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@",itemDeletePath,itemId];
    NSURL *url = [NetworkManager constructURLWithPath:path];
    NSDictionary *postDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", itemId, @"itemId", nil];
    NSMutableURLRequest *request = [NetworkManager createPostRequestWithURL:url andDictionary:postDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data
                              
                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        completionBlock(agoraError);
    }];
    [task resume];
}

+(void)addReview:(Review *)review withToken:(NSString *)token andCompletionBlock:(void(^)(AgoraError *error))completionBlock {
    NSURL *url = [NetworkManager constructURLWithPath:addReviewPath];
    NSDictionary *postDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", review.reviewDescription, @"reviewDescription", @(review.reviewerUserId), @"reviewerUserId", @(review.reviewedUserId), @"reviewedUserId", @(review.rating), @"rating", nil];
    NSMutableURLRequest *request = [NetworkManager createPostRequestWithURL:url andDictionary:postDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data
                              
                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        completionBlock(agoraError);
    }];
    [task resume];
}

+(void)getMessagesForConversation:(int) conversationId andCompletionBlock:(void(^)(AgoraError *error)) completionBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@/messages",chatPath,[NSNumber numberWithInt:conversationId]];
    NSURL *url = [self constructURLWithPath:path];
    NSString *token = [SessionManager fetchAgoraToken];
    NSDictionary *getDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", nil];
    NSMutableURLRequest *request =  [NetworkManager createGetRequestWithUrl:url andToken:getDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data

                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        if (agoraError) {
            //bounce the user out... probs issue a notification here.
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AgoraNetworkParser parseNetworkJSONwithMessages:json];
            });
        }
        completionBlock(agoraError);


    }];
}

+(void)updateProfileWithDictionary:(NSMutableDictionary *)updatedAttributes andCompletionBlock:(void(^)(AgoraError *error))completionBlock {
    NSURL *url = [NetworkManager constructURLWithPath:profileUpdatePath];
    [updatedAttributes setObject:@([SessionManager getCurrentUser].userId) forKey:@"userId"];
    [updatedAttributes setObject:[SessionManager fetchAgoraToken] forKey:@"token"];
    NSMutableURLRequest *request = [NetworkManager createPostRequestWithURL:url andDictionary:updatedAttributes];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data

                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        completionBlock(agoraError);
    }];
    [task resume];
}

+(void)getConversationWithUserId:(int) userId andCompletionBlock:(void(^)(AgoraError *error)) completionBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@",chatPath,[NSNumber numberWithInt:userId]];
    NSString *token = [SessionManager fetchAgoraToken];
    NSDictionary *tokenDict = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", nil];
    NSURL *url = [self constructURLWithPath:path];
    NSMutableURLRequest *request =  [NetworkManager createGetRequestWithUrl:url andToken:tokenDict];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data

                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        if (agoraError) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AgoraNetworkParser parseNetworkJSONwithConversation:json];
                completionBlock(agoraError);
            });
        }
      }];
      [task resume];
}

+(void)deleteConversationWithId:(int) conversationId recipientId:(NSNumber *) recipientId senderDisplayName:(NSString *)senderDisplayName itemName:(NSString *)itemName andCompletionBlock:(void(^)(AgoraError *error)) completionBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@",chatPath,[NSNumber numberWithInt:conversationId]];
    NSString *token = [SessionManager fetchAgoraToken];
    NSDictionary *tokenDict = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", recipientId, @"recipientId", senderDisplayName, @"senderDisplayName", itemName, @"itemName", nil];
    NSURL *url = [self constructURLWithPath:path];
    NSMutableURLRequest *request =  [NetworkManager createDeleteRequestWithUrl:url andToken:tokenDict];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        completionBlock(agoraError);
        
    }];
    [task resume];
}


+(void)updateItemWithDictionary:(NSMutableDictionary *)updatedAttributes andCompletionBlock:(void(^)(AgoraError *error))completionBlock {
    NSURL *url = [NetworkManager constructURLWithPath:itemUpdatePath];
    [updatedAttributes setObject:[SessionManager fetchAgoraToken] forKey:@"token"];
    NSMutableURLRequest *request = [NetworkManager createPostRequestWithURL:url andDictionary:updatedAttributes];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data

                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        completionBlock(agoraError);
    }];
    [task resume];
}

+(void)getMessagesWithConversationId:(int) conversationId andCompletionBlock:(void(^)(AgoraError *error)) completionBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@/messages",chatPath,[NSNumber numberWithInt:conversationId]];
    NSString *token = [SessionManager fetchAgoraToken];
    NSDictionary *getDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", nil];
    NSURL *url = [self constructURLWithPath:path];
    NSMutableURLRequest *request =  [NetworkManager createGetRequestWithUrl:url andToken:getDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data

                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        if (agoraError) {
            //bounce the user out... probs issue a notification here.
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AgoraNetworkParser parseNetworkJSONwithMessages:json];
                completionBlock(agoraError);
            });
        }
    }];
    [task resume];
}

+(void)zeroBadgeForUserId:(int) userId {
    NSString *path = [NSString stringWithFormat:@"%@/zeroBadge/%@",profileUpdatePath,[NSNumber numberWithInt:userId]];
    NSString *token = [SessionManager fetchAgoraToken];
    NSDictionary *postDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", nil];
    NSURL *url = [self constructURLWithPath:path];
    NSMutableURLRequest *request =  [NetworkManager createPostRequestWithURL:url andDictionary:postDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
    }];
    [task resume];
}

+(void)zeroUnreadCountforConversation:(Conversation *)conversation {
    NSNumber *isSeller = ([SessionManager getCurrentUser].userId == conversation.sellerId) ? @YES : @NO;
    NSString *path = [NSString stringWithFormat:@"%@/zeroUnreadNumber/%@",chatPath,[NSNumber numberWithInt:conversation.conversationId]];
    NSString *token = [SessionManager fetchAgoraToken];
    NSDictionary *postDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", isSeller, @"isSeller", nil];
    NSURL *url = [self constructURLWithPath:path];
    NSMutableURLRequest *request =  [NetworkManager createPostRequestWithURL:url andDictionary:postDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
    }];
    [task resume];
}

+(void)reloadMarketplaceWithToken:(NSString *)token school:(NSString *)school andCompletionBlock:(void(^)(AgoraError *error))completionBlock {
    NSURL *url = [NetworkManager constructURLWithPath:marketplaceReload];
    NSDictionary *postDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", school, @"school", nil];
    NSMutableURLRequest *request = [NetworkManager createPostRequestWithURL:url andDictionary:postDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data

                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        if (agoraError) {
            [SessionManager logout];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [AgoraNetworkParser parseNetworkJSONandInsertIntoCoreData:json];
                completionBlock(agoraError);
            });
        }
    }];
    [task resume];
}

+(void)registerWithEmail:(NSString *)email
                password:(NSString *)password
                  school:(NSString *)school
      andCompletionBlock:(void(^)(AgoraError *error))completionBlock{
    NSURL *url = [NetworkManager constructURLWithPath:registerPath];
    NSDictionary *postDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:email, @"email", password, @"password", school, @"school", nil];
    [NetworkManager createAndExecuteRequestWithPostDictionary:postDictionary URL:url andCompletionBlock:completionBlock];
}

+(void)loginWithEmail:(NSString *)email
             password:(NSString *)password
            apnsToken:(NSString *)apnsToken
   andCompletionBlock:(void(^)(int userId, NSString *token, AgoraError *error))completionBlock {
    NSURL *url = [NetworkManager constructURLWithPath:loginPath];
    NSDictionary *postDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:email, @"email", password, @"password", apnsToken, @"apnsToken", nil];
    NSMutableURLRequest *request = [NetworkManager createPostRequestWithURL:(NSURL *)url andDictionary:(NSDictionary *)postDictionary];

    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(0, NULL, agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data

                              options:kNilOptions
                              error:&error];
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(0, NULL, agoraError);
            return;
        }
        agoraError = [NetworkManager constructErrorFromJSON:json];

        if (agoraError) {
            completionBlock(0, NULL, agoraError);
            return;
        }
        User *agoraUser = [NetworkManager constructUserFromJSON:json];
        completionBlock(agoraUser.userId, [json objectForKey:@"token"], agoraError);
    }];
    [task resume];
}

+(AgoraError *)constructAgoraErrorFromError:(NSError *)error {
    return [AgoraError createUnhandledNetworkError];
}

+(AgoraError *)constructErrorFromJSON:(NSDictionary *)json {
    NSDictionary *error = [json objectForKey:@"error"];
    if (!error) {
        return NULL;
    }
    return [[AgoraError alloc] initWithStatusCode:[[error valueForKey:@"statusCode"]unsignedIntegerValue] name:[error objectForKey:@"name"] message:[error objectForKey:@"message"]];
}

+(User *)constructUserFromJSON:(NSDictionary *)json {
    NSDictionary *userDict = [json objectForKey:@"user"];
    if (!userDict) {
        return NULL;
    }

    User *user = [[User alloc] initWithDictionary:userDict inManagedObjectContext:[NetworkManager privateContext]];
    [NetworkManager saveContexts];

    return user;
}

+ (void)saveContexts {
    [[NetworkManager privateContext] performBlock:^{
        NSError *childError = nil;
        if ([[NetworkManager privateContext] save:&childError]) {
            [[NetworkManager managedObjectContext] performBlock:^{
                NSError *parentError = nil;
                if (![[NetworkManager managedObjectContext] save:&parentError]) {
                    NSLog(@"Error saving parent");
                }
            }];
        } else {
            NSLog(@"Error saving child");
        }
    }];
}

+ (NSManagedObjectContext *)privateContext {
    NSManagedObjectContext *concurrentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [concurrentContext setParentContext:[NetworkManager managedObjectContext]];
    return concurrentContext;
}

+(NSArray *)constructConversationsFromJSON: (NSDictionary *)json {

    NSDictionary *conversationDict = [json objectForKey:@"conversations"];
    if (!conversationDict) {
        return NULL;
    }
    Conversation *conversation = [[Conversation alloc] initWithDictionary:conversationDict inManagedObjectContext:[NetworkManager managedObjectContext]];
    [[NetworkManager managedObjectContext] save:nil];
    return conversation;
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

+(NSURL *)constructURLWithPath:(NSString *)path {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, path]];
}

+(NSMutableURLRequest *)createPostRequestWithURL:(NSURL *)url andDictionary:(NSDictionary *)dict {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NetworkManager createPostStringFromDictionary:dict andHttpMethod:@"POST"];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    return request;
}

+(NSString *)createPostStringFromDictionary:(NSDictionary *)dict andHttpMethod: (NSString *) httpMethod{
    NSArray *keys = dict.allKeys;
    NSArray *values = dict.allValues;
    NSMutableString *postString = [[NSMutableString alloc] init];
    for (int i = 0; i < keys.count; i++) {
        [postString appendString:[NSString stringWithFormat:@"%@=", keys[i]]];
        [postString appendString:[NSString stringWithFormat:@"%@&", values[i]]];
    }
    if ([httpMethod isEqualToString:@"POST"])
    [postString appendString:@"&submit="];
    return postString;
}

+(void)createAndExecuteRequestWithPostDictionary:(NSDictionary *)postDictionary URL:(NSURL *)url andCompletionBlock:(void(^)(AgoraError *error))completionBlock{
    NSMutableURLRequest *request = [NetworkManager createPostRequestWithURL:url andDictionary:postDictionary];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        AgoraError *agoraError;
        if (error) {
            agoraError = [NetworkManager constructAgoraErrorFromError:error];
            completionBlock(agoraError);
            return;
        }
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data
                              
                              options:kNilOptions
                              error:&error];
        agoraError = [NetworkManager constructErrorFromJSON:json];
        completionBlock(agoraError);
    }];
    [task resume];
}


+(NSMutableURLRequest *)createGetRequestWithUrl:(NSURL *)url andToken:(NSDictionary *)token {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:token];
    return request;
}

+(NSMutableURLRequest *)createDeleteRequestWithUrl:(NSURL *)url andToken:(NSDictionary *)token {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    NSMutableData *postData = [[NSMutableData alloc] initWithData: [NSKeyedArchiver archivedDataWithRootObject:token]];
    NSString *body = [self createPostStringFromDictionary:token andHttpMethod:@"POST"];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    return request;
}


@end
