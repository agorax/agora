//
//  AgoraColors.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/6/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "AgoraColors.h"
#import <UIKit/UIKit.h>

@implementation AgoraColors

+ (UIColor *) agoraGreenColor
{
    UIColor *color = [UIColor colorWithRed:(56/255.0) green:(216/255.0) blue:(2/255.0) alpha:1];
    return color;
}

+ (UIColor *) agoraBlueColor
{
    UIColor *color = [UIColor colorWithRed:(66/255.0) green:(167/255.0) blue:(255/255.0) alpha:1];
    return color;
}

+ (UIColor *) agoraGrayColor
{
    UIColor *color = [UIColor colorWithRed:(208/255.0) green:(208/255.0) blue:(208/255.0) alpha:1];
    return color;
}

+ (UIColor *) agoraGradientColor
{
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"Gradient"]];
}

@end
