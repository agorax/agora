//
//  dateConverter.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dateConverter : NSObject

+ (NSString *)dateToString: (NSDate *) originalDate;
+ (NSDate *)stringToDate:(NSString *)originalString;
+ (int)listingExpires: (NSDate *)listDate;

@end
