//
//  ChatManager.m
//  AgoraExchange
//
//  Created by Travis Chambers on 7/21/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "AgoraNetworkParser.h"
#import "ChatManager.h"


//NSString *const domain = @"http://0783e2b6.ngrok.io"; //http://localhost:8000

@implementation ChatManager : NSObject

#if TARGET_IPHONE_SIMULATOR

NSString *const chatDomain = @"http://ec2-13-58-246-147.us-east-2.compute.amazonaws.com";

#else

NSString *const chatDomain = @"http://ec2-13-58-246-147.us-east-2.compute.amazonaws.com";

#endif

+ (instancetype)sharedInstance
{
    static ChatManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ChatManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (id) init {
    self = [super init];
    if( !self ) return nil;
    self.sioClient = [[SocketIOClient alloc] initWithSocketURL: [NSURL URLWithString:chatDomain] config:@{@"log": @YES, @"forcePolling": @YES}];
    return self;
}
- (void)establishConnection {
    [self.sioClient on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        // DO anything after connect here
    }];
    [self.sioClient connect];

}


- (void) closeConnection {
    [self.sioClient disconnect];
}

- (void) reconnectOnDisconnect: (void (^)())completionBlock {
    [self.sioClient on:@"disconnect" callback:^(NSArray* data,SocketAckEmitter *emitted){
        [self establishConnection];
        completionBlock();
    }];
}

- (void) createConversation: (int)sellerId buyer: (int) buyerId item: (int)itemId message: (NSString *) message senderDisplayName: (NSString *)senderDisplayName itemName: (NSString *) itemName completionBlock:(void(^)(NSNumber * conversationId)) completionBlock {
    [self establishConnection];
    [[self.sioClient emitWithAck:@"conversate" with:@[@{@"sellerId": [NSNumber numberWithInt:sellerId], @"buyerId":[NSNumber numberWithInt:buyerId], @"itemId" : [NSNumber numberWithInt:itemId], @"message" : message, @"senderDisplayName": senderDisplayName, @"itemName": itemName}]] timingOutAfter:0 callback:^(NSArray* data) {
        completionBlock(data[0]);
    }];
}

- (void) sendMessageToRoom: (NSString *)room event: (NSString *) event message: (NSString *)message from: (int) senderId convo:(int) convoId senderDisplayName: (NSString *) senderDisplayName recipientId: (int) recipientId numberOfPictures: (int) numberOfPictures completionBlock:(void(^)(NSNumber * messageId)) completionBlock {
    [[self.sioClient emitWithAck:event with:@[@{@"room": room, @"message":message, @"senderId":[NSNumber numberWithInt:senderId], @"conversationId" : [NSNumber numberWithInt:convoId], @"senderDisplayName": senderDisplayName, @"recipientId" :[NSNumber numberWithInt:recipientId], @"numberOfPictures": [NSNumber numberWithInt:numberOfPictures]}]] timingOutAfter:0 callback:^(NSArray* data) {
        completionBlock(data[0]);
    }];
}

- (void) sendMediaMessageToRoom: (NSString *)room event: (NSString *) event messageId: (NSNumber *) messageId message: (NSString *)message from: (int) senderId convo:(int) convoId senderDisplayName: (NSString *) senderDisplayName recipientId: (int) recipientId numberOfPictures: (int) numberOfPictures completionBlock:(void(^)(NSNumber * messageId)) completionBlock {
    [[self.sioClient emitWithAck:event with:@[@{@"room": room, @"messageId":messageId, @"message":message, @"senderId":[NSNumber numberWithInt:senderId], @"conversationId" : [NSNumber numberWithInt:convoId], @"senderDisplayName": senderDisplayName, @"recipientId" :[NSNumber numberWithInt:recipientId], @"numberOfPictures": [NSNumber numberWithInt:numberOfPictures]}]] timingOutAfter:0 callback:^(NSArray* data) {
        completionBlock(data[0]);
    }];
}

- (void) userIsTypingInRoom: (NSString *)room {
    NSLog(@"room: %@",room );
    if(room) {
        [self.sioClient emit:@"is typing" with:@[@{@"room": room}]];
    }
}

- (void) userStoppedTypingInRoom: (NSString *)room {
    NSLog(@"room: %@",room );
    if(room) {
        [self.sioClient emit:@"stopped typing" with:@[@{@"room": room}]];
    }
}

- (void) listenForTyping: (void(^)(bool))completionBlock {
    [self.sioClient on:@"user typing" callback:^(NSArray* data,SocketAckEmitter *emitted){
        completionBlock([data[0][@"typing"] isEqualToString:@"true"]);
    }];
}

- (void) getMessages: (void (^)(Message *))completionBlock {
    [self.sioClient on:@"received" callback:^(NSArray* data,SocketAckEmitter *emitted){
        Message *message = [AgoraNetworkParser parseNetworkJSONwithSocketMessage:data];
        completionBlock(message);
    }];
   
}

- (void) subscribeToRoom: (NSString *)room {
     [self.sioClient emit:@"subscribe" with:@[@{@"room":room}]];
}

@end

