//
//  keyboardController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/12/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface keyboardController : NSObject

+ (void) registerKeyboardControllerWithVC: (UIViewController *)vc;

@end
