//
//  ChatTableViewCell.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/10/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ChatTableViewCell.h"
#import "AgoraColors.h"
#import "AgoraImageManager.h"

static NSMutableDictionary *imageCache;

@interface ChatTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end

@implementation ChatTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.unReadAlert.layer.cornerRadius = self.unReadAlert.layer.bounds.size.height/2.0;
    self.unReadAlert.clipsToBounds = YES;
    self.unReadAlert.hidden = YES;
    if (!imageCache)
    {
        imageCache = [[NSMutableDictionary alloc] init];
    }
    self.profilePictureImageView.image = [UIImage imageNamed:@"Image"];
}

- (void) setViewModel:(ChatTableCellViewModel *)viewModel {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.itemNameLabel.text = viewModel.itemName;
        self.nameLabel.text = viewModel.name;
        self.dateLabel.text = viewModel.dateString;
        self.lastMessageLabel.text = viewModel.lastMessage;
        self.unReadAlert.hidden = ([viewModel.conversation respondsToSelector:NSSelectorFromString(@"unReadMessages")]) ?(viewModel.conversation.unReadMessages == 0) : true;
        NSURL *imageUrl = [NSURL URLWithString:[AgoraImageManager urlForItem:viewModel.item atIndex:1]];
        [self loadPictureWithURL:imageUrl];
    });
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    [selectedBackgroundView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    selectedBackgroundView.layer.borderWidth = 2;
    selectedBackgroundView.layer.borderColor = [AgoraColors agoraBlueColor].CGColor;
    selectedBackgroundView.alpha = .7;
    [self setSelectedBackgroundView:selectedBackgroundView];
}

- (void) loadPictureWithURL:(NSURL *)url
{
    NSData *data = [imageCache valueForKey:[url absoluteString]];
    UIImage *im;
    if (data) {
        im = [UIImage imageWithData:data];
    }
    if (im)
    {
        [UIView transitionWithView:self.profilePictureImageView
                          duration:0.4f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.profilePictureImageView.image = im;
                        } completion:nil];
    }
    else
    {
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    [imageCache setValue:data forKey:[url absoluteString]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.profilePictureImageView.image = image;
                    });
                }
            }
        }];
        [task resume];
    }
}

+ (void)clearImageCache
{
    imageCache = nil;
}

-(void)prepareForReuse
{
    self.profilePictureImageView.image = [UIImage imageNamed:@"Image"];
    self.unReadAlert.hidden = YES;
    [super prepareForReuse];
}

@end
