//
//  ReviewTableViewCell.h
//  AgoraX
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+CoreDataClass.h"

@interface ReviewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *firstStarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *secondStarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *thirdStarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *fourthStarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *fifthStarImageView;

@property (weak, nonatomic) IBOutlet UILabel *reviewDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *reviewerProfilePicture;
@property (strong, nonatomic) IBOutlet UILabel *ratingText;

@property (strong, nonatomic) User *reviewer;
+ (instancetype)instantiateFromNib;

@end
