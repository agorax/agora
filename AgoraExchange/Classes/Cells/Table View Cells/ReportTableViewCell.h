//
//  ReportTableViewCell.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/10/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *reportReason;
- (void) setupWithOption:(NSString *) option;

@end
