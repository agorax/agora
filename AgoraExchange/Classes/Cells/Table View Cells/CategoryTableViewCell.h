//
//  CategoryTableViewCell.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/3/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewCell : UITableViewCell

- (void) setupWithCategory: (NSString *) category;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;

@end
