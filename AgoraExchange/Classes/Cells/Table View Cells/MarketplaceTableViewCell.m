//
//  MarketplaceTableViewCell.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MarketplaceTableViewCell.h"
#import "AgoraColors.h"

@implementation MarketplaceTableViewCell

+ (instancetype) instantiateFromNib
{
    return [[NSBundle mainBundle] loadNibNamed:@"MarketplaceTableViewCell" owner:self options:nil].firstObject;
}

- (void) prepareForReuse
{
    [super prepareForReuse];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) setupWithOption:(NSString *) option
{
    self.optionName.text = option;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    [selectedBackgroundView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    selectedBackgroundView.layer.borderWidth = 2;
    selectedBackgroundView.layer.borderColor = [AgoraColors agoraBlueColor].CGColor;
    selectedBackgroundView.alpha = .7;
    [self setSelectedBackgroundView:selectedBackgroundView];
}

@end
