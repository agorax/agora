//
//  MyListingsTableViewCell.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/21/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/ParseUI.h>
#import <BEMCheckBox/BEMCheckBox.h>

@protocol ListingTableViewCellDelegate <NSObject>

- (void)checkboxDidChangeTo:(BOOL)checked onItemId:(int)itemId;

@end

@interface MyListingsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *picture;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkbox;
@property (nonatomic) int itemId;
@property (weak, nonatomic) id <ListingTableViewCellDelegate>delegate;

@end
