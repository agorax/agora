//
//  ReportTableViewCell.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/10/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ReportTableViewCell.h"
#import "AgoraColors.h"

@implementation ReportTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

+ (instancetype) instantiateFromNib
{
    return [[NSBundle mainBundle] loadNibNamed:@"ReportTableViewCell" owner:self options:nil].firstObject;
}

- (void) setupWithOption:(NSString *) option
{
    self.reportReason.text = option;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    [selectedBackgroundView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    selectedBackgroundView.layer.borderWidth = 2;
    selectedBackgroundView.layer.borderColor = [AgoraColors agoraBlueColor].CGColor;
    selectedBackgroundView.alpha = .7;
    [self setSelectedBackgroundView:selectedBackgroundView];
}

@end
