//
//  ChatTableCellViewModel.h
//  AgoraExchange
//
//  Created by Travis Chambers on 8/22/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Conversation+CoreDataClass.h"
#import "User+CoreDataClass.h"
#import "Item+CoreDataClass.h"

@interface ChatTableCellViewModel : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *itemName;
@property (strong, nonatomic) NSString *lastMessage;
@property (strong, nonatomic) NSString *dateString;
@property (strong, nonatomic) Item *item;
@property (strong, nonatomic) Conversation *conversation;

-(id) initWithConversation: (Conversation *)conversation item: (Item *) item;

@end
