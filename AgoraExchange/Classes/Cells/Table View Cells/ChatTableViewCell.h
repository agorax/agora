//
//  ChatTableViewCell.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/10/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/ParseUI.h>
#import "ChatTableCellViewModel.h"

@interface ChatTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (strong, nonatomic) ChatTableCellViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UIView *unReadAlert;
@property (strong, nonatomic) NSURLSession *session;

- (void)loadPictureWithURL: (NSURL *)url;
+ (void)clearImageCache;
- (void)setViewModel:(ChatTableCellViewModel *)viewModel;

@end
