//
//  ChatTableCellViewModel.m
//  AgoraExchange
//
//  Created by Travis Chambers on 8/22/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "ChatTableCellViewModel.h"
#import "SessionManager.h"
#import <UIKit/UIKit.h>

@implementation ChatTableCellViewModel

-(id) initWithConversation:(Conversation *)conversation item:(Item *)item {
    self = [super init];
    if (self) {
        User *otherUser = [self getOtherUserFromConversation: conversation];
        self.name = (otherUser.name && [otherUser.name length] > 0) ? otherUser.name : otherUser.email;
        self.lastMessage = conversation.lastMessage;
        self.itemName = item.name;
        self.dateString = [self formatDate:conversation.updatedAt];
        self.item = item;
        self.conversation = conversation;
    }
    return self;
}

-(NSString *) formatDate: (NSDate *)date {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd"];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    return [formatter stringFromDate:date];
}

-(User *)getOtherUserFromConversation: (Conversation *)conversation {
    if ([SessionManager getCurrentUser].userId == conversation.buyerId)
    {
        User *seller = [User getUserWithId:conversation.sellerId inManagedObjectContext:[ChatTableCellViewModel managedObjectContext]];
        return seller;
    }
    else
    {
        User *buyer = [User getUserWithId:conversation.buyerId inManagedObjectContext:[ChatTableCellViewModel managedObjectContext]];
        return buyer;
    }
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
