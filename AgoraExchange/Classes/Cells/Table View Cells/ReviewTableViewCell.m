//
//  ReviewTableViewCell.m
//  AgoraX
//
//  Created by Justin Wang on 7/9/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ReviewTableViewCell.h"

@implementation ReviewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (instancetype)instantiateFromNib
{
    return [[NSBundle mainBundle] loadNibNamed:@"ReviewTableViewCell" owner:self options:nil].firstObject;
}

- (void)prepareForReuse
{
    self.reviewerProfilePicture.image = [UIImage imageNamed:@"Profile Picture Small"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    selectedBackgroundView.layer.borderWidth = 2;
    selectedBackgroundView.layer.borderColor = [UIColor whiteColor].CGColor;
    [self setSelectedBackgroundView:selectedBackgroundView];
}

@end
