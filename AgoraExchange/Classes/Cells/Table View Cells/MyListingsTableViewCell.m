//
//  MyListingsTableViewCell.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/21/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "MyListingsTableViewCell.h"
#import "AgoraColors.h"

@interface MyListingsTableViewCell() <BEMCheckBoxDelegate>

@end

@implementation MyListingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.checkbox.delegate = self;
    // Initialization code
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.picture.image = [UIImage imageNamed:@"Image"];
    self.checkbox.on = false;
}

-(void)didTapCheckBox:(BEMCheckBox *)checkBox {
    [self.delegate checkboxDidChangeTo:checkBox.on onItemId:self.itemId];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    selectedBackgroundView.layer.borderWidth = 2;
    selectedBackgroundView.layer.borderColor = [UIColor whiteColor].CGColor;
    [self setSelectedBackgroundView:selectedBackgroundView];
}

@end
