//
//  MyProfileTableViewCell.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/10/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileTableViewCell : UITableViewCell

- (void) setupWithOption: (NSString *) option;
@property (strong, nonatomic) IBOutlet UILabel *optionName;

@end
