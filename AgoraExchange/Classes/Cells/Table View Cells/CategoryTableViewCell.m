//
//  CategoryTableViewCell.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/3/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "CategoryTableViewCell.h"
#import "AgoraColors.h"

@implementation CategoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

+ (instancetype) instantiateFromNib
{
    return [[NSBundle mainBundle] loadNibNamed:@"CategoryTableViewCell" owner:self options:nil].firstObject;
}

- (void) setupWithCategory:(NSString *) category
{
    self.categoryLabel.text = category;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    [selectedBackgroundView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    selectedBackgroundView.layer.borderWidth = 2;
    selectedBackgroundView.layer.borderColor = [AgoraColors agoraBlueColor].CGColor;
    selectedBackgroundView.alpha = .7;
    [self setSelectedBackgroundView:selectedBackgroundView];
}

@end
