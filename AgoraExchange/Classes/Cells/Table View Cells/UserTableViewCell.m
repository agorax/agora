//
//  UserTableViewCell.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/26/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "UserTableViewCell.h"
#import "AgoraColors.h"

@implementation UserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.profilePictureImageView.layer.cornerRadius = 25.0;
    [self.profilePictureImageView setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    [selectedBackgroundView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    selectedBackgroundView.layer.borderWidth = 2;
    selectedBackgroundView.layer.borderColor = [AgoraColors agoraBlueColor].CGColor;
    selectedBackgroundView.alpha = .7;
    [self setSelectedBackgroundView:selectedBackgroundView];
}

@end
