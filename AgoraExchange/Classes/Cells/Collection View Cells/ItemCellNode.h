//
//  ItemCellNode.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "Item+CoreDataClass.h"

@interface ItemCellNode : ASCellNode

- (void)setupWithItem:(Item *)item;

@end
