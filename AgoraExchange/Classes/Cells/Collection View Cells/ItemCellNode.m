//
//  ItemCellNode.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/30/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "ItemCellNode.h"
#import "dateConverter.h"
#import "WishlistController.h"
#import "AgoraColors.h"

@interface ItemCellNode () <ASNetworkImageNodeDelegate>

@property (strong, nonatomic) ASTextNode *nameTextNode;
@property (strong, nonatomic) ASTextNode *priceTextNode;
@property (strong, nonatomic) ASImageNode *calendarIconImageNode;
@property (strong, nonatomic) ASTextNode *dateTextNode;
@property (strong, nonatomic) ASNetworkImageNode *photoImageNode;
@property (strong, nonatomic) ASImageNode *favoriteIconImageNode;
@property (strong, nonatomic) ASTextNode *numberLikesTextNode;
@property (strong, nonatomic) Item *item;
@property int numberLikes;

@end

@implementation ItemCellNode

- (instancetype) init
{
    self = [super init];
    
    self.backgroundColor = [UIColor whiteColor];
    self.nameTextNode = [[ASTextNode alloc] init];
    self.nameTextNode.layerBacked = YES;
    [self addSubnode:self.nameTextNode];
    
    self.dateTextNode = [[ASTextNode alloc] init];
    self.dateTextNode.layerBacked = YES;
    [self addSubnode:self.dateTextNode];
    
    self.priceTextNode = [[ASTextNode alloc] init];
    self.priceTextNode.layerBacked = YES;
    [self addSubnode:self.priceTextNode];
    
    self.photoImageNode = [[ASNetworkImageNode alloc] init];
    self.photoImageNode.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageNode.defaultImage = [UIImage imageNamed:@"Image"];
    self.photoImageNode.backgroundColor = [AgoraColors agoraGrayColor];
    self.photoImageNode.delegate = self;
    [self addSubnode:self.photoImageNode];
    
    self.favoriteIconImageNode = [[ASImageNode alloc] init];
    self.favoriteIconImageNode.contentMode = UIViewContentModeScaleAspectFill;
    self.favoriteIconImageNode.layerBacked = YES;
    [self addSubnode:self.favoriteIconImageNode];
    
    self.calendarIconImageNode = [[ASImageNode alloc] init];
    self.calendarIconImageNode.contentMode = UIViewContentModeScaleAspectFill;
    self.calendarIconImageNode.layerBacked = YES;
    [self addSubnode:self.calendarIconImageNode];
    
    self.numberLikesTextNode = [[ASTextNode alloc] init];
    self.numberLikesTextNode.layerBacked = YES;
    [self addSubnode:self.numberLikesTextNode];
    
    self.usesImplicitHierarchyManagement = YES;
    
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    ASStackLayoutSpec *verticalStack = [ASStackLayoutSpec verticalStackLayoutSpec];
    verticalStack.alignItems = ASStackLayoutAlignItemsCenter;
    
    self.photoImageNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, constrainedSize.max.width);
    
    //ASStackLayoutSpec *favoriteHorizontalStack = [ASStackLayoutSpec horizontalStackLayoutSpec];
    //favoriteHorizontalStack.alignItems = ASStackLayoutAlignItemsCenter;
    //favoriteHorizontalStack.children = @[self.favoriteIconImageNode, self.numberLikesTextNode];
    
    //ASStackLayoutSpec *dateHorizontalStack = [ASStackLayoutSpec horizontalStackLayoutSpec];
    //dateHorizontalStack.alignItems = ASStackLayoutAlignItemsCenter;
    //dateHorizontalStack.children = @[self.calendarIconImageNode, self.dateTextNode];
    
    verticalStack.children = @[self.photoImageNode, self.nameTextNode, self.priceTextNode];
    
    ASInsetLayoutSpec *insetLayoutSpec = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(8.0f, 8.0f, 8.0f, 8.0f) child:verticalStack];
    
    return insetLayoutSpec;
}

- (void)setupWithItem:(Item *)item
{
    self.item = item;
    NSMutableParagraphStyle *textParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    textParagraphStyle.alignment = NSTextAlignmentCenter;
    
    self.nameTextNode.attributedText = [[NSAttributedString alloc] initWithString:item.name attributes:@{NSParagraphStyleAttributeName: textParagraphStyle, NSForegroundColorAttributeName: [UIColor blackColor], NSFontAttributeName: [UIFont systemFontOfSize:23]}];
    self.priceTextNode.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"$%d", item.price] attributes:@{NSParagraphStyleAttributeName: textParagraphStyle, NSForegroundColorAttributeName: [UIColor darkGrayColor], NSFontAttributeName: [UIFont systemFontOfSize:17]}];
//    self.dateTextNode.attributedText = [[NSAttributedString alloc] initWithString:[dateConverter dateToString:item.createdAt] attributes:@{NSParagraphStyleAttributeName: textParagraphStyle, NSForegroundColorAttributeName: [UIColor darkGrayColor], NSFontAttributeName: [UIFont systemFontOfSize:17]}];
    self.photoImageNode.URL = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3.amazonaws.com/agora-exchange-bucket/Item/%d-1", item.itemId]];
    self.favoriteIconImageNode.image = [UIImage imageNamed:@"Favorite Icon Blue"];
    self.calendarIconImageNode.image = [UIImage imageNamed:@"Calendar Icon"];
    //self.numberLikes = [item[@"numberLikes"] integerValue];
    self.numberLikesTextNode.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", self.numberLikes] attributes:@{NSParagraphStyleAttributeName: textParagraphStyle, NSForegroundColorAttributeName: [UIColor darkGrayColor], NSFontAttributeName: [UIFont systemFontOfSize:17]}];
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted) {
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    } else {
        self.backgroundColor = [UIColor whiteColor];
    }
}

-(void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    if(image)
    {
        [UIView transitionWithView:imageNode.view
                          duration:2.0f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            imageNode.image = image;
                        } completion:nil];
    }
}

@end
