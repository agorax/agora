//
//  Constants.m
//  AgoraExchange
//
//  Created by Justin Wang on 6/16/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString *const userBio = @"custom:bio";
NSString *const userLocation = @"custom:location";
NSString *const userRating = @"custom:rating";
NSString *const userNumberOfRatings = @"custom:num_ratings";
NSString *const userNumberOfReports = @"custom:num_reports";

NSString *const emptyAttributeValue = @"EMPTY";

@end
