//
//  Constants.h
//  AgoraExchange
//
//  Created by Justin Wang on 6/16/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

extern NSString *const userBio;
extern NSString *const userLocation;
extern NSString *const userRating;
extern NSString *const userNumberOfRatings;
extern NSString *const userNumberOfReports;
extern NSString *const emptyAttributeValue;

@end
