//
//  WishlistController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/18/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "WishlistController.h"

typedef NSMutableDictionary<NSString *, NSOperationQueue *> MyDictionary;

static MyDictionary *operationQueueDictionary;

@implementation WishlistController

+ (void)addItemToWishlist: (PFObject *)item
{
    if (!operationQueueDictionary)
    {
        operationQueueDictionary = [[MyDictionary alloc] init];
    }
    NSOperationQueue *operationQueue = [operationQueueDictionary objectForKey:item.objectId];
    if (!operationQueue)
    {
        operationQueue = [[NSOperationQueue alloc] init];
        operationQueue.maxConcurrentOperationCount = 1;
        [operationQueueDictionary setObject:operationQueue forKey:item.objectId];
    }
    NSBlockOperation *addOperation = [[NSBlockOperation alloc] init];
    [addOperation addExecutionBlock:^{
        PFObject *wishlist = [PFObject objectWithClassName:@"Wishlist"];
        wishlist = [[PFUser currentUser][@"wishlistPointer"] fetch];
        NSMutableArray *itemIdArray = [[NSMutableArray alloc] initWithArray:wishlist[@"itemIdArray"]];
        if ([itemIdArray containsObject:item.objectId])
        {
            return;
        }
        [itemIdArray addObject:item.objectId];
        wishlist[@"itemIdArray"] = itemIdArray;
        NSMutableArray *wishlistIdArray = [[NSMutableArray alloc] initWithArray:item[@"wishlistIdArray"]];
        if ([wishlistIdArray containsObject:wishlist.objectId])
        {
            return;
        }
        [wishlistIdArray addObject:wishlist.objectId];
        item[@"wishlistIdArray"] = wishlistIdArray;
        item[@"numberLikes"] = [NSNumber numberWithInt:(int)[wishlistIdArray count]];
        if([wishlist save])
        {
            [item save];
        }
    }];
    [operationQueue addOperation:addOperation];
    
}

+ (void)removeItemFromWishlist: (PFObject *)item
{
    if (!operationQueueDictionary)
    {
        operationQueueDictionary = [[MyDictionary alloc] init];
    }
    NSOperationQueue *operationQueue = [operationQueueDictionary objectForKey:item.objectId];
    if (!operationQueue)
    {
        operationQueue = [[NSOperationQueue alloc] init];
        operationQueue.maxConcurrentOperationCount = 1;
        [operationQueueDictionary setObject:operationQueue forKey:item.objectId];
    }
    
    NSBlockOperation *deleteOperation = [[NSBlockOperation alloc] init];
    [deleteOperation addExecutionBlock:^{
        PFObject *wishlist = [PFObject objectWithClassName:@"Wishlist"];
        wishlist = [[PFUser currentUser][@"wishlistPointer"] fetch];
        NSMutableArray *newArray = [[NSMutableArray alloc] initWithArray:wishlist[@"itemIdArray"]];
        if (![newArray containsObject:item.objectId])
        {
            return;
        }
        [newArray removeObject:item.objectId];
        wishlist[@"itemIdArray"] = newArray;
        NSMutableArray *wishlistIdArray = [[NSMutableArray alloc] initWithArray:item[@"wishlistIdArray"]];
        if (![wishlistIdArray containsObject:wishlist.objectId])
        {
            return;
        }
        [wishlistIdArray removeObject:wishlist.objectId];
        item[@"wishlistIdArray"] = wishlistIdArray;
        item[@"numberLikes"] = [NSNumber numberWithInt:[wishlistIdArray count]];
        if ([wishlist save]) {
            [item save];
        }
    }];
    [operationQueue addOperation:deleteOperation];
}

@end
