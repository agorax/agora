//
//  FollowController.m
//  AgoraExchange
//
//  Created by Justin Wang on 7/25/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "FollowController.h"
#import <Parse/Parse.h>

typedef NSMutableDictionary<NSString *, NSOperationQueue *> MyDictionary;

static MyDictionary *operationQueueDictionary;

@implementation FollowController

+ (void)followUserWithObjectId:(NSString *)userId
{
    if (!operationQueueDictionary)
    {
        operationQueueDictionary = [[MyDictionary alloc] init];
    }
    NSOperationQueue *operationQueue = [operationQueueDictionary objectForKey:userId];
    if (!operationQueue)
    {
        operationQueue = [[NSOperationQueue alloc] init];
        operationQueue.maxConcurrentOperationCount = 1;
        [operationQueueDictionary setObject:operationQueue forKey:userId];
    }
    NSBlockOperation *followOperation = [[NSBlockOperation alloc] init];
    [followOperation addExecutionBlock:^{
        NSMutableArray *followingArray = [NSMutableArray arrayWithArray:[PFUser currentUser][@"followingArray"]];
        if (![followingArray containsObject:userId])
        {
            [followingArray addObject:userId];
            [PFUser currentUser][@"followingArray"] = followingArray;
            [[PFUser currentUser] saveInBackground];
        }
    }];
    [operationQueue addOperation:followOperation];

}

+ (void)unfollowUserWithObjectId:(NSString *)userId
{
    if (!operationQueueDictionary)
    {
        operationQueueDictionary = [[MyDictionary alloc] init];
    }
    NSOperationQueue *operationQueue = [operationQueueDictionary objectForKey:userId];
    if (!operationQueue)
    {
        operationQueue = [[NSOperationQueue alloc] init];
        operationQueue.maxConcurrentOperationCount = 1;
        [operationQueueDictionary setObject:operationQueue forKey:userId];
    }
    NSBlockOperation *unfollowOperation = [[NSBlockOperation alloc] init];
    [unfollowOperation addExecutionBlock:^{
        NSMutableArray *followingArray = [NSMutableArray arrayWithArray:[PFUser currentUser][@"followingArray"]];
        if ([followingArray containsObject:userId])
        {
            [followingArray removeObject:userId];
            [PFUser currentUser][@"followingArray"] = followingArray;
            [[PFUser currentUser] saveInBackground];
        }
    }];
    [operationQueue addOperation:unfollowOperation];
    
}

@end
