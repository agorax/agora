//
//  User+CoreDataProperties.h
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "User+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *school;
@property (nonatomic) int16_t rating;
@property (nonatomic) int16_t numRatings;
@property (nullable, nonatomic, copy) NSString *bio;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) BOOL emailVerified;
@property (nullable, nonatomic, copy) NSString *email;
@property (nonatomic) int16_t userId;
@property (nullable, nonatomic, copy) NSString *pictureURL;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSDate *createdAt;
@property (nullable, nonatomic, copy) NSDate *updatedAt;

@end

NS_ASSUME_NONNULL_END
