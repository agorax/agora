//
//  Item+CoreDataProperties.h
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "Item+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Item (CoreDataProperties)

+ (NSFetchRequest<Item *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *category;
@property (nullable, nonatomic, copy) NSString *itemDescription;
@property (nullable, nonatomic, copy) NSString *school;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) int16_t price;
@property (nonatomic) int16_t userId;
@property (nonatomic) int16_t itemId;
@property (nullable, nonatomic, copy) NSString *author;
@property (nonatomic) int64_t isbn;
@property (nonatomic) int16_t edition;
@property (nonatomic) int16_t numberOfPictures;
@property (nullable, nonatomic, copy) NSDate *createdAt;
@property (nullable, nonatomic, copy) NSDate *updatedAt;

@end

NS_ASSUME_NONNULL_END
