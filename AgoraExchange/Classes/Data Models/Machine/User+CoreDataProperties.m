//
//  User+CoreDataProperties.m
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"User"];
}

@dynamic school;
@dynamic rating;
@dynamic numRatings;
@dynamic bio;
@dynamic name;
@dynamic emailVerified;
@dynamic email;
@dynamic userId;
@dynamic pictureURL;
@dynamic location;
@dynamic createdAt;
@dynamic updatedAt;

@end
