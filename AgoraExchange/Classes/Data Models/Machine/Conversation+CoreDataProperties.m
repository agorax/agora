//
//  Conversation+CoreDataProperties.m
//  AgoraExchange
//
//  Created by Travis Chambers on 7/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "Conversation+CoreDataProperties.h"

@implementation Conversation (CoreDataProperties)

+ (NSFetchRequest<Conversation *> *)fetchRequest {
    return [[NSFetchRequest alloc] initWithEntityName:@"Conversation"];
}

@dynamic conversationId;
@dynamic sellerId;
@dynamic buyerId;
@dynamic room;
@dynamic itemId;
@dynamic lastMessage;
@dynamic unReadMessages;
@dynamic updatedAt;
@dynamic createdAt;

@end
