//
//  Conversation+CoreDataProperties.h
//  AgoraExchange
//
//  Created by Travis Chambers on 7/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "Conversation+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface Conversation (CoreDataProperties)

+ (NSFetchRequest<Conversation *> *)fetchRequest;

@property (nonatomic, copy) NSString *room;
@property (nonatomic) int16_t conversationId;
@property (nonatomic) int16_t sellerId;
@property (nonatomic) int16_t buyerId;
@property (nonatomic) int16_t itemId;
@property (nonatomic, copy) NSString *lastMessage;
@property (nonatomic) int16_t unReadMessages;
@property (nonatomic) NSDate *createdAt;
@property (nonatomic) NSDate *updatedAt;

@end

NS_ASSUME_NONNULL_END
