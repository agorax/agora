//
//  Review+CoreDataProperties.m
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "Review+CoreDataProperties.h"

@implementation Review (CoreDataProperties)

+ (NSFetchRequest<Review *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Review"];
}

@dynamic reviewDescription;
@dynamic rating;
@dynamic reviewedUserId;
@dynamic reviewerUserId;
@dynamic createdAt;
@dynamic updatedAt;

@end
