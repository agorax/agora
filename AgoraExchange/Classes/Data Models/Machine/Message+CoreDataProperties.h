//
//  Message+CoreDataProperties.h
//  AgoraExchange
//
//  Created by Travis Chambers on 7/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Message+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface Message (CoreDataProperties)

+ (NSFetchRequest<Message *> *)fetchRequest;

@property (nonatomic, copy) NSString *message;
@property (nonatomic) int16_t conversationId;
@property (nonatomic) int16_t senderId;
@property (nonatomic) int16_t messageId;
@property (nonatomic) int16_t numberOfPictures;
@property (nonatomic) NSDate *createdAt;
@property (nonatomic) NSDate *updatedAt;

@end

NS_ASSUME_NONNULL_END
