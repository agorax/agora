//
//  Message+CoreDataProperties.m
//  AgoraExchange
//
//  Created by Travis Chambers on 7/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "Message+CoreDataProperties.h"

@implementation Message (CoreDataProperties)

+ (NSFetchRequest<Message *> *)fetchRequest {
    return [[NSFetchRequest alloc] initWithEntityName:@"Message"];
}

@dynamic conversationId;
@dynamic senderId;
@dynamic messageId;
@dynamic message;
@dynamic numberOfPictures;
@dynamic updatedAt;
@dynamic createdAt;


@end
