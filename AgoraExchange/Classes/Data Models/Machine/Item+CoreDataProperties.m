//
//  Item+CoreDataProperties.m
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "Item+CoreDataProperties.h"

@implementation Item (CoreDataProperties)

+ (NSFetchRequest<Item *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Item"];
}

@dynamic category;
@dynamic itemDescription;
@dynamic school;
@dynamic name;
@dynamic price;
@dynamic userId;
@dynamic itemId;
@dynamic author;
@dynamic edition;
@dynamic isbn;
@dynamic numberOfPictures;
@dynamic createdAt;
@dynamic updatedAt;

@end
