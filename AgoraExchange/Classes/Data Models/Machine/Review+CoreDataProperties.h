//
//  Review+CoreDataProperties.h
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "Review+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Review (CoreDataProperties)

+ (NSFetchRequest<Review *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *reviewDescription;
@property (nonatomic) int16_t rating;
@property (nonatomic) int16_t reviewedUserId;
@property (nonatomic) int16_t reviewerUserId;
@property (nullable, nonatomic, copy) NSDate *createdAt;
@property (nullable, nonatomic, copy) NSDate *updatedAt;

@end

NS_ASSUME_NONNULL_END
