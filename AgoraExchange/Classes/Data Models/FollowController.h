//
//  FollowController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/25/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FollowController : NSObject

+ (void)followUserWithObjectId: (NSString *)userId;
+ (void)unfollowUserWithObjectId: (NSString *)userId;

@end
