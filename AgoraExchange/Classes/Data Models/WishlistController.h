//
//  WishlistController.h
//  AgoraExchange
//
//  Created by Justin Wang on 7/18/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface WishlistController : NSObject

+ (void)addItemToWishlist: (PFObject *)item;
+ (void)removeItemFromWishlist: (PFObject *)item;

@end
