//
//  Item+CoreDataClass.m
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "Item+CoreDataClass.h"
#import "dateConverter.h"
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@implementation Item

-(Item *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context{
    self = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
    [self updateFromAttributes:dictionary];
    
    return self;
}

-(void)updateFromAttributes:(NSDictionary *)attributes {
    if ([self.updatedAt compare:[dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"updatedAt"])]] == NSOrderedSame && self.updatedAt) {
        return;
    }
    self.school = NULL_TO_NIL([attributes objectForKey:@"school"]);
    self.itemDescription = NULL_TO_NIL([attributes valueForKey:@"itemDescription"]);
    self.name = NULL_TO_NIL([attributes objectForKey:@"name"]);
    self.price = [[attributes objectForKey:@"price"] integerValue];
    self.userId = [[attributes valueForKey:@"userId"] integerValue];
    self.itemId = [[attributes objectForKey:@"itemId"] integerValue];
    self.category = NULL_TO_NIL([attributes objectForKey:@"category"]);
    self.author = NULL_TO_NIL([attributes objectForKey:@"author"]);
    self.edition = [NULL_TO_NIL([attributes objectForKey:@"edition"]) integerValue];
    self.isbn = [NULL_TO_NIL([attributes objectForKey:@"isbn"]) integerValue];
    self.numberOfPictures = [[attributes objectForKey:@"numberOfPictures"] integerValue];
    self.createdAt = [dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"createdAt"])];
    self.updatedAt = [dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"updatedAt"])];
}

+(Item *)getItemWithId:(int)itemId inManagedObjectContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"itemId == %d", itemId]];
    [request setFetchLimit:1];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return (results && [results count] == 1) ? [results firstObject] : NULL;
}

+(Item *)getItemWithId:(int)itemId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request{
    [request setPredicate:[NSPredicate predicateWithFormat:@"itemId == %d", itemId]];
    [request setFetchLimit:1];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return (results && [results count] == 1) ? [results firstObject] : NULL;
}

@end
