//
//  Review+CoreDataClass.m
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "Review+CoreDataClass.h"
#import "dateConverter.h"
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

/*
 @property (nullable, nonatomic, copy) NSString *reviewDescription;
 @property (nonatomic) int16_t rating;
 @property (nonatomic) int16_t reviewedUserId;
 @property (nonatomic) int16_t reviewerUserId;

 */


@implementation Review

-(Review *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context {
    self = [NSEntityDescription insertNewObjectForEntityForName:@"Review" inManagedObjectContext:context];
    [self updateFromAttributes:dictionary];
    return self;
}

-(void)updateFromAttributes:(NSDictionary *)attributes {
    if ([self.updatedAt compare:[dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"updatedAt"])]] == NSOrderedSame && self.updatedAt) {
        return;
    }
    self.reviewedUserId = [[attributes objectForKey:@"reviewedUserId"] integerValue];
    self.reviewerUserId = [[attributes objectForKey:@"reviewerUserId"] integerValue];
    self.rating = [[attributes objectForKey:@"rating"] integerValue];
    self.reviewDescription = NULL_TO_NIL([attributes objectForKey:@"reviewDescription"]);
    self.createdAt = [dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"createdAt"])];
    self.updatedAt = [dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"updatedAt"])];

}

+(Review *)getReviewWithReviewerId:(int)reviewerId reviewedId:(int)reviewedId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request{
    [request setPredicate:[NSPredicate predicateWithFormat:@"reviewerUserId == %d && reviewedUserId == %d", reviewerId, reviewedId]];
    [request setFetchLimit:1];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return (results && [results count] == 1) ? [results firstObject] : NULL;
}

@end
