//
//  Message+CoreDataClass.m
//  AgoraExchange
//
//  Created by Travis Chambers on 7/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "Message+CoreDataClass.h"

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@implementation Message

-(Message *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context{
    self = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
    [self updateFromAttributes:dictionary];
    
    return self;
}

-(void)updateFromAttributes:(NSDictionary *)attributes {
    self.message = NULL_TO_NIL([attributes valueForKey:@"message"]);
    self.senderId = [NULL_TO_NIL([attributes objectForKey:@"senderId"]) integerValue];
    self.messageId = [NULL_TO_NIL([attributes objectForKey:@"messageId"]) integerValue];
    self.conversationId = [NULL_TO_NIL([attributes valueForKey:@"conversationId"]) integerValue];
    self.numberOfPictures = ([NULL_TO_NIL([attributes valueForKey:@"numberOfPictures"]) integerValue] == 1) ? 1 : 0 ;
}

+(Message *)getMessageWithId:(int)messageId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request {
    [request setPredicate:[NSPredicate predicateWithFormat:@"messageId == %d", messageId]];
    [request setFetchLimit:1];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return (results && [results count] == 1) ? [results firstObject] : NULL;
}

+(NSArray *)getMessagesWithConversationId:(int)conversationId andRefreshCount: (int)refreshCount inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request {
    [request setPredicate:[NSPredicate predicateWithFormat:@"conversationId == %d", conversationId]];
    [request setFetchLimit:20];
    [request setFetchOffset:20 * refreshCount];
    NSSortDescriptor *messagesPagination = [[NSSortDescriptor alloc] initWithKey:@"messageId" ascending:NO];
    [request setSortDescriptors:@[messagesPagination]];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return results;
}

+(NSArray *)getMessagesWithConversationId:(int)conversationId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request {
    [request setPredicate:[NSPredicate predicateWithFormat:@"conversationId == %d", conversationId]];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return results;
}

+(void)deleteMessagesForConversationId:(int)conversationId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request {
    NSArray *messagesToDelete = [Message getMessagesWithConversationId:conversationId inManagedObjectContext:context andFetchRequest:request];
    for (Message *message in messagesToDelete) {
        NSError * error = nil;
        [context deleteObject:message];
        [context save:&error];
    }
}

-(NSUInteger)messageHash {
   return self.senderId ^ self.messageId ^ self.message.hash ;
}

-(Boolean)isMediaMessage {
    return false;
}

-(NSString *)text {
    return self.message;
}

-(NSString *)senderDisplayName {
    return [NSString stringWithFormat:@"%hd,",self.senderId];
}

-(NSDate *)date {
    return self.createdAt;
}


@end
