//
//  Conversation+CoreDataClass.m
//  AgoraExchange
//
//  Created by Travis Chambers on 7/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import "Conversation+CoreDataClass.h"
#import "dateConverter.h"

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@implementation Conversation

-(Conversation *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context{
    self = [NSEntityDescription insertNewObjectForEntityForName:@"Conversation" inManagedObjectContext:context];
    [self updateFromAttributes:dictionary];
    
    return self;
}

-(void)updateFromAttributes:(NSDictionary *)attributes {
    if ([self.updatedAt compare:[dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"updatedAt"])]] == NSOrderedSame && self.updatedAt) {
        return;
    }
    self.room = NULL_TO_NIL([attributes valueForKey:@"room"]);
    self.buyerId = [NULL_TO_NIL([attributes objectForKey:@"buyerId"]) integerValue];
    self.sellerId = [NULL_TO_NIL([attributes objectForKey:@"sellerId"]) integerValue];
    self.conversationId = [NULL_TO_NIL([attributes valueForKey:@"conversationId"]) integerValue];
    self.itemId = [NULL_TO_NIL([attributes objectForKey:@"itemId"]) integerValue];
    self.lastMessage = NULL_TO_NIL([attributes objectForKey:@"lastMessage"]);
    self.unReadMessages = ([SessionManager getCurrentUser].userId == self.sellerId) ? [NULL_TO_NIL([attributes objectForKey:@"sellerUnreadNumber"]) integerValue] : [NULL_TO_NIL([attributes objectForKey:@"buyerUnreadNumber"]) integerValue];
    self.createdAt = [dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"createdAt"])];
    self.updatedAt = [dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"updatedAt"])];
}

+(Conversation *)getConversationWithId:(int)conversationId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request {
    [request setPredicate:[NSPredicate predicateWithFormat:@"conversationId == %d", conversationId]];
    [request setFetchLimit:1];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return (results && [results count] == 1) ? [results firstObject] : NULL;
}

+(NSArray *)getConversationsWithUserId:(int)userId inManagedObjectContext: (NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request {
    [request setPredicate:[NSPredicate predicateWithFormat:@"sellerId == %d OR buyerId == %d", userId, userId]];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return results;
}

+(NSArray *)getConversationsWithItemId:(int)itemId inManagedObjectContext: (NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request {
    [request setPredicate:[NSPredicate predicateWithFormat:@"itemId == %d", itemId]];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return results;
}

+(void)deleteConversation:(Conversation *)conversation inManagedObjectContext:(NSManagedObjectContext *)context {
    NSError * error = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
    [Message deleteMessagesForConversationId:conversation.conversationId inManagedObjectContext:context andFetchRequest:request];
    [context deleteObject:conversation];
    [context save:&error];
}

@end
