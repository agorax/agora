//
//  Message+JSQMessage.h
//  AgoraExchange
//
//  Created by Travis Chambers on 7/26/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import "Message+CoreDataClass.h"
#import <UIKit/UIKit.h>
//
//@interface Message: Message <JSQMessageData>
//+(NSUInteger *)messageHash;
//
//@end
