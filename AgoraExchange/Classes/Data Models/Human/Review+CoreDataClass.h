//
//  Review+CoreDataClass.h
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Review : NSManagedObject

-(Review *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;

+(Review *)getReviewWithReviewerId:(int)reviewerId reviewedId:(int)reviewedId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

-(void)updateFromAttributes:(NSDictionary *)attributes;
@end

NS_ASSUME_NONNULL_END

#import "Review+CoreDataProperties.h"
