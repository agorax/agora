//
//  Conversation+CoreDataClass.h
//  AgoraExchange
//
//  Created by Travis Chambers on 7/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Message+CoreDataClass.h"
#import "SessionManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface Conversation : NSManagedObject

-(Conversation *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;

-(void)updateFromAttributes:(NSDictionary *)attributes;

+(Conversation *)getConversationWithId:(int)conversationId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

+(NSArray *)getConversationsWithUserId:(int)userId inManagedObjectContext: (NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

+(NSArray *)getConversationsWithItemId:(int)itemId inManagedObjectContext: (NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

+(void)deleteConversation:(Conversation *)conversation inManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Conversation+CoreDataProperties.h"
