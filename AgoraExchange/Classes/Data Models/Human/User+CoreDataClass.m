//
//  User+CoreDataClass.m
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import "User+CoreDataClass.h"
#import "SessionManager.h"
#import "dateConverter.h"
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@implementation User

-(User *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context{
    self = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    [self updateFromAttributes:dictionary];
    return self;
}

-(void)updateFromAttributes:(NSDictionary *)attributes {
    if ([self.updatedAt compare:[dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"updatedAt"])]] == NSOrderedSame && self.updatedAt) {
        return;
    }
    self.school = NULL_TO_NIL([attributes objectForKey:@"school"]);
    self.rating = [[attributes valueForKey:@"rating"] respondsToSelector:@selector(integerValue)] ? [[attributes valueForKey:@"rating"] integerValue] : 0;
    self.numRatings = [[attributes valueForKey:@"numRatings"] integerValue];
    self.bio = NULL_TO_NIL([attributes objectForKey:@"bio"]);
    self.name = NULL_TO_NIL([attributes objectForKey:@"name"]);
    self.emailVerified = [[attributes valueForKey:@"emailVerified"] boolValue];
    self.email = NULL_TO_NIL([attributes objectForKey:@"email"]);
    self.userId = [[attributes valueForKey:@"userId"] integerValue];
    self.pictureURL = NULL_TO_NIL([attributes objectForKey:@"pictureURL"]);
    self.location = NULL_TO_NIL([attributes objectForKey:@"location"]);
    self.createdAt = [dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"createdAt"])];
    self.updatedAt = [dateConverter stringToDate:NULL_TO_NIL([attributes objectForKey:@"updatedAt"])];
}

+(User *)getUserWithId:(int)userId inManagedObjectContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"userId == %d", userId]];
    [request setFetchLimit:1];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return (results && [results count] == 1) ? [results firstObject] : NULL;
}

+(User *)getUserWithId:(int)userId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request{
    [request setPredicate:[NSPredicate predicateWithFormat:@"userId == %d", userId]];
    [request setFetchLimit:1];
    NSArray *results = [context executeFetchRequest:request error:nil];
    return (results && [results count] == 1) ? [results firstObject] : NULL;
}


@end
