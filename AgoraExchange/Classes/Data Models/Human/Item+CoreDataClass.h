//
//  Item+CoreDataClass.h
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Item : NSManagedObject

-(Item *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;

+(Item *)getItemWithId:(int)itemId inManagedObjectContext:(NSManagedObjectContext *)context;

+(Item *)getItemWithId:(int)itemId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

-(void)updateFromAttributes:(NSDictionary *)attributes;

@end

NS_ASSUME_NONNULL_END

#import "Item+CoreDataProperties.h"
