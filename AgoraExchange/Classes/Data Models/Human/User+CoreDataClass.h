//
//  User+CoreDataClass.h
//  
//
//  Created by Justin Wang on 6/13/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : NSManagedObject

-(User *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;

-(void)updateFromAttributes:(NSDictionary *)attributes;

+(User *)getUserWithId:(int)userId inManagedObjectContext:(NSManagedObjectContext *)context;

+(User *)getUserWithId:(int)userId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

@end

NS_ASSUME_NONNULL_END

#import "User+CoreDataProperties.h"
