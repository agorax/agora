//
//  Message+CoreDataClass.h
//  AgoraExchange
//
//  Created by Travis Chambers on 7/23/17.
//  Copyright © 2017 Agora Technology Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <JSQMessagesViewController/JSQMessagesViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface Message : NSManagedObject <JSQMessageData>

-(Message *)initWithDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;

-(void)updateFromAttributes:(NSDictionary *)attributes;

+(Message *)getMessageWithId:(int)messageId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

+(NSArray *)getMessagesWithConversationId:(int)conversationId andRefreshCount: (int)refreshCount inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

+(void)deleteMessagesForConversationId:(int)conversationId inManagedObjectContext:(NSManagedObjectContext *)context andFetchRequest:(NSFetchRequest *)request;

-(NSUInteger)messageHash;

-(Boolean) isMediaMessage;

-(NSString *)text;

-(NSString *)senderDisplayName;

@end

NS_ASSUME_NONNULL_END

#import "Message+CoreDataProperties.h"
