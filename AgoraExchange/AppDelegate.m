//
//  AppDelegate.m
//  AgoraExchange
//
//  Created by Justin Wang on 5/31/16.
//  Copyright © 2016 Agora Technology Development, LLC. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Quickblox/Quickblox.h>
#import "ChatTableViewCell.h"
#import "SVProgressHUD.h"
#import <TSMessages/TSMessageView.h>
#import "ChatViewController.h"
#import "AsyncDisplayKit/ASPINRemoteImageDownloader.h"
#import "PinCache.h"
#import "alertViewController.h"
#import <PINRemoteImage/PINRemoteImageCaching.h>
#import <AWSCore/AWSCore.h>
#import "NetworkManager.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Initialize the Amazon Cognito credentials provider
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1 identityId:nil accountId:@"398453963060" identityPoolId:@"us-east-1:7f87a8aa-481b-4aff-95a6-8e0483782b70" unauthRoleArn:@"arn:aws:iam::398453963060:role/Cognito_AgoraExchangeIdentityPoolUnauth_Role" authRoleArn:@"arn:aws:iam::398453963060:role/Cognito_AgoraExchangeIdentityPoolAuth_Role" logins:nil];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    
    // Initialize Parse.
    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration>  _Nonnull configuration) {
        configuration.applicationId = @"DmaiEtQErBE329L0mo1kz2DyRs4JAO8egsluIDAR";
        configuration.clientKey = @"FprVhUpJv7WSzuueK9FQGSvMDmUBoa6CHwGHNJCM";
        configuration.server = @"https://parseapi.back4app.com";
        configuration.localDatastoreEnabled = YES;
    }]];
    
    PFACL *defaultACL = [[PFACL alloc] init];
    [defaultACL setPublicWriteAccess:YES];
    [defaultACL setPublicReadAccess:YES];
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
    [QBSettings setApplicationID:43214];
    [QBSettings setAuthKey:@"MF2JqtYtVcdHTeW"];
    [QBSettings setAuthSecret:@"dwdjCRjM2CWt7Jd"];
    [QBSettings setAccountKey:@"u4HzJE8qZsfApZqUxwyC"];
    [QBSettings setAutoReconnectEnabled:YES];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    self.navigationController = self.storyboard.instantiateInitialViewController;
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    self.LoginViewController = [self.navigationController.viewControllers firstObject];
    
    [self checkVersionNumber];
    [self checkUserAuthenticated];
    [self registerForPushNotifications];
    [SessionManager configureSyncTimer];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [SessionManager invalidateSyncTimer];
    
    [PFQuery clearAllCachedResults];
    //[[[[ASPINRemoteImageDownloader sharedDownloader] sharedPINRemoteImageManager] defaultImageCache] removeAllObjects];
    
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *cacheDirectoryURL = [[fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *PFFileCacheDirectoryURL = [cacheDirectoryURL URLByAppendingPathComponent:@"Parse/PFFileCache" isDirectory:YES];
    NSArray *PFFileCacheDirectory = [fileManager contentsOfDirectoryAtURL:PFFileCacheDirectoryURL includingPropertiesForKeys:nil options:0 error:&error];
    
    if (!PFFileCacheDirectory || error) {
        if (error && error.code != NSFileReadNoSuchFileError) {
            NSLog(@"Error : Retrieving content of directory at URL %@ failed with error : %@", PFFileCacheDirectoryURL, error);
        }
        return;
    }
    for (NSURL *fileURL in PFFileCacheDirectory) {
        BOOL success = [fileManager removeItemAtURL:fileURL error:&error];
        if (!success || error) {
            NSLog(@"Error : Removing item at URL %@ failed with error : %@", fileURL, error);
            error = nil;
        }
    }
    
}

- (void)checkUserAuthenticated
{
    if (![SessionManager getCurrentUser] || ![SessionManager fetchAgoraToken])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:nil];
    }
}

- (void)checkVersionNumber
{
    PFQuery *minVersionNumberQuery = [PFQuery queryWithClassName:@"ApplicationInfo"];
    [minVersionNumberQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        NSNumber * minimumVersionNumber = (NSNumber *)[objects.firstObject objectForKey:@"minimumVersionNumber"];
        NSNumber * versionNumber = [NSNumber numberWithDouble:((NSString *)[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]).doubleValue];
        if (versionNumber.doubleValue < minimumVersionNumber.doubleValue) {
            UIAlertController *alert = [alertViewController updateVersion];
            [AppDelegate topViewController:[UIApplication sharedApplication].keyWindow.rootViewController].view.userInteractionEnabled = NO;
            [[AppDelegate topViewController:[UIApplication sharedApplication].keyWindow.rootViewController] presentViewController:alert animated:YES completion:nil];
        }
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Chats Need Refresh" object:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"Tab Badge Refresh"
     object:self];
    [self checkVersionNumber];
    [self checkUserAuthenticated];
    [self registerForPushNotifications];
    [SessionManager configureSyncTimer];
    [NetworkManager zeroBadgeForUserId:[SessionManager getCurrentUser].userId];
    [UIApplication sharedApplication].applicationIconBadgeNumber =[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) registerForPushNotifications {
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) )
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if( !error )
             {
                 [[UIApplication sharedApplication] registerForRemoteNotifications]; // required to get the app to do anything at all about push notifications
                 NSLog( @"Push registration success." );
             }
             else
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );  
             }  
         }];  
    }
}

#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *devicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    self.deviceToken = devicetoken;
    [SessionManager storeAPNSToken:devicetoken];

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"Tab Badge Refresh"
     object:self];
    [self handleNotificationWithUserInfo:userInfo];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"User Info = %@",notification.request.content.userInfo);
    [self handleNotificationWithUserInfo:notification.request.content.userInfo];
    
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    NSLog(@"User Info = %@",response.notification.request.content.userInfo);
    [self handleNotificationWithUserInfo:response.notification.request.content.userInfo];
    completionHandler();
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler {

    [self handleNotificationWithUserInfo:userInfo];
    handler(UIBackgroundFetchResultNoData);
}

+ (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    // Handling UITabBarController
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [AppDelegate topViewController:tabBarController.selectedViewController];
    }
    // Handling UINavigationController
    else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [AppDelegate topViewController:navigationController.visibleViewController];
    }
    // Handling Modal views
    else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [AppDelegate topViewController:presentedViewController];
    }
    // Handling UIViewController's added as subviews to some other views.
    else {
        for (UIView *view in [rootViewController.view subviews])
        {
            id subViewController = [view nextResponder];    // Key property which most of us are unaware of / rarely use.
            if ( subViewController && [subViewController isKindOfClass:[UIViewController class]])
            {
                return [AppDelegate topViewController:subViewController];
            }
        }
        return rootViewController;
    }
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [ChatTableViewCell clearImageCache];
    [PFQuery clearAllCachedResults];
    [[[[ASPINRemoteImageDownloader sharedDownloader] sharedPINRemoteImageManager] defaultImageCache] removeAllObjects];
    //[[[[ASPINRemoteImageDownloader sharedDownloader] sharedPINRemoteImageManager] defaultImageCache] removeAllObjects];
    
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *cacheDirectoryURL = [[fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *PFFileCacheDirectoryURL = [cacheDirectoryURL URLByAppendingPathComponent:@"Parse/PFFileCache" isDirectory:YES];
    NSArray *PFFileCacheDirectory = [fileManager contentsOfDirectoryAtURL:PFFileCacheDirectoryURL includingPropertiesForKeys:nil options:0 error:&error];
    
    if (!PFFileCacheDirectory || error) {
        if (error && error.code != NSFileReadNoSuchFileError) {
            NSLog(@"Error : Retrieving content of directory at URL %@ failed with error : %@", PFFileCacheDirectoryURL, error);
        }
        return;
    }
    
    for (NSURL *fileURL in PFFileCacheDirectory) {
        BOOL success = [fileManager removeItemAtURL:fileURL error:&error];
        if (!success || error) {
            NSLog(@"Error : Removing item at URL %@ failed with error : %@", fileURL, error);
            error = nil;
        }
    }
}

- (void) handleNotificationWithUserInfo: (NSDictionary *) userInfo {
    if ([userInfo objectForKey:@"aps"])
    {
        NSString *alert = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        //You have x new messages
        //        if ([alert length] > 8 && [[alert substringToIndex:8] isEqualToString:@"You have"])
        //        {
        //            return;
        //        }
        //new message
        if ([userInfo objectForKey:@"type"] && [[userInfo objectForKey:@"type"] isEqualToString:@"newMessage"])
        {
            [self incrementChatBadgeNumberWithConversationId:[userInfo objectForKey:@"conversationId"]];
            NSString *message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
            [TSMessage showNotificationInViewController:[AppDelegate topViewController:[UIApplication sharedApplication].keyWindow.rootViewController] title:@"New Message!"  subtitle:message type:TSMessageNotificationTypeMessage];
            return;
        }
        //deleted conversation
        if ([userInfo objectForKey:@"type"] && [[userInfo objectForKey:@"type"] isEqualToString:@"conversationDeleted"])
        {
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
            Conversation *dialogToDelete = [Conversation getConversationWithId:[[userInfo objectForKey:@"conversationId"] intValue] inManagedObjectContext:[self managedObjectContext] andFetchRequest:request];
            [Conversation deleteConversation:dialogToDelete inManagedObjectContext:[self managedObjectContext]];
            [TSMessage showNotificationInViewController:[AppDelegate topViewController:[UIApplication sharedApplication].keyWindow.rootViewController] title:@"Deleted Conversation!" subtitle:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] type:TSMessageNotificationTypeError];
            return;
        }
        //deleted item
        if ([userInfo objectForKey:@"type"] && [[userInfo objectForKey:@"type"] isEqualToString:@"itemDeleted"])
        {
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
            NSArray *dialogsToDelete = [Conversation getConversationsWithItemId:[[userInfo objectForKey:@"itemId"] intValue] inManagedObjectContext:[self managedObjectContext] andFetchRequest:request];
            for (Conversation *conversation in dialogsToDelete) {
                [Conversation deleteConversation:conversation inManagedObjectContext:[self managedObjectContext]];
            }
            [TSMessage showNotificationInViewController:[AppDelegate topViewController:[UIApplication sharedApplication].keyWindow.rootViewController] title:@"Deleted Item!" subtitle:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] type:TSMessageNotificationTypeError];
            return;
        }
        
        if ([userInfo objectForKey:@"type"] && [[userInfo objectForKey:@"type"] isEqualToString:@"newMessageMedia"])
        {
            [self incrementChatBadgeNumberWithConversationId:[userInfo objectForKey:@"conversationId"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"chatNotification" object:nil userInfo:userInfo];
            [TSMessage showNotificationInViewController:[AppDelegate topViewController:[UIApplication sharedApplication].keyWindow.rootViewController] title: [userInfo objectForKey:@"name"] subtitle:@"Sent you a picture!" type:TSMessageNotificationTypeMessage];
            return;
        }
        
        NSString *message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        
        
        [TSMessage showNotificationInViewController:[AppDelegate topViewController:[UIApplication sharedApplication].keyWindow.rootViewController] title:@"New Message!"  subtitle:message type:TSMessageNotificationTypeMessage];
    }
}

- (void) incrementChatBadgeNumberWithConversationId: (NSNumber *) conversationId {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Conversation"];
    Conversation *conversation = [Conversation getConversationWithId:[conversationId intValue] inManagedObjectContext:[self managedObjectContext] andFetchRequest:request];
    conversation.unReadMessages = conversation.unReadMessages + 1;
    [[self managedObjectContext] save:nil];
    NSDictionary* userInfo = @{@"total": [SessionManager totalUnreadCount]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"chatNotification" object:nil userInfo:userInfo];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Chats Need Refresh" object:nil userInfo:nil];
}

#pragma mark - Core Data
- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self  applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"AgoraExchange.sqlite"]];
    NSError *error = nil;
    NSDictionary *options = @{ NSMigratePersistentStoresAutomaticallyOption : @(YES),
                               NSInferMappingModelAutomaticallyOption : @(YES) };
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil   URL:storeUrl options:options error:&error])
    {
        /*Error for store creation should be handled in here*/
    }
    
    return persistentStoreCoordinator;
}
- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

@end
